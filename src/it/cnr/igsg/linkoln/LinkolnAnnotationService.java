/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

import java.io.IOException;
import java.io.StringReader;

abstract class LinkolnAnnotationService {

	private LinkolnDocument linkolnDocument = null;
	
	protected String input = "";
	
	protected StringBuilder output = new StringBuilder();
	
	protected int position = 0;
	
	int getPosition() {
	
		return position;
	}
	
	void setPosition(int position) {
		this.position = position;
	}

	protected AnnotationEntity annotationEntity = null;
	
	protected int offset = 0;
	protected int length = 0;
	protected String text = "";

	
	LinkolnDocument getLinkolnDocument() {
		return linkolnDocument;
	}

	void setLinkolnDocument(LinkolnDocument linkolnDocument) {
		
		this.linkolnDocument = linkolnDocument;
	}

	void setInput(String input) {
		
		this.input = input;
	}

	String getOutput() {
	
		if(output == null) return null;
		
		return output.toString();
	}
	
	final boolean run() {
		
		output = new StringBuilder();
		
		//position = 0; //Update 0.8.6 - position customizable
		annotationEntity = null;
		offset = 0;
		length = 0;
		text = "";
		
		try {
			
			yyreset(new StringReader(input));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	abstract void yyreset(java.io.Reader reader);
	abstract int yylex() throws java.io.IOException;
	abstract String yytext();
	abstract int yylength();
	abstract void yybegin(int newState);
	abstract void yypushback(int number);
	

	/* ...text text [LKN:ANNOTATION_NAME:NORMALIZED_VALUE:ID]text included in the annotation[/LKN] text text... */
	final static protected String NS = "LKN";
	final static protected String O = "[";
	final static protected String C = "]";
	final static protected String SEP = ":";
	final static protected String OPEN = O + NS + SEP;
	final static protected String CLOSE = O + "/" + NS + C;
	

	protected final void addText() {
		
		addText(yytext());
	}

	protected final void addText(String text) {
		
		output.append(text);
	}

	protected final void addEntity(AnnotationEntity entity) {
		
		addEntity(entity, true, true);
	}
	
	protected final void addEntity(AnnotationEntity entity, boolean serialize) {
		
		addEntity(entity, serialize, true);
	}
	
	protected final void addEntity(AnnotationEntity entity, boolean serialize, boolean store) {
		
		getLinkolnDocument().addAnnotationEntity(entity, store);
		
		if(serialize) {
		
			output.append(serializeEntity(entity));
		}
	}

	final String serializeEntity(AnnotationEntity entity) {
		
		//TODO syntax checks

		//Attenzione: normalizedValue must not contain SEP or O or C symbols 

		StringBuilder annotation = new StringBuilder();

		/* annotation.append(OPEN + entity.getEntity().toString() + SEP + entity.getValue() + SEP + entity.getId() + C); */
		
		annotation.append(OPEN);
		annotation.append(entity.getEntity().toString());
		annotation.append(SEP);
		annotation.append(entity.getValue());
		annotation.append(SEP);
		annotation.append(entity.getId());
		annotation.append(C);
		annotation.append(entity.getText());
		annotation.append(CLOSE);
		
		return annotation.toString();
	}

	protected final void start(AnnotationEntity newEntity, int state, boolean leftEdge, boolean rightEdge) {

		text = yytext();
		
		if(leftEdge) {
			
			addText(text.substring(0,1));
			position++;
			text = text.substring(1);
		}
	
		yypushback(text.length());
	
		if(rightEdge) {
		
			text = text.substring(0, text.length()-1);
		}
		
		annotationEntity = newEntity;

		if(annotationEntity != null) {
			
			annotationEntity.setPosition(position);
			annotationEntity.setText(Util.removeAllAnnotations(text));
		}
				
		offset = 0;
		length = text.length();		
		yybegin(state);	
	}
	
	protected final void annotate(AnnotationEntity newEntity, String value) {
		
		annotate(newEntity, value, false, false, true);
	}
	
	protected final void annotate(AnnotationEntity newEntity, String value, boolean leftEdge, boolean rightEdge) {
		
		annotate(newEntity, value, leftEdge, rightEdge, true);
	}
	
	protected final void annotate(AnnotationEntity newEntity, String value, boolean leftEdge, boolean rightEdge, boolean store) {
		
		 //Attenzione: non utilizzare all'interno di un JFlex state, in quel caso utilizzare save().
		
		if(value == null) value = "";
		
		text = yytext();
		
		if(leftEdge) {
			
			addText(text.substring(0,1));
			position++;
			text = text.substring(1);
		}
	
		annotationEntity = newEntity;
		annotationEntity.setPosition(position);
		
		if(rightEdge) {
		
			text = text.substring(0, text.length()-1);
			yypushback(1);
		}

		text = Util.removeAllAnnotations(text);
		
		annotationEntity.setText(text);

		annotationEntity.setValue(value);
		
		position += text.length();

		addEntity(annotationEntity, true, store);
	}
	
	protected final AnnotationEntity retrieveEntity() {
		
		return retrieveEntity(yytext());
	}
	
	protected final AnnotationEntity retrieveEntity(String text) {
		
		//TODO Replace with a proper RegEx?
		
		int start = text.indexOf(SEP) + 1;
		int end = text.indexOf(SEP, start);
		
		String name = text.substring(start, end);
		
		start = end + 1;
		end = text.indexOf(SEP, start);
		
		String value = text.substring(start, end);

		start = end + 1;
		end = text.indexOf(C, start);
		
		String id = text.substring(start, end);

		start = end + 1;
		end = text.indexOf(O, start);
		
		String content = null;
		
		if(end > -1) content = text.substring(start, end);

		AnnotationEntity ae = getLinkolnDocument().getAnnotationEntity(id);

		if(ae == null && content != null) {
			
			//Ricostruisce l'oggetto AnnotationEntity senza memorizzarlo
			
			ae = new AnnotationEntity(Entity.findByName(name));
			ae.setId(id);
			ae.setValue(value);
			ae.setText(content);
		}
		
		return ae;
	}
	
	//[LKN:CL_DOCTYPE:SENT:9]sentenza[/LKN] [LKN:PARTY::15]Benham c. Regno Unito[/LKN]
	protected final AnnotationEntity retrieveEntity(String text, String entityName) {
		
		int cut = text.indexOf("[LKN:" + entityName.toUpperCase() + ":");
		
		if(cut > -1) {
			
			return retrieveEntity(text.substring(cut));
		}
		
		return null;
	}
	
	/*
	 * Replace old entity with new annotation entity, serialize new annotation entity
	 */
	protected final void replaceEntity(AnnotationEntity oldEntity, AnnotationEntity newEntity) {
		
		newEntity.setPosition(oldEntity.getPosition());
		newEntity.setText(oldEntity.getText());
		newEntity.setValue(oldEntity.getValue());
		newEntity.setId(oldEntity.getId());
		
		//Nota: Eventuali related entities non vengono aggiunte qua
		
		getLinkolnDocument().replaceAnnotationEntity(oldEntity, newEntity);

		output.append(serializeEntity(newEntity));
		
		position += newEntity.getText().length();
	}
	
	protected void save(AnnotationEntity entity, String value) {
		
		save(entity, value, true);
	}
	
	protected void save(AnnotationEntity entity, String value, boolean serialize) {
		
		/*
		 * Con serialize=true è usata all'interno di una start condition che ha annotationEntity=null
		 * per annotare un frammento di testo interno (yytext) e serializzarlo subito;
		 * 
		 * con serialize=false è usata all'interno di una start condition che ha annotationEntity != null
		 * per annotare un frammento di testo interno (yytext) senza serializzarlo (quando è presente un
		 * annotationEntity il testo viene serializzato tutto insieme alla fine della start condition). 
		 * 
		 * Per entrambi offset e position vanno avanti.
		 */
		
		entity.setPosition(position);
		
		String text = Util.removeAllAnnotations(yytext());
		entity.setText(text);

		entity.setValue(value);

		if(annotationEntity != null) {
			
			entity.addRelatedEntity(annotationEntity);
			annotationEntity.addRelatedEntity(entity);
		}
		
		addEntity(entity, serialize);
		
		offset += yylength();
		position += text.length();	
	}
	
	protected void saveInner(AnnotationEntity mainEntity, AnnotationEntity innerEntity, String value) {
		
		/*
		 * Serve ad inserire un'annotazione all'interno di un'altra (related),
		 * ereditando position e text, senza serializzare l'annotazione interna.
		 * 
		 * Nota: offset e position non vanno avanti.
		 */
		
		innerEntity.setPosition(mainEntity.getPosition());
		innerEntity.setText(mainEntity.getText());
		
		innerEntity.setValue(value);

		innerEntity.addRelatedEntity(mainEntity);
		mainEntity.addRelatedEntity(innerEntity);
		
		addEntity(innerEntity, false);
	}

	protected final boolean checkEnd() {

		return checkEnd(0); //default: ritorna allo stato iniziale
	}
	
	//true: raggiunta e superata la fine del buffer
	protected final boolean checkEnd(int returnState) {
		
		offset++;
		position++;
		
		//System.out.println("l:" + length + " o:" + offset + " text:" + yytext());
		
		boolean ret = false;
		
		if(offset >= length) {
			
			ret = true;
			
			if(offset > length) {
				yypushback(1);
				position--;
			}
			
			if(annotationEntity != null) {
				
				addValues();
				
				addEntity(annotationEntity);
			}

			yybegin(returnState);
		}
		
		if(annotationEntity == null) {
			
			addText(yytext());
		}
		
		return ret;
	}
	
	/*
	 * Evita che entità legittime all'interno di uno stato che iniziano sull'edge
	 * del pattern facciano aumentare offset oltre length. Es.: " 22/11/2018novembre 2015," -> pattern " 22/11/2018n"
	 */
	//true: raggiunta la fine del buffer
	protected final boolean checkEdge() {
		
		//System.out.println("checkLastChar - " + yytext() + " o:" + offset + " l:" + length);
		
		if(offset == length) {
			
			yypushback(yylength());

			if(annotationEntity != null) {
				
				addValues();
				
				addEntity(annotationEntity);
			}
			
			yybegin(0);
			
			return true;
		}
		
		return false;
	}

	private void addValues() {
		
		if(this instanceof S004_Dates) ((S004_Dates) this).addValue();
		
		//3.2.7 - commentata
		//if(this instanceof S182_AliasPartitions) ((S182_AliasPartitions) this).addValue();
		
	}
}
