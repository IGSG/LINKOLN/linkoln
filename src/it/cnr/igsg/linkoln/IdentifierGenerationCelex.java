/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

class IdentifierGenerationCelex {

	static boolean run(LinkolnDocument linkolnDocument, LinkolnReference linkolnReference) {
		
		Identifier identifier = new Identifier();
		identifier.setType(Identifiers.CELEX);
	
		String lang = "it";
		
        String prefix = "http://eur-lex.europa.eu/legal-content/" + lang + "/TXT/?uri=CELEX:";
		
        /*
         * TODO
         * 
         * - e che  abroga  la  decisione  quadro 2008/977/GAI del Consiglio; 
         * 
         * Per ora è annotata come DECISIONE ma in questo caso è DECISIONE QUADRO (framework)
         * e serve la lettera F per produrre il CELEX
         * 
         */

		//ALIAS LEGISLATIVI
		String alias = linkolnReference.getAlias();
		
		if(alias != null) {
			
			String aliasCelex = getAliasCode(linkolnReference, alias);
			
			if(aliasCelex == null) {
				
				//Alias legislativo non presente 
				
				return false;
			
			} else {
			
				identifier.setCode(aliasCelex);
				identifier.setUrl(prefix + aliasCelex);
				
				linkolnReference.addIdentifier(identifier);
				
				return true;
			}
		}
		
		String celex = "";
		
		String type = "";
		String sector = "";
		String celexNumber = "";
		String celexYear = "";
		
		String docType = linkolnReference.getDocType();
     
    	String auth = linkolnReference.getAuthority();
    	
    	if(auth != null && ( auth.equals("CGUE") || auth.equals("CGCE") ) ) {
    		
    		//Perchè è obbligatorio il docType?
    		//if(docType == null || ( !docType.equals("SENT") && !docType.equals("ORD") ) ) return false;
    		//Update 3.3.5
    		if(docType == null) docType = "SENT";
    		
    		if( !docType.equals("SENT") && !docType.equals("ORD") ) return false;
    		
    		String caseNumber = linkolnReference.getCaseNumber();
    		
    		if(caseNumber == null) return false;
    		
    		celexNumber = Util.readFirstNumber(caseNumber);
    		celexYear = Util.normalizeYear(Util.readSecondNumber(caseNumber));
    		
    		if(celexNumber.equals("") || celexYear.equals("")) return false;
    		
    		type = "CJ"; //sentenza default
    		if(docType.equals("ORD")) type = "CO";
    		
    		sector = "6";
    		
    		//TODO check: nel caso di cause riunite si devono prendere i valori anno e numero più vecchi.

    		if(celexNumber.length() == 1) { celexNumber = "000" + celexNumber; }
    		if(celexNumber.length() == 2) { celexNumber = "00" + celexNumber; }
    		if(celexNumber.length() == 3) { celexNumber = "0" + celexNumber; }

    		celex = sector + celexYear + type + celexNumber;
   		
    		identifier.setCode(celex);
    		identifier.setUrl(prefix + celex);
    		
    		linkolnReference.addIdentifier(identifier);
    		
    		return true;
    	}
    	
        if(docType == null) return false;
        
        //TODO aggiungere docType INDIRIZZO
        
        if(!docType.equals("DIR") && !docType.equals("REG") && !docType.equals("RACC") && !docType.equals("DECIS")) return false;
        
        if(docType.equals("DIR")) { sector="3"; type = "L"; }
        if(docType.equals("REG")) { sector="3"; type = "R"; }
        if(docType.equals("RACC")) { sector="3"; type = "H"; }
        if(docType.equals("DECIS")) { sector="3"; type = "D"; }
        
        celexNumber = linkolnReference.getNumber();
        
        celexYear = linkolnReference.getYear();
        
		if(celexYear == null && linkolnReference.getDate() != null) {
			celexYear = linkolnReference.getDate().substring(0, 4);
		}

        if(celexNumber == null || celexYear == null) return false;
        
        if(celexNumber.length() == 1) { celexNumber = "000" + celexNumber; }
		if(celexNumber.length() == 2) { celexNumber = "00" + celexNumber; }
		if(celexNumber.length() == 3) { celexNumber = "0" + celexNumber; }

		celex = sector + celexYear + type + celexNumber;
		
		identifier.setCode(celex);
		identifier.setUrl(prefix + celex);
		
		linkolnReference.addIdentifier(identifier);
		
		return true;
	}

	private static String getAliasCode(LinkolnReference linkolnReference, String alias) {
		
		String celex = "";
		
        String celexSuffix = "";
    	
		//EVENTUALE PARTIZIONE
		String artValue = linkolnReference.getCelexArticle();
		
        if(artValue != null) {
			
			if(artValue.length()==1) artValue = "00" + artValue;
			if(artValue.length()==2) artValue = "0" + artValue;
			celexSuffix = artValue;
			
		} else {
			
			celexSuffix = "/TXT";
		}

        //if(alias.equals("EU_TUE")) celex = "11992M";  //Questo non funziona TODO
        if(alias.equals("TRATTATO_UE")) celex = "11992E";
        
        if(alias.equals("TRATTATO_CEE")) {
        	
        	celex = "11957E/TXT";
        	return celex;
        }
        
        if(alias.equals("TRATTATO_FUNZ_UE")) celex = "12008E";
		
        if(celex.equals("")) return null;
        
		return celex + celexSuffix;
	}
	
}
