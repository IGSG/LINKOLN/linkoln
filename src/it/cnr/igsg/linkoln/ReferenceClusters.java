/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

class ReferenceClusters {

	/*
	 * Generazione dei reference clusters.
	 * 
	 * Verifica la compatibilità di un reference con un'altra reference (o con il suo cluster di appartenenza).
	 * 
	 * Restituisce le features di una reference considerando il cluster di appartenenza.
	 * 
	 */
	
	private ReferenceClusters() { }
	
	private LinkolnDocument linkolnDocument = null;
	
	ReferenceClusters(LinkolnDocument linkolnDocument) {
		
		this.linkolnDocument = linkolnDocument;
		
		reference2cluster = new HashMap<Reference,ReferenceCluster>();
	}
	
	Map<Reference,ReferenceCluster> reference2cluster = null;
	
	//Metodi per l'aggiunta a posteriori dei valori derivanti dai NEAR SERVICES.
	void addGeo(Reference reference, AnnotationEntity ae) {
		
		ReferenceCluster rc = reference2cluster.get(reference);
		
		if(rc != null) rc.addGeo(ae);
	}
	
	void addAuthority(Reference reference, AnnotationEntity ae) {
		
		ReferenceCluster rc = reference2cluster.get(reference);
		
		if(rc != null) rc.addAuthority(ae);
	}
		
	//Generazione dei cluster
	boolean generateClusters() {

		if(linkolnDocument == null || reference2cluster == null) return false;
		
		if(linkolnDocument.isDebug()) System.out.println("Generating reference clusters...");
		
		ArrayList<Reference> references = new ArrayList<Reference>();
		Set<Reference> skipList = new HashSet<Reference>();
		Set<ReferenceCluster> clusters = new HashSet<ReferenceCluster>();
		
		for(AnnotationEntity ae : linkolnDocument.getAnnotationEntities()) {
			
			 //Ignora i reference derivanti da alias (non devono essere completati con il clustering service)
			 if(ae instanceof Reference && getAlias(((Reference) ae)) == null) {
				
				references.add((Reference) ae);
			}
		}

		for(Reference ref : references) {
			
			if(skipList.contains(ref)) continue;
			
			skipList.add(ref);
			
			ReferenceCluster cluster = reference2cluster.get(ref);
					
			if(cluster == null) {
				
				cluster = new ReferenceCluster(linkolnDocument);
				cluster.add(ref);
				clusters.add(cluster);
				
				reference2cluster.put(ref, cluster);
			}
			
			for(Reference obj : references) {
				
				if(skipList.contains(obj)) continue;
				
				if(areCompatibles(ref, obj)) {
					
					cluster.add(obj);
					
					reference2cluster.put(obj, cluster);
					
					skipList.add(obj);
				}
			}
		}
		
		if(linkolnDocument.isDebug()) System.out.println("# generated clusters: " + reference2cluster.values().size());
		
		return false;
	}

	private boolean areCompatibles(Reference ref1, Reference ref2) {

		/*
		 * 
		 * Testa sia le condizioni necessarie che quelle sufficienti.
		 * 
		 * Non solo non ci devono essere features incompatibili, 
		 * ci devono anche essere delle feature in comune uguali per essere compabili.
		 * 
		 * Numero/Anno
		 * Auth/Numero
		 * Auth/Data
		 * Tipo/Numero
		 * Tipo/Data
		 * 
		 * 
		 * Per ogni feature, vedere se è incompabile e se è uguale.
		 * 
		 */
		
		//CONDIZIONI NECESSARIE
		
		boolean sameNumber = false;
		boolean sameYear = false;
		boolean sameDate = false;
		boolean sameAuth = false;
		boolean sameDocType = false;
		boolean sameCaseNumber = false;
		
		if(getNumber(ref1) != null && getNumber(ref2) != null) {
			
			if(getNumber(ref1).getValue().equals(getNumber(ref2).getValue())) {
				
				sameNumber = true;
				
			} else {
				
				return false;
			}
		}

		if(getDate(ref1) != null && getDate(ref2) != null) {
			
			if(getDate(ref1).getValue().equals(getDate(ref2).getValue())) {
				
				sameDate = true;
				
			} else {
				
				return false;
			}
		}
		
		if(getYear(ref1) != null && getYear(ref2) != null) {
			
			if(getYear(ref1).getValue().equals(getYear(ref2).getValue())) {
				
				sameYear = true;
				
			} else {
				
				return false;
			}
		}
		
		//Nel caso uno abbia data e l'altro anno, verifica il sameYear
		if(getDate(ref1) != null && getYear(ref2) != null) {
			
			AnnotationEntity year = getDate(ref1).getRelatedEntity(Entity.YEAR);
			
			if(year != null) {
				
				if(year.getValue().equals(getYear(ref2).getValue())) {
					
					sameYear = true;
					
				} else {
					
					return false;
				}
			}
		}
		
		//Nel caso uno abbia anno e l'altro data, verifica il sameYear
		if(getYear(ref1) != null && getDate(ref2) != null) {
			
			AnnotationEntity year = getDate(ref2).getRelatedEntity(Entity.YEAR);
			
			if(year != null) {
				
				if(year.getValue().equals(getYear(ref1).getValue())) {
					
					sameYear = true;
					
				} else {
					
					return false;
				}
			}
		}
		
		/*
		 * 
		 * TODO
		 * 
		 * Si potrebbe fare una verifica nel caso uno abbia un anno e l'altro una data
		 * che la data non disti oltre un anno rispetto all'anno per atti e provvedimenti emanati a fine anno.
		 * 
		 * Es. "d.lgs. n.387 del 15 gennaio 2001" e "d.lgs. 387/2000"
		 * 
		 */
		
		String type1 = getDocumentType(ref1) != null ? getDocumentType(ref1).getValue() : null;
		String type2 = getDocumentType(ref2) != null ? getDocumentType(ref2).getValue() : null;

		if(type1 != null && type2 != null) {
			
			if(type1.equals(type2)) {
				
				sameDocType = true;
				
			} else {
				
				return false;				
			}
		}

		String auth1 = getAuthority(ref1) != null ? getAuthority(ref1).getValue() : null;
		String auth2 = getAuthority(ref2) != null ? getAuthority(ref2).getValue() : null;
		
		if(auth1 != null && auth2 != null) {
			
			if(auth1.equals(auth2)) {
		
				sameAuth = true;
				
			} else {

				if( ( (auth1.equals("THIS_COURT") || auth1.equals("COURT") ) && ( auth2.equals("CORTE_COST") || auth2.equals("CORTE_CASS") ) ) ||
					( (auth2.equals("THIS_COURT") || auth2.equals("COURT") ) && ( auth1.equals("CORTE_COST") || auth1.equals("CORTE_CASS") ) ) ) {
					
					sameAuth = true;
					
				} else {
				
					return false;
				}
			}
		}
		
		//Controlla altri possibili conflitti con other-authorities, ministries, etc.
		
		String otherAuth1 = getOtherAuthority(ref1) != null ? getOtherAuthority(ref1).getValue() : null;
		String otherAuth2 = getOtherAuthority(ref2) != null ? getOtherAuthority(ref2).getValue() : null;
		String ministry1 = getMinistry(ref1) != null ? getMinistry(ref1).getValue() : null;
		String ministry2 = getMinistry(ref2) != null ? getMinistry(ref2).getValue() : null;
		
		if(otherAuth1 != null && ministry2 != null) return false;
		if(otherAuth2 != null && ministry1 != null) return false;
		
		//TODO altre situazioni di incompatibilità ?
		
		
		/*
		 * Compatibilità tipo legislativo ed authority legislativa
		 * es.: legge <> pretura
		 */
		if(type1 != null && auth2 != null) {
			
			if(isLaw(type1) && !isLawAuthority(auth2)) return false;
		}
		
		if(auth1 != null && type2 != null) {
			
			if(isLaw(type2) && !isLawAuthority(auth1)) return false;
		}
		

		//CaseNumber
		if(getCaseNumber(ref1) != null && getCaseNumber(ref2) != null) {
	
			if(getCaseNumber(ref1).getValue().equals(getCaseNumber(ref2).getValue())) {
		
				sameCaseNumber = true;
		
			} else {
		
				return false;
			}
		}

		//CONDIZIONI SUFFICIENTI
		
		if(sameDate && sameNumber) return true;
		
		if(sameYear && sameNumber) return true;
		
		if(sameAuth && sameNumber) return true;
		
		if(sameDocType && sameNumber) return true;
		
		if(sameAuth && sameDate) return true;
		
		if(sameDocType && sameDate) return true;
		
		if(sameCaseNumber) return true;
		
		return false;
	}
	
	private boolean isLaw(String value) {
		
		if(value.equalsIgnoreCase("l")) return true;
		
		return false;
	}
	
	private boolean isLawAuthority(String value) {

		//if(value.toLowerCase().startsWith("stato")) return true;
		if(value.toLowerCase().startsWith("regione")) return true;
		if(value.toLowerCase().startsWith("provincia")) return true;
		
		return false;
	}
	
	AnnotationEntity getAuthority(Reference reference) {
		
		return getAuthority(reference, true);
	}
	
	AnnotationEntity getAuthority(Reference reference, boolean lookInSection) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getAuth();
		}

		AnnotationEntity entity = reference.getRelatedEntity(Entity.CL_AUTH);
		if(entity != null) return entity;
		
		entity = reference.getRelatedEntity(Entity.LEG_AUTH);
		if(entity != null) return entity;
		
		entity = reference.getRelatedEntity(Entity.EU_CL_AUTH);
		if(entity != null) return entity;
		
		entity = reference.getRelatedEntity(Entity.EU_LEG_AUTH);
		if(entity != null) return entity;
		
		//Cerca prima nel Party se è presente (per riferimenti CEDU)
		entity = reference.getRelatedEntity(Entity.PARTY);
		
		if(entity != null) {
			
			entity = entity.getRelatedEntity(Entity.EU_CL_AUTH);
			if(entity != null) return entity;
		}
		
		//Cerca nel full-european-number (caso BCE)
		entity = reference.getRelatedEntity(Entity.FULL_EU_NUMBER);
		
		if(entity != null) {
			
			entity = entity.getRelatedEntity(Entity.EU_LEG_AUTH);
			if(entity != null) return entity;
		}
		
		//Cerca nel case-number (caso CaseNumber della CGUE, es.: causa C-21/16)
		entity = reference.getRelatedEntity(Entity.CASENUMBER);
		
		if(entity != null) {
			
			entity = entity.getRelatedEntity(Entity.EU_CL_AUTH);
			if(entity != null) return entity;
		}
		
		//Cerca nel docType (es.: pattern della Corte Cost. "S. 123/2012" )
		entity = reference.getRelatedEntity(Entity.CL_DOCTYPE);
		
		if(entity != null) {
			
			entity = entity.getRelatedEntity(Entity.CL_AUTH);
			if(entity != null) return entity;
		}
		
		//Cerca nel full-number (caso Corte dei Conti n. 14/QM/03)
		entity = reference.getRelatedEntity(Entity.FULL_NUMBER);
		
		if(entity != null) {
			
			entity = entity.getRelatedEntity(Entity.CL_AUTH);
			if(entity != null) return entity;
		}
		
		//Cerca nella sezione
		if(entity == null && lookInSection) {
			
			AnnotationEntity section = getAuthSection(reference, false, false);
			
			if(section != null) {
				
				entity = section.getRelatedEntity(Entity.CL_AUTH);
				
				if(entity != null) return entity;
				
				entity = section.getRelatedEntity(Entity.EU_CL_AUTH);
				
				if(entity != null) return entity;
			}
		}

		return null;
	}
	
	AnnotationEntity getOtherAuthority(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getOtherAuth();
		}

		AnnotationEntity entity = reference.getRelatedEntity(Entity.OTHER_AUTH);
		if(entity != null) return entity;
		
		entity = getFullNumber(reference);
		
		if(entity != null) {
			
			AnnotationEntity item = entity.getRelatedEntity(Entity.OTHER_AUTH);
			if(item != null) return item;
		}
		
		return null;
	}
	
	AnnotationEntity getDocumentType(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getDocType();
		}

		//Il document type può essere attaccato all'alias o direttamente alla reference
		AnnotationEntity parent = getAlias(reference);
		
		if(parent == null) parent = reference;
		
		AnnotationEntity entity = parent.getRelatedEntity(Entity.CL_DOCTYPE);
		if(entity != null) return entity;

		entity = parent.getRelatedEntity(Entity.LEG_DOCTYPE);
		if(entity != null) return entity;
		
		entity = parent.getRelatedEntity(Entity.EU_LEG_DOCTYPE);
		if(entity != null) return entity;
		
		entity = parent.getRelatedEntity(Entity.DOCTYPE);
		if(entity != null) return entity;
		
		return null;
	}
	
	AnnotationEntity getCity(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getCity();
		}

		AnnotationEntity entity = reference.getRelatedEntity(Entity.CITY);
		
		if(entity != null) return entity;

		entity = reference.getRelatedEntity(Entity.MUNICIPALITY);
		
		if(entity != null) return entity;
		
		//Look in authority
		AnnotationEntity auth = getAuthority(reference);

		if(auth != null) {
			
			entity = auth.getRelatedEntity(Entity.CITY);
			
			if(entity != null) return entity;
			
			entity = auth.getRelatedEntity(Entity.MUNICIPALITY);
			
			if(entity != null) return entity;
		}
		
		return null;
	}
	
	AnnotationEntity getAuthSection(Reference reference) {
		
		return getAuthSection(reference, true, true);
	}
	
	AnnotationEntity getAuthSection(Reference reference, boolean lookInAuthority, boolean lookInFullNumber) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getSection();
		}
		
		AnnotationEntity auth = reference.getRelatedEntity(Entity.CL_SECTION);
		
		if(auth == null && lookInAuthority) {
			
			//look for a section auth within the main auth
			auth = getAuthority(reference, false);
			
			if(auth != null) {
				
				auth = auth.getRelatedEntity(Entity.CL_SECTION);
				
			} else {
				
				return null;
			}
		}

		if(auth == null && lookInFullNumber) {
			
			//look for a section auth within the full number
			AnnotationEntity fullNumber = getFullNumber(reference);
			
			if(fullNumber != null && fullNumber.getRelatedEntity(Entity.CL_SECTION) != null) {
				
				return fullNumber.getRelatedEntity(Entity.CL_SECTION);
				
			} else {
				
				return null;
			}
		}

		return auth;
	}

	AnnotationEntity getDetachedSection(Reference reference) {
		
		AnnotationEntity det = reference.getRelatedEntity(Entity.CL_DETACHED);
		
		if(det == null) {
			
			//look for a det auth within the main auth
			AnnotationEntity auth = getAuthority(reference);
			
			if(auth != null) {
				
				return auth.getRelatedEntity(Entity.CL_DETACHED);
			}
		}

		return null;
	}

	AnnotationEntity getDetCity(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getDetached();
		}

		AnnotationEntity auth = getDetachedSection(reference);
		
		if(auth != null) {
			
			AnnotationEntity entity = auth.getRelatedEntity(Entity.CITY);
			
			if(entity != null) return entity;
			
			entity = auth.getRelatedEntity(Entity.MUNICIPALITY);
			
			if(entity != null) return entity;
		}
		
		return null;
	}
		
	AnnotationEntity getRegion(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getRegion();
		}

		AnnotationEntity entity = reference.getRelatedEntity(Entity.REGION);
		
		if(entity != null) {

			return entity;
		}
		
		AnnotationEntity auth = getAuthority(reference);
		
		if(auth != null) {
			
			entity = auth.getRelatedEntity(Entity.REGION);
			
			if(entity != null) {

				return entity;
			}
			
			AnnotationEntity authDist = auth.getRelatedEntity(Entity.CL_DETACHED);
			
			if(authDist != null) {
				
				entity = authDist.getRelatedEntity(Entity.REGION);
				
				if(entity != null) {

					return entity;
				}
			}
		}
		
		return null;
	}
	
	AnnotationEntity getMinistry(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getMinistry();
		}

		AnnotationEntity entity = reference.getRelatedEntity(Entity.MINISTRY);
		
		if(entity != null) {

			return entity;
		}
		
		
		return null;
	}
	
	AnnotationEntity getNumber(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getNumber();
		}

		AnnotationEntity entity = reference.getRelatedEntity(Entity.NUMBER);
		if(entity != null) {
			
			return entity;
		}
		
		entity = reference.getRelatedEntity(Entity.NUMBER_YEAR);
		if(entity != null) {
			
			AnnotationEntity number = entity.getRelatedEntity(Entity.NUMBER);
			
			if(number != null) {
				
				return number;
			}
		}

		entity = reference.getRelatedEntity(Entity.FULL_NUMBER);
		if(entity != null) {
			
			AnnotationEntity number = entity.getRelatedEntity(Entity.NUMBER);
			
			if(number != null) {
				
				return number;
			}
		}

		entity = reference.getRelatedEntity(Entity.FULL_EU_NUMBER);
		if(entity != null) {
			
			AnnotationEntity number = entity.getRelatedEntity(Entity.NUMBER);
			
			if(number != null) {
				
				return number;
			}
		}

		return null;
	}
	
	AnnotationEntity getFullNumber(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getFullNumber();
		}

		AnnotationEntity entity = reference.getRelatedEntity(Entity.FULL_NUMBER);
		if(entity != null) return entity;

		entity = reference.getRelatedEntity(Entity.FULL_EU_NUMBER);
		if(entity != null) return entity;
		
		return null;
	}
	
	AnnotationEntity getYear(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getYear();
		}

		AnnotationEntity entity = reference.getRelatedEntity(Entity.YEAR);
		
		if(entity != null) {
			return entity;
		}
		
		entity = reference.getRelatedEntity(Entity.NUMBER_YEAR);
		if(entity != null) {
			
			AnnotationEntity year = entity.getRelatedEntity(Entity.YEAR);
			
			if(year != null) {
				
				return year;
			}
		}

		entity = reference.getRelatedEntity(Entity.FULL_NUMBER);
		if(entity != null) {
			
			AnnotationEntity year = entity.getRelatedEntity(Entity.YEAR);
			
			if(year != null) {
				
				return year;
			}
		}

		entity = reference.getRelatedEntity(Entity.FULL_EU_NUMBER);
		if(entity != null) {
			
			AnnotationEntity year = entity.getRelatedEntity(Entity.YEAR);
			
			if(year != null) {
				
				return year;
			}
		}
		
		return null;
	}
	
	AnnotationEntity getDate(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getDate();
		}

		AnnotationEntity entity = reference.getRelatedEntity(Entity.DOC_DATE);
		
		if(entity != null) {
			
			return entity;
		}

		return null;
	}
	
	AnnotationEntity getDepositDate(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getDepositDate();
		}

		AnnotationEntity entity = reference.getRelatedEntity(Entity.DEPOSIT_DATE);
		
		if(entity != null) {
			
			return entity;
		}

		return null;
	}
	
	AnnotationEntity getNotificationDate(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getNotificationDate();
		}

		AnnotationEntity entity = reference.getRelatedEntity(Entity.NOTIFICATION_DATE);
		
		if(entity != null) {
			
			return entity;
		}

		return null;
	}
	
	AnnotationEntity getPublicationDate(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getPublicationDate();
		}

		AnnotationEntity entity = reference.getRelatedEntity(Entity.PUBLICATION_DATE);
		
		if(entity != null) {
			
			return entity;
		}

		return null;
	}
		
	AnnotationEntity getCaseNumber(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getCaseNumber();
		}

		AnnotationEntity entity = reference.getRelatedEntity(Entity.CASENUMBER);
		
		if(entity != null) {
			
			return entity;
		}

		return null;
	}
	
	AnnotationEntity getApplicant(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			ReferenceCluster rc = reference2cluster.get(reference);
			
			return rc.getApplicant();
		}

		AnnotationEntity party = reference.getRelatedEntity(Entity.PARTY);
		
		if(party != null) {
			
			AnnotationEntity applicant = party.getRelatedEntity(Entity.APPLICANT);
			
			if(applicant != null) {
				
				return applicant;
			}
		}
		
		return null;
	}

	AnnotationEntity getDefendant(Reference reference) {

		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getDefendant();
		}

		AnnotationEntity party = reference.getRelatedEntity(Entity.PARTY);
		
		if(party != null) {
			
			AnnotationEntity defendant = party.getRelatedEntity(Entity.DEFENDANT);
			
			if(defendant != null) {
				
				return defendant;
			}
		}
		
		return null;
	}

	AnnotationEntity getSubject(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getSubject();
		}

		AnnotationEntity entity = reference.getRelatedEntity(Entity.SUBJECT);
		
		if(entity != null) {
			
			return entity;
		}
		
		AnnotationEntity caseNum = getCaseNumber(reference);
		
		if(caseNum != null) {
			
			entity = caseNum.getRelatedEntity(Entity.SUBJECT);
			
			if(entity != null) {
				
				return entity;
			}
		}

		//Cerca nella sezione
		if(entity == null) {
			
			AnnotationEntity section = getAuthSection(reference);
			
			if(section != null) {
				
				entity = section.getRelatedEntity(Entity.SUBJECT);
				
				if(entity != null) return entity;
			}
		}
		
		return null;
	}
	
	AnnotationEntity getEuAcronym(Reference reference) {
		
		if(linkolnDocument.isUseClustering() && reference2cluster.get(reference) != null) {
			
			return reference2cluster.get(reference).getEuAcronym();
		}

		AnnotationEntity entity = reference.getRelatedEntity(Entity.EU_ACRONYM);
		
		if(entity != null) return entity;
		
		entity = getFullNumber(reference);
		
		if(entity != null) {
			
			AnnotationEntity item = entity.getRelatedEntity(Entity.EU_ACRONYM);
			
			if(item != null) return item;
		}
		
		entity = getDocumentType(reference);
		
		if(entity != null) {
			
			AnnotationEntity item = entity.getRelatedEntity(Entity.EU_ACRONYM);
			
			if(item != null) return item;
		}
		
		return null;
	}
	
	AnnotationEntity getAlias(Reference reference) {
		
		AnnotationEntity entity = reference.getRelatedEntity(Entity.LEG_ALIAS);
		
		if(entity != null) {
			
			return entity;
		}
		
		entity = reference.getRelatedEntity(Entity.EU_LEG_ALIAS);
		
		if(entity != null) {
			
			return entity;
		}
		
		return null;
	}
	
	String getPartition(Reference reference) {
		
		AnnotationEntity partition = reference.getRelatedEntity(Entity.PARTITION);
		
		if(partition != null) {
			
			//3.2.7 aggiorna i valori delle partitions create da S182_AliasPartitions
			if(partition.getValue().equals("")) ((Partition) partition).updateValue();
			
			return partition.getValue();
		}
		
		return null;
	}
	
	String getRoNumber(Reference reference) {
		
		AnnotationEntity roNumber = reference.getRelatedEntity(Entity.RO_NUMBER);
		
		if(roNumber != null) {
			
			return roNumber.getValue();
		}
		
		return null;
	}
	
	String getRvNumber(Reference reference) {
		
		AnnotationEntity rvNumber = reference.getRelatedEntity(Entity.RV_NUMBER);
		
		if(rvNumber != null) {
			
			return rvNumber.getValue();
		}
		
		return null;
	}
	
	String getGuAttr(Reference reference) {
		
		AnnotationEntity guSeries = reference.getRelatedEntity(Entity.GU_ATTR);
		
		if(guSeries != null) {
			
			return guSeries.getValue();
		}
		
		// Cerca nel doc-type=GU
		
		AnnotationEntity docType = getDocumentType(reference);
		
		if(docType != null) {
			
			guSeries = docType.getRelatedEntity(Entity.GU_ATTR);
			
			if(guSeries != null) return guSeries.getValue();
		}
		
		return null;
	}
	
	boolean isReference(Reference reference) {

		/*
		 * Indica se la reference soddisfa i requisiti per diventare un lkn:reference invece di un lkn:warning
		 * nel caso di reference parziali completati da servizi successivi.
		 * 
		 * Regola generale per condizione sufficiente: se è presente authority OR docType AND numero OR data   <--- UPDATE 3.3.7 authority OR docType AND (numero AND anno) OR data  
		 * 
		 * Eccezioni:
		 * 
		 *  - se è presente solo CaseLaw authority e data non considerarlo riferimento;
		 *  - se è statuto regionale è sufficiente la presenza della regione
		 *  - se authority è CEDU e sono presenti applicant e defendant
		 *  - in caso di OtherAuthority occorrono almeno due feature principali come data e numero, data e tipo, numero e anno (data da sola NO, numero da solo NO) 
		 */
		
		if(getAuthority(reference, true) != null && getAuthority(reference, true).getValue().equals("CEDU") &&
				getApplicant(reference) != null && getDefendant(reference) != null) return true;
		
		if(getDocumentType(reference) != null && getDocumentType(reference).getValue() .equals("STATUTO") && getRegion(reference) != null) return true;
		
		boolean hasAuthority = false;
		boolean hasOtherAuthority = false;
		boolean hasAuthSection = false;
		
		boolean hasDocType = false;
		boolean hasNumber = false;
		boolean hasDate = false;
		boolean hasYear = false;

		if(getAuthority(reference, true) != null || getOtherAuthority(reference) != null || getAuthSection(reference, true, true) != null) hasAuthority = true;
		if(getOtherAuthority(reference) != null) hasOtherAuthority = true;
		if(getAuthSection(reference, true, true) != null) hasAuthSection = true;
		
		if(getDocumentType(reference) != null) hasDocType = true;
		
		if(getNumber(reference) != null || getCaseNumber(reference) != null) hasNumber = true;
		
		if(getDate(reference) != null) hasDate = true;
		
		if(getYear(reference) != null) hasYear = true;
		
		if(hasAuthority && !hasOtherAuthority && !hasAuthSection && !hasDocType && !hasNumber && hasDate) {
			
			if(getAuthority(reference, true).getEntity().equals(Entity.CL_AUTH)) return false;
		}

		//Regole su outher authority
		if(hasOtherAuthority && getAuthority(reference, true) == null) {
			
			if(hasDate && hasDocType) return true;
			if(hasDate && hasNumber) return true;
			if(hasYear && hasNumber) return true;

			return false;
		}

		/*if( (hasAuthority || hasDocType) && (hasNumber || hasDate) ) {*/
		if( (hasAuthority || hasDocType) && ( (hasNumber && hasYear) || hasDate) ) {
			
			return true;
		}
		
		return false;
	}
}
