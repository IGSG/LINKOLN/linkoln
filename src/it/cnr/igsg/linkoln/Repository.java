/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

class Repository {
	
	private static final String costRepositoryFileName = "data/cost.csv";
	
	static Map<String,String> costRepository = null;
	static Map<String,String> costDatesRepository = null;
	
	static boolean initCostRepository() {
		
		File costRepositoryFile = new File(costRepositoryFileName);
		
		BufferedReader reader = null;
		InputStream in = null;
		
		try {
		
			if( !costRepositoryFile.exists()) {
				
				//Look for it as a resource in the jar file
				Repository util = new Repository();
				in = util.getClass().getResourceAsStream("/" + costRepositoryFileName);
				
				if(in == null) {
					return false;
				}
				
				reader = new BufferedReader(new InputStreamReader(in));
				
			} else {
				
				reader = new BufferedReader(new InputStreamReader(new FileInputStream(costRepositoryFile)));
			}
		
			Repository.costRepository = new HashMap<String,String>();
			Repository.costDatesRepository = new HashMap<String,String>();
			
			String l = null;
	
		    while( ( l = reader.readLine() ) != null ) {
	
		    	//1957,8,SENT,1957-01-17,1957-01-26
		    	
		    	String[] items = l.split(",");
		    	
		    	String anno = items[0];
		    	String numero = items[1];
		    	String tipo = items[2];
		    	String data = items[3];
		    	String dataDep = items[4];
		    	
		    	//String record = tipo + "_" + anno + "_" + numero + "_" + data;
		    	String record = "ECLI:IT:COST:" + anno + ":" + numero;
		    	
		    	costRepository.put(numero + "_" + data, record);
		    	costRepository.put(numero + "_" + anno, record);
		    	costRepository.put(numero + "_" + anno + "_" + data, record);
		    	costRepository.put(tipo + "_" + numero + "_" + anno, record);
		    	costRepository.put(tipo + "_" + numero + "_" + data, record);
		    	
		    	costRepository.put(numero + "_" + dataDep, record);
		    	costRepository.put(numero + "_" + anno + "_" + dataDep, record);
		    	costRepository.put(tipo + "_" + numero + "_" + dataDep, record);
		    	
		    	costDatesRepository.put(numero + "_" + anno, data);
		    }
		    
		    reader.close();
		    if(in != null) in.close();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	static boolean checkRepositoryCost(LinkolnDocument linkolnDocument, Reference referenceEntity, boolean strict) {
		
		/*
		 * strict mode se authority non esplicita
		 */
		
		ReferenceClusters referenceClusters = linkolnDocument.getReferenceClusters();
		
		String docType = null;
		String number = null;
		String year = null;
		String date = null;
		
		if(referenceClusters.getDocumentType(referenceEntity) != null) docType = referenceClusters.getDocumentType(referenceEntity).getValue();
		if(referenceClusters.getNumber(referenceEntity) != null) number = referenceClusters.getNumber(referenceEntity).getValue();
		if(referenceClusters.getYear(referenceEntity) != null) year = referenceClusters.getYear(referenceEntity).getValue();
		if(referenceClusters.getDate(referenceEntity) != null) date = referenceClusters.getDate(referenceEntity).getValue();

		//Fai passare tutti i riferimenti post 2023
		try {
			Integer cutYear = null;
			if(year != null) {
				cutYear = Integer.valueOf(year);
			} else {
				if(date != null) cutYear = Integer.valueOf(date.substring(0, 4));
			}
			if(cutYear != null && cutYear > 2023) return true;
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		if(strict) {
			
			if(checkRepositoryCost(number, year, date, docType) != null) {
				
				return true;
			} 
			
		} else {
		
			if(year == null && date != null) year = date.substring(0, 4);
			
			if(number != null && year != null) {
			
				if(checkRepositoryCost(number, year, null, null) != null) {
					
					return true;
				}
			}
			
			if(docType != null && date != null) {
				
				if(checkRepositoryCost(null, null, date, docType) != null) {
					
					return true;
				}
			}

		}
		
		return false;
	}
	
	//Return ECLI if present, otherwise null
	static String checkRepositoryCost(String number, String year, String date, String docType) {
		
		String key = "";
		
		if(docType != null) key = docType + "_";
		if(number != null) key += number;
		
		if(date != null) {
			
			key += "_" + date;
			
		} else {
			
			if(year != null) key += "_" + year;
		}
		
		return Repository.costRepository.get(key);
	}
	
	static String getCostDate(String year, String num) {
		
		String data = costDatesRepository.get(num + "_" + year);
		
		return data;
	}
	
	private static final String ceduFileName = "data/cedu.csv";
	
	static Map<String,RecordCedu> ecli2recordCedu = null;
	
	static Map<String,Set<RecordCedu>> date_country2recordsCedu = null;
	
	static Map<String,RecordCedu> applicant_country2recordCedu = null;
	
	static boolean initCeduRepository() {
		
		/*
		 * FORMAT:
		 * 
		 * ECLI ITEMID CASENUMBER DATE LANG DEFENDANT APPLICANT
		 * 
		 * ECLI:CE:ECHR:2023:1212JUD001523117,001-229392,15231/17,2023-12-12,ENG,TUR,KOLAY AND OTHERS
		 * 
		 */
		
		File ceduRepositoryFile = new File(ceduFileName);
		
		BufferedReader reader = null;
		InputStream in = null;
		
		try {
		
			if( !ceduRepositoryFile.exists()) {
				
				//Look for it as a resource in the jar file
				Repository util = new Repository();
				in = util.getClass().getResourceAsStream("/" + ceduFileName);
				
				if(in == null) {
					return false;
				}
				
				reader = new BufferedReader(new InputStreamReader(in));
				
			} else {
				
				reader = new BufferedReader(new InputStreamReader(new FileInputStream(ceduRepositoryFile)));
			}
		
			ecli2recordCedu = new HashMap<String,RecordCedu>();
			date_country2recordsCedu = new HashMap<String,Set<RecordCedu>>();
			applicant_country2recordCedu = new HashMap<String,RecordCedu>();
			
			Set<String> skipMultipleMatches = new HashSet<String>();
			
			String l = null;
	
		    while( ( l = reader.readLine() ) != null ) {
	
		    	String[] items = l.split(",");
		    	
		    	RecordCedu recordCedu = new RecordCedu();
		    	
		    	recordCedu.ecli = items[0];
		    	recordCedu.itemId = items[1];
		    	recordCedu.caseNumber = items[2];
		    	recordCedu.date = items[3];
		    	recordCedu.lang = items[4];
		    	recordCedu.defendant = items[5];
		    	String applicant = items[6];
		    	
		    	//process applicant
		    	applicant = Util.stripAccents(applicant).toLowerCase().trim();

		    	if(applicant.endsWith("& others")) applicant = applicant.substring(0, applicant.length()-"& others".length()).trim();
		    	if(applicant.endsWith("and others")) applicant = applicant.substring(0, applicant.length()-"and others".length()).trim();
		    	if(applicant.endsWith("others")) applicant = applicant.substring(0, applicant.length()-"others".length()).trim();
		    	if(applicant.endsWith("et al.")) applicant = applicant.substring(0, applicant.length()-"et al.".length()).trim();
		    	if(applicant.endsWith("et al")) applicant = applicant.substring(0, applicant.length()-"et al".length()).trim();
		    	if(applicant.endsWith("et autres")) applicant = applicant.substring(0, applicant.length()-"et autres".length()).trim();
		    	if(applicant.endsWith("autres")) applicant = applicant.substring(0, applicant.length()-"autres".length()).trim();
		    	if(applicant.endsWith("et autre")) applicant = applicant.substring(0, applicant.length()-"et autre".length()).trim();
		    	
		    	recordCedu.applicant = applicant;
		    	
		    	String[] countries = recordCedu.defendant.split(";");
		    	
		    	for(int i = 0; i < countries.length; i++) {
		    		
		    		String country = Util.getTwoLettersCountryCode(countries[i].trim());
		    		
		    		if(country == null || country.length() != 2) {
		    			//System.err.println(recordCedu.defendant);
		    			continue;
		    		}
		    		
		    		String date_country = recordCedu.date + "_" + country;
		    		
			    	Set<RecordCedu> records = date_country2recordsCedu.get(date_country);
			    	
			    	if(records == null) {
			    		
			    		records = new HashSet<RecordCedu>();
			    		date_country2recordsCedu.put(date_country, records);
			    	}
			    	
			    	records.add(recordCedu);
			    	
			    	String applicant_country = applicant + "_" + country; 
			    	RecordCedu value = applicant_country2recordCedu.get(applicant_country);
			    	
			    	if(value == null) {
			    		
			    		applicant_country2recordCedu.put(applicant_country, recordCedu);
			    		
			    	} else {
			    		
			    		if( !value.caseNumber.equals(recordCedu.caseNumber)) {
			    			
			    			skipMultipleMatches.add(applicant_country);
			    		}
			    	}
		    	}
		    	
		    	ecli2recordCedu.put(recordCedu.ecli, recordCedu);
		    }
		    
		    for(String key : skipMultipleMatches) applicant_country2recordCedu.put(key, null);
		    
		    reader.close();
		    if(in != null) in.close();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private static final String guRepositoryFileName = "data/gu.csv";
	
	static Map<String,String> guRepository = null;
	
	/*
	 * 
	 * http://www.gazzettaufficiale.it/eli/gu/2021/11/22/278/sg,2021,11,22,278
	 * 
	 */
	static boolean initGuRepository() {
		
		File guRepositoryFile = new File(guRepositoryFileName);
		
		BufferedReader reader = null;
		InputStream in = null;
		
		try {
		
			if( !guRepositoryFile.exists()) {
				
				//Look for it as a resource in the jar file
				Repository util = new Repository();
				in = util.getClass().getResourceAsStream("/" + guRepositoryFileName);
				
				if(in == null) {
					return false;
				}
				
				reader = new BufferedReader(new InputStreamReader(in));
				
			} else {
				
				reader = new BufferedReader(new InputStreamReader(new FileInputStream(guRepositoryFile)));
			}
		
			Repository.guRepository = new HashMap<String,String>();
			
			String l = null;
	
		    while( ( l = reader.readLine() ) != null ) {
	
		    	String[] items = l.split(",");
		    	
		    	String uri = items[0];
		    	String anno = items[1];
		    	String mese = items[2];
		    	String giorno = items[3];
		    	String numero = items[4];
		    	
		    	//Skip Supplemento Ordinario !?
		    	if(uri.indexOf("/so/") > -1) continue;
		    	
		    	//String record = anno + "_" + mese + "_" + giorno + "_" + numero;
		    	//guRepository.put(numero + "_" + anno, record);
		    	
		    	guRepository.put(numero + "_" + anno, uri);
		    }
		    
		    reader.close();  
		    if(in != null) in.close();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
}
