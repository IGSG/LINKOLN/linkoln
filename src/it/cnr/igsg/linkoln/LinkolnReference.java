/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

import java.util.HashSet;
import java.util.Set;

public final class LinkolnReference {
	
	/*
	 * Presentazione dei riferimenti estratti da Linkoln con valori delle features normalizzati.
	 */
	
	private String sourceName = ""; //Nome file o provenienza del riferimento
	private String sourcePartitionId = "";
	private int id = 0;

	private String refType = null; //VALUES: unknown, legislation, caselaw, other? (contratti, accordi, protocolli), prassi?, doctrine?
	private String refScope = null; //VALUES: unknown, nazionale, regionale, comunitario, internazionale, provinciale, comunale
	
	private String text = null;
	private String context = null;

	private String title = null;
	
	private String alias = null;
	private String authority = null;
	private String authoritySection = null;
	private String detachedCity = null;
	private String otherAuthority = null;
	private String docType = null;
	private String number = null;
	private String fullNumber = null;
	private String euAcronym = null;
	private String year = null;
	private String date = null;
	private String depositDate = null;
	private String notificationDate = null;
	private String publicationDate = null;
	
	private String caseNumber = null;
	private String roNumber = null;
	private String rvNumber = null;
	private String guAttr = null;
	private String area = null;
	private String city = null;
	private String region = null;
	private String ministry = null;
	private String applicant = null;
	private String defendant = null;
	
	private String partition = null;
	private String article = null;
	private String comma = null;

	//Ignora il riferimento nelle successive richieste di output
	private boolean skip = false;
	
	//Eventuali identificatori ed URL associate alla reference
	private Set<Identifier> identifiers = new HashSet<Identifier>();
	
	public Set<Identifier> getIdentifiers() {
		
		return this.identifiers;
	}
	
	boolean addIdentifier(Identifier identifier) {
		
		return identifiers.add(identifier);
	}
	
	private String citedDocSimpleId = "";
	
	
	LinkolnReference() { }

	public int getId() {
		return id;
	}

	void setId(int id) {
		this.id = id;
	}
	
	public String getSourceName() {
		return sourceName;
	}

	void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	
	public String getSourcePartitionId() {
		return sourcePartitionId;
	}

	void setSourcePartitionId(String sourcePartitionId) {
		this.sourcePartitionId = sourcePartitionId;
	}

	public String getText() {
		return text;
	}

	void setText(String text) {
		this.text = text;
	}

	public String getContext() {
		return context;
	}

	void setContext(String context) {
		this.context = context;
	}

	String getTitle() {
		return title;
	}

	void setTitle(String title) {
		this.title = title;
	}

	void setRefType(String refType) {
		this.refType = refType;
	}

	void setRefScope(String refScope) {
		this.refScope = refScope;
	}

	void setDetachedCity(String detachedCity) {
		this.detachedCity = detachedCity;
	}

	void setAuthority(String authority) {
		this.authority = authority;
	}

	void setAuthoritySection(String authoritySection) {
		this.authoritySection = authoritySection;
	}

	void setDocType(String docType) {
		this.docType = docType;
	}

	void setNumber(String number) {
		this.number = number;
	}

	void setYear(String year) {
		this.year = year;
	}
	
	void setDate(String date) {
		this.date = date;
	}

	void setDepositDate(String date) {
		this.depositDate = date;
	}

	void setNotificationDate(String date) {
		this.notificationDate = date;
	}

	void setPublicationDate(String date) {
		this.publicationDate = date;
	}

	void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	void setRoNumber(String roNumber) {
		this.roNumber = roNumber;
	}

	void setRvNumber(String rvNumber) {
		this.rvNumber = rvNumber;
	}

	void setGuAttr(String guAttr) {
		this.guAttr = guAttr;
	}

	void setCity(String city) {
		this.city = city;
	}
	
	void setRegion(String region) {
		this.region = region;
	}

	void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	void setDefendant(String defendant) {
		this.defendant = defendant;
	}

	void setAlias(String alias) {
		this.alias = alias;
	}

	void setPartition(String partition) {
		this.partition = partition;
	}

	void setOtherAuthority(String otherAuthority) {
		this.otherAuthority = otherAuthority;
	}

	void setFullNumber(String fullNumber) {
		this.fullNumber = fullNumber;
	}

	void setEuAcronym(String euAcronym) {
		this.euAcronym = euAcronym;
	}

	void setMinistry(String ministry) {
		this.ministry = ministry;
	}
	
	void setArea(String area) {
		this.area = area;
	}

	public String getAuthority() {
		
		if(authority == null && getOtherAuthority() == null) {
			
			//Completamento dell'autorità basato su specifici attributi
			//TODO Dovrebbe essere controllato il DOC-TYPE, non è sempre corretto fare così
			if(getMinistry() != null) return "MINISTERO";
			if(getRegion() != null) return "REGIONE";
			if(getCity() != null) return "PROVINCIA";
			
			return null;
		}
		if(authority == null) return null;
		if(authority.trim().length() < 1) return null;
		return authority.trim();
	}
	
	public String getAuthoritySection() {
		if(authoritySection == null) return null;
		if(authoritySection.trim().length() < 1) return null;
		return authoritySection.trim();
	}
	
	public String getDetachedCity() {
		if(detachedCity == null) return null;
		if(detachedCity.trim().length() < 1) return null;
		return detachedCity.trim();
	}

	public String getDocType() {
		if(docType == null) return null;
		if(docType.trim().length() < 1) return null;
		return docType.trim();
	}

	public String getNumber() {
		if(number == null) return null;
		if(number.trim().length() < 1) return null;
		return number.trim();
	}

	public String getYear() {
		if(year == null) return null;
		if(year.trim().length() < 1) return null;
		return year.trim();
	}
	
	public String getDepositDate() {
		if(depositDate == null) return null;
		if(depositDate.trim().length() < 1) return null;
		return depositDate.trim();
	}

	public String getNotificationDate() {
		if(notificationDate == null) return null;
		if(notificationDate.trim().length() < 1) return null;
		return notificationDate.trim();
	}

	public String getPublicationDate() {
		if(publicationDate == null) return null;
		if(publicationDate.trim().length() < 1) return null;
		return publicationDate.trim();
	}

	public String getDate() {
		if(date == null) return null;
		if(date.trim().length() < 1) return null;
		return date.trim();
	}

	public String getCaseNumber() {
		if(caseNumber == null) return null;
		if(caseNumber.trim().length() < 1) return null;
		return caseNumber.trim();
	}
	
	public String getRoNumber() {
		return roNumber;
	}

	public String getRvNumber() {
		return rvNumber;
	}
	
	public String getGuAttr() {
		return guAttr;
	}

	public String getCity() {
		if(city == null) return null;
		if(city.trim().length() < 1) return null;
		return city.trim();
	}

	public String getRegion() {
		if(region == null) return null;
		if(region.trim().length() < 1) return null;
		return region.trim();
	}

	public String getApplicant() {
		if(applicant == null) return null;
		if(applicant.trim().length() < 1) return null;
		return applicant.trim();
	}

	public String getDefendant() {
		if(defendant == null) return null;
		if(defendant.trim().length() < 1) return null;
		return defendant.trim();
	}
	
	public String getAlias() {
		if(alias == null) return null;
		if(alias.trim().length() < 1) return null;
		return alias.trim();
	}
	
	public String getPartition() {
		if(partition == null) return null;
		if(partition.trim().length() < 1) return null;
		return partition.trim();
	}
	
	public String getCelexArticle() {
		
		if(partition == null) return null;
		if(partition.trim().length() < 1) return null;

		int start = partition.toLowerCase().indexOf("articolo-");
		
		if(start < 0) return null;
		
		String article = partition.substring(start + "articolo-".length());
		
		int end = article.indexOf("_");
		
		if(end > 0) article = article.substring(0, end);
		
		return article;
	}
	
	boolean isSkip() {
		return skip;
	}

	public void skip() {
		
		this.skip = true;
	}

	public void unskip() {
		
		this.skip = false;
	}

	/**
	 * Possibili valori: legislation, caselaw, unknown
	 * 
	 * @return
	 */
	public String getRefType() {
		if(refType == null) return "unknown";
		if(refType.trim().length() < 1) return "unknown";
		return refType.trim();
	}

	/**
	 * Possibili valori: internazionale, comunitario, nazionale, regionale, provinciale, comunale, unknown
	 * 
	 * @return
	 */
	public String getRefScope() {
		if(refScope == null) return "unknown";
		if(refScope.trim().length() < 1) return "unknown";
		return refScope.trim();
	}

	public String getOtherAuthority() {
		if(otherAuthority == null) return null;
		if(otherAuthority.trim().length() < 1) return null;
		return otherAuthority.trim();
	}

	public String getFullNumber() {
		if(fullNumber == null) return null;
		if(fullNumber.trim().length() < 1) return null;
		return fullNumber.trim();
	}

	public String getEuAcronym() {
		if(euAcronym == null) return null;
		if(euAcronym.trim().length() < 1) return null;
		return euAcronym.trim();
	}

	public String getMinistry() {
		if(ministry == null) return null;
		if(ministry.trim().length() < 1) return null;
		return ministry.trim();
	}
	
	public String getArea() {
		if(area == null) return null;
		if(area.trim().length() < 1) return null;
		return area.trim();
	}
	
	public String getCitedDocSimpleId() {
		if(citedDocSimpleId == null) return null;
		if(citedDocSimpleId.trim().length() < 1) return null;
		return citedDocSimpleId;
	}

	void setCitedDocSimpleId(String citedDocSimpleId) {
		this.citedDocSimpleId = citedDocSimpleId;
	}

	public String getArticle() {
		if(article == null) return null;
		if(article.trim().length() < 1) return null;
		return article;
	}

	void setArticle(String article) {
		this.article = article;
	}

	public String getComma() {
		if(comma == null) return null;
		if(comma.trim().length() < 1) return null;
		return comma;
	}

	void setComma(String comma) {
		this.comma = comma;
	}

	@Override
	public String toString() {

		/*
		 * Non stampare gli attributi "avanzati/debug": id, ref-type e ref-scope
		 * Non stampare la URL, fai un metodo apposito
		 */
		return toString(false, false);
	}

	public String toString(boolean id, boolean url) {
		
		return printAttributes(id, url);
	}
	
	private String printAttributes(boolean withId, boolean withUrl) {
		
		String attributes = "";
		
		if(withId) {
			
			attributes += "id='" + getId() + "' ";

			attributes += "ref-type='" + getRefType() + "' ";
		
			attributes += "ref-scope='" + getRefScope() + "' ";
		}
		
		if( !Util.isNullOrEmpty(getAlias())) attributes += "alias='" + getAlias() + "' ";
		
		if( !Util.isNullOrEmpty(getDocType())) attributes += "doc-type='" + getDocType() + "' ";
		
		if( !Util.isNullOrEmpty(getAuthority())) attributes += "authority='" + getAuthority() + "' ";
	
		if( !Util.isNullOrEmpty(getRegion())) attributes += "region='" + getRegion() + "' ";
	
		if( !Util.isNullOrEmpty(getCity())) attributes += "city='" + getCity() + "' ";
	
		if( !Util.isNullOrEmpty(getDetachedCity())) attributes += "detached-city='" + getDetachedCity() + "' ";
		
		if( !Util.isNullOrEmpty(getAuthoritySection())) attributes += "section='" + getAuthoritySection() + "' ";
		
		if( !Util.isNullOrEmpty(getMinistry())) attributes += "ministry='" + getMinistry() + "' ";
		
		if( !Util.isNullOrEmpty(getOtherAuthority())) attributes += "other-authority='" + getOtherAuthority() + "' ";
		
		if( !Util.isNullOrEmpty(getNumber())) attributes += "doc-number='" + getNumber() + "' ";
		
		if( !Util.isNullOrEmpty(getYear())) attributes += "doc-year='" + getYear() + "' ";
		
		if( !Util.isNullOrEmpty(getDate())) attributes += "doc-date='" + getDate() + "' ";
		
		if( !Util.isNullOrEmpty(getDepositDate())) attributes += "deposit-date='" + getDepositDate() + "' ";
		
		if( !Util.isNullOrEmpty(getNotificationDate())) attributes += "notification-date='" + getNotificationDate() + "' ";
		
		if( !Util.isNullOrEmpty(getPublicationDate())) attributes += "publication-date='" + getPublicationDate() + "' ";
		
		if( !Util.isNullOrEmpty(getFullNumber())) attributes += "full-number='" + getFullNumber() + "' ";
		
		if( !Util.isNullOrEmpty(getEuAcronym())) attributes += "eu-acronym='" + getEuAcronym() + "' ";
		
		if( !Util.isNullOrEmpty(getCaseNumber())) attributes += "case-number='" + getCaseNumber() + "' ";
		
		if( !Util.isNullOrEmpty(getArea())) attributes += "area='" + getArea() + "' ";
		
		if( !Util.isNullOrEmpty(getRoNumber())) attributes += "ro-number='" + getRoNumber() + "' ";
		
		if( !Util.isNullOrEmpty(getRvNumber())) attributes += "rv-number='" + getRvNumber() + "' ";
		
		if( !Util.isNullOrEmpty(getGuAttr())) attributes += "gu-attr='" + getGuAttr() + "' ";
		
		if( !Util.isNullOrEmpty(getApplicant())) attributes += "applicant='" + getApplicant() + "' ";
		
		if( !Util.isNullOrEmpty(getDefendant())) attributes += "defendant='" + getDefendant() + "' ";
		
		if( !Util.isNullOrEmpty(getPartition())) attributes += "partition='" + getPartition().toLowerCase() + "' ";
		
		if( !Util.isNullOrEmpty(getTitle())) attributes += "title=\"" + getTitle() + "\" ";
		
		if(withUrl) {
	
			String url = getUrl();
			
			if(url != null) attributes += "url='" + url + "' ";
		}
		
		return attributes.trim();
	}
	
	public String getUrl() {
		
		String url = null;
		
		for(Identifier identifier : identifiers) {
			
			if(identifier.getType().equals(Identifiers.URL)) {
				
				url = identifier.getUrl();
				break;
			}

			if(identifier.getType().equals(Identifiers.URN_NIR)) {
				
				url = identifier.getUrl();
				break;
			}
			
			if(identifier.getType().equals(Identifiers.CELEX)) {
				
				url = identifier.getUrl();
				break;
			}

			if(identifier.getType().equals(Identifiers.ECLI)) {
				
				url = identifier.getUrl();
				break;
			}
		}
		
		return url;
	}

	String getJson() {
		
		String json = "\t{";
		
		json += "\n\t\t\"id\":\"" + getId() + "\",";
		
		if( !Util.isNullOrEmpty(getSourceName())) json += "\n\t\t\"source-name\":\"" + getSourceName() + "\",";
		
		if( !Util.isNullOrEmpty(getSourcePartitionId())) json += "\n\t\t\"source-partition-id\":\"" + getSourcePartitionId() + "\",";
		
		json += "\n\t\t\"ref-type\":\"" + getRefType() + "\",";
		
		json += "\n\t\t\"ref-scope\":\"" + getRefScope() + "\",";
		
		if( !Util.isNullOrEmpty(getText())) json += "\n\t\t\"text\":\"" + getText() + "\",";
		
		if( !Util.isNullOrEmpty(getContext())) json += "\n\t\t\"context\":\"" + getContext() + "\",";
		
		if( !Util.isNullOrEmpty(getAlias())) json += "\n\t\t\"alias\":\"" + getAlias() + "\",";
		
		if( !Util.isNullOrEmpty(getPartition())) json += "\n\t\t\"partition\":\"" + getPartition().toLowerCase() + "\",";
		
		if( !Util.isNullOrEmpty(getDocType())) json += "\n\t\t\"doc-type\":\"" + getDocType() + "\",";
		
		if( !Util.isNullOrEmpty(getAuthority())) json += "\n\t\t\"authority\":\"" + getAuthority() + "\",";

		if( !Util.isNullOrEmpty(getRegion())) json += "\n\t\t\"region\":\"" + getRegion() + "\",";

		if( !Util.isNullOrEmpty(getCity())) json += "\n\t\t\"city\":\"" + getCity() + "\",";

		if( !Util.isNullOrEmpty(getDetachedCity())) json += "\n\t\t\"detached-city\":\"" + getDetachedCity() + "\",";
		
		if( !Util.isNullOrEmpty(getAuthoritySection())) json += "\n\t\t\"section\":\"" + getAuthoritySection() + "\",";
		
		if( !Util.isNullOrEmpty(getMinistry())) json += "\n\t\t\"ministry\":\"" + getMinistry() + "\",";
		
		if( !Util.isNullOrEmpty(getOtherAuthority())) json += "\n\t\t\"other-authority\":\"" + getOtherAuthority() + "\",";
		
		if( !Util.isNullOrEmpty(getNumber())) json += "\n\t\t\"number\":\"" + getNumber() + "\",";
		
		if( !Util.isNullOrEmpty(getYear())) json += "\n\t\t\"year\":\"" + getYear() + "\",";
		
		if( !Util.isNullOrEmpty(getDate())) json += "\n\t\t\"doc-date\":\"" + getDate() + "\",";
		
		if( !Util.isNullOrEmpty(getDepositDate())) json += "\n\t\t\"deposit-date\":\"" + getDepositDate() + "\",";
		
		if( !Util.isNullOrEmpty(getNotificationDate())) json += "\n\t\t\"notification-date\":\"" + getNotificationDate() + "\",";
		
		if( !Util.isNullOrEmpty(getPublicationDate())) json += "\n\t\t\"publication-date\":\"" + getPublicationDate() + "\",";
		
		if( !Util.isNullOrEmpty(getFullNumber())) json += "\n\t\t\"full-number\":\"" + getFullNumber() + "\",";
		
		if( !Util.isNullOrEmpty(getEuAcronym())) json += "\n\t\t\"eu-acronym\":\"" + getEuAcronym() + "\",";
		
		if( !Util.isNullOrEmpty(getCaseNumber())) json += "\n\t\t\"case-number\":\"" + getCaseNumber() + "\",";
		
		if( !Util.isNullOrEmpty(getArea())) json += "\n\t\t\"area\":\"" + getArea() + "\",";
		
		if( !Util.isNullOrEmpty(getRoNumber())) json += "\n\t\t\"ro-number\":\"" + getRoNumber() + "\",";
		
		if( !Util.isNullOrEmpty(getRvNumber())) json += "\n\t\t\"rv-number\":\"" + getRvNumber() + "\",";
		
		if( !Util.isNullOrEmpty(getGuAttr())) json += "\n\t\t\"gu-attr\":\"" + getGuAttr() + "\",";
		
		if( !Util.isNullOrEmpty(getApplicant())) json += "\n\t\t\"applicant\":\"" + getApplicant() + "\",";
		
		if( !Util.isNullOrEmpty(getDefendant())) json += "\n\t\t\"defendant\":\"" + getDefendant() + "\",";
		
		String url = getUrl();
		
		if(url != null) json += "\n\t\t\"url\":\"" + url + "\",";

		if( !Util.isNullOrEmpty(getCitedDocSimpleId())) {
			
			json += "\n\t\t\"cited-doc-simple-id\":\"" + getCitedDocSimpleId() + "\",";
			
			if( !Util.isNullOrEmpty(getArticle())) {
				
				json += "\n\t\t\"cited-art-simple-id\":\"" + getCitedDocSimpleId() + "_" + getArticle().toLowerCase() + "\",";
				
				if( !Util.isNullOrEmpty(getComma())) json += "\n\t\t\"cited-comma-simple-id\":\"" + getCitedDocSimpleId() + "_" + getArticle().toLowerCase() + "_" + getComma().toLowerCase() + "\",";
			}
		}
		
		if(json.endsWith(",")) json = json.substring(0, json.length()-1);
		
		json += "\n\t}";
		
		return json;
	}
	

	String getCsv() {
		
		String csv = "\"" + getId() + "\",";
		
		if( !Util.isNullOrEmpty(getSourceName())) csv += "\"" + getSourceName() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getSourcePartitionId())) csv += "\"" + getSourcePartitionId() + "\","; else csv += "\"\",";
		
		csv += "\"" + getRefType() + "\",";
		
		csv += "\"" + getRefScope() + "\",";
		
		if( !Util.isNullOrEmpty(getText())) csv += "\"" + getText() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getContext())) csv += "\"" + getContext() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getAlias())) csv += "\"" + getAlias() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getPartition())) csv += "\"" + getPartition().toLowerCase() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getDocType())) csv += "\"" + getDocType() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getAuthority())) csv += "\"" + getAuthority() + "\","; else csv += "\"\",";

		if( !Util.isNullOrEmpty(getRegion())) csv += "\"" + getRegion() + "\","; else csv += "\"\",";

		if( !Util.isNullOrEmpty(getCity())) csv += "\"" + getCity() + "\","; else csv += "\"\",";

		if( !Util.isNullOrEmpty(getDetachedCity())) csv += "\"" + getDetachedCity() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getAuthoritySection())) csv += "\"" + getAuthoritySection() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getMinistry())) csv += "\"" + getMinistry() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getOtherAuthority())) csv += "\"" + getOtherAuthority() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getEuAcronym())) csv += "\"" + getEuAcronym() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getNumber())) csv += "\"" + getNumber() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getYear())) csv += "\"" + getYear() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getFullNumber())) csv += "\"" + getFullNumber() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getCaseNumber())) csv += "\"" + getCaseNumber() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getArea())) csv += "\"" + getArea() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getDate())) csv += "\"" + getDate() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getDepositDate())) csv += "\"" + getDepositDate() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getNotificationDate())) csv += "\"" + getNotificationDate() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getPublicationDate())) csv += "\"" + getPublicationDate() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getApplicant())) csv += "\"" + getApplicant() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getDefendant())) csv += "\"" + getDefendant() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getRoNumber())) csv += "\"" + getRoNumber() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getRvNumber())) csv += "\"" + getRvNumber() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getGuAttr())) csv += "\"" + getGuAttr() + "\","; else csv += "\"\",";
		
		String url = getUrl();
			
		if(url != null) csv += "\"" + url + "\","; else csv += "\"\",";

		if( !Util.isNullOrEmpty(getCitedDocSimpleId())) csv += "\"" + getCitedDocSimpleId() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getCitedDocSimpleId()) && !Util.isNullOrEmpty(getArticle())) 
			csv += "\"" + getCitedDocSimpleId() + "_" + getArticle().toLowerCase() + "\","; else csv += "\"\",";
		
		if( !Util.isNullOrEmpty(getCitedDocSimpleId()) && !Util.isNullOrEmpty(getArticle()) && !Util.isNullOrEmpty(getComma())) 
			csv += "\"" + getCitedDocSimpleId() + "_" + getArticle().toLowerCase() + "_" + getComma().toLowerCase() + "\""; else csv += "\"\"";
		
		return csv;
	}
	
}
