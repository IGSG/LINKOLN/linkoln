/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

class InputAuthorityService {

	InputAuthorityService() { }
	
	boolean run(LinkolnDocument linkolnDocument) {

		ReferenceClusters referenceClusters = linkolnDocument.getReferenceClusters();
		
		if(referenceClusters == null) return false;
		
		if(linkolnDocument.getInputAuthority() == null) return false;
		
		if(linkolnDocument.isDebug()) System.out.println("Running InputAuthorityService...");
		
		for(ReferenceCluster cluster : referenceClusters.reference2cluster.values()) {
			
			if(cluster.getDocType() != null && (cluster.getAuth() == null || cluster.getAuth().getValue().equals("THIS_COURT") || cluster.getAuth().getValue().equals("COURT")) &&
				(cluster.getDocType().getValue().equals("SENT") || cluster.getDocType().getValue().equals("ORD") ) ) {

				/*
				 * UPDATE 3.3.6 Reso Obsoleto da S171_ReferencesCorteCost e Repository Corte Cost
				 * 
				 * Nota: se input authority è CORTE_COST, il THIS_COURT è già convertito in CORTE COST
				 * 
				if(linkolnDocument.getInputAuthority().equalsIgnoreCase("CORTE_COST")) {
					
					if(cluster.getNumber() != null && ( cluster.getYear() != null || cluster.getDate() != null ) && cluster.getCaseNumber() == null &&
						cluster.getSection() == null && cluster.getSubject() == null && cluster.getApplicant() == null 
						&& cluster.getDetached() == null && cluster.getRegion()  == null && cluster.getCity()  == null) {
						
						//TODO check anno non inferiore a 1956
						//TODO check numero non superiore a una certa soglia?
						
						AnnotationEntity ae = new AnnotationEntity(Entity.CL_AUTH);
						ae.setValue("CORTE_COST");
						cluster.addAuthority(ae);
					}
				}
				*/

				if(linkolnDocument.getInputAuthority().equalsIgnoreCase("CORTE_CASS")) {
					
					if(cluster.getNumber() != null && ( cluster.getYear() != null || cluster.getDate() != null ) && 
					cluster.getDetached() == null && cluster.getRegion()  == null && cluster.getCity()  == null && cluster.getDetached() == null ) {
						
						AnnotationEntity ae = new AnnotationEntity(Entity.CL_AUTH);
						ae.setValue("CORTE_CASS");
						cluster.addAuthority(ae);
					}
					
					//TODO altri pattern? date applicant (già coperto), casenumber applicant ? casenumber date ?
				}
			}
		}
		
		return false;
	}
}
