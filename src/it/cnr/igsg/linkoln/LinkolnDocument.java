/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class LinkolnDocument {
	
	//Configurazione per aquisizione del testo, per la generazione dell'html e per il run di Linkoln

	//TIPO DI INPUT (plain, annotated, marker)
	private InputType inputType = InputType.PLAIN_TEXT;

	//Skips
	private boolean skipWarnings = false;
	private boolean skipRefsWithoutUrl = false;

	//HTML
	private boolean htmlAddHeader = true;
	private boolean htmlUsePre = false;
	private boolean htmlAddArtificialSpaces = true;
	private boolean htmlAddBrTags = true;
	private boolean htmlAddTitles = true;
	private boolean htmlAddAttributes = false;
	private boolean htmlTargetBlank = true;

	//ALTRE CONF di esecuzione
	private boolean loadMunicipalites = true;
	private boolean loadCostRepository = false;
	private boolean loadCeduRepository = true;
	private boolean loadGuRepository = true;
	private boolean useClustering = true;
	private boolean useNearAuthorities = true;
	private boolean useNearRegions = true;
	private boolean debug = false;
	private boolean expandLtGt = false;
	
	//Metadati dell'input
	private String inputAuthority = null;
	private String inputRegion = null;
	private String inputArea = null; //civile o penale
	private String inputDate = null; //nel formato YYYY-MM-GG
	private String sourceName = "";
	
	private Map<String,String> partitionId2text = new HashMap<String,String>(); 
	
	public Map<String,String> getPartitionId2text() {
		
		return Collections.unmodifiableMap(partitionId2text);
	}
	
	boolean isLoadMunicipalites() {
		return loadMunicipalites;
	}

	public void setLoadMunicipalites(boolean loadMunicipalites) {
		this.loadMunicipalites = loadMunicipalites;
	}

	boolean isLoadCostRepository() {
		return loadCostRepository;
	}

	public void setLoadCostRepository(boolean loadCostRepository) {
		this.loadCostRepository = loadCostRepository;
	}
	
	boolean isLoadCeduRepository() {
		return loadCeduRepository;
	}

	public void setLoadCeduRepository(boolean loadCeduRepository) {
		this.loadCeduRepository = loadCeduRepository;
	}

	boolean isLoadGuRepository() {
		return loadGuRepository;
	}

	public void setLoadGuRepository(boolean loadGuRepository) {
		this.loadGuRepository = loadGuRepository;
	}
	
	boolean isUseClustering() {
		return useClustering;
	}

	public void setUseClustering(boolean useClustering) {
		this.useClustering = useClustering;
	}

	boolean isUseNearAuthorities() {
		return useNearAuthorities;
	}

	public void setUseNearAuthorities(boolean useNearAuthorities) {
		this.useNearAuthorities = useNearAuthorities;
	}

	boolean isUseNearRegions() {
		return useNearRegions;
	}

	public void setUseNearRegions(boolean useNearRegions) {
		this.useNearRegions = useNearRegions;
	}

	InputType getInputType() {
		return inputType;
	}
	
	boolean isSkipWarnings() {
		return skipWarnings;
	}

	/**
	 * Attiva o ignora le annotazioni di warning nelle successive richieste di output.
	 * 
	 * @param skipWarnings
	 */
	public void skipWarnings(boolean skipWarnings) {
		this.skipWarnings = skipWarnings;
	}

	boolean isSkipRefsWithoutUrl() {
		return skipRefsWithoutUrl;
	}

	/**
	 * Attiva o ignora le annotazioni relative ai riferimenti senza URL nelle successive richieste di output.
	 * 
	 * @param skipWarnings
	 */
	public void skipRefsWithoutUrl(boolean skipRefsWithoutUrl) {
		this.skipRefsWithoutUrl = skipRefsWithoutUrl;
	}

	boolean isExpandLtGt() {
		return expandLtGt;
	}

	public void setExpandLtGt(boolean expandLtGt) {
		this.expandLtGt = expandLtGt;
	}

	boolean isHtmlAddHeader() {
		return htmlAddHeader;
	}

	void setHtmlAddHeader(boolean htmlAddHeader) {
		this.htmlAddHeader = htmlAddHeader;
	}

	boolean isHtmlUsePre() {
		return htmlUsePre;
	}

	void setHtmlUsePre(boolean htmlUsePre) {
		this.htmlUsePre = htmlUsePre;
	}
	
	boolean isHtmlTargetBlank() {
		return htmlTargetBlank;
	}

	void setHtmlTargetBlank(boolean htmlTargetBlank) {
		this.htmlTargetBlank = htmlTargetBlank;
	}

	boolean isHtmlAddArtificialSpaces() {
		return htmlAddArtificialSpaces;
	}

	void setHtmlAddArtificialSpaces(boolean htmlAddArtificialSpaces) {
		this.htmlAddArtificialSpaces = htmlAddArtificialSpaces;
	}
	
	boolean isHtmlAddBrTags() {
		return htmlAddBrTags;
	}

	void setHtmlAddBrTags(boolean htmlAddBrTags) {
		this.htmlAddBrTags = htmlAddBrTags;
	}
	
	boolean isHtmlAddTitles() {
		return htmlAddTitles;
	}

	void setHtmlAddTitles(boolean htmlAddTitles) {
		this.htmlAddTitles = htmlAddTitles;
	}

	boolean isHtmlAddAttributes() {
		return htmlAddAttributes;
	}

	void setHtmlAddAttributes(boolean htmlAddAttributes) {
		this.htmlAddAttributes = htmlAddAttributes;
	}

	boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	String getInputAuthority() {
		return inputAuthority;
	}

	public void setInputAuthority(String inputAuthority) {
		
		if(inputAuthority != null && !inputAuthority.equalsIgnoreCase("CORTE_COST") && !inputAuthority.equalsIgnoreCase("CORTE_CASS") 
				&& !inputAuthority.equalsIgnoreCase("CORTE_CONTI") && !inputAuthority.equalsIgnoreCase("CONS_STATO")) {
			
			return;
		}
		
		if(inputAuthority != null && inputAuthority.equalsIgnoreCase("CORTE_COST")) {
			
			loadCostRepository = true;
		}
		
		this.inputAuthority = inputAuthority;
	}

	String getInputRegion() {
		return inputRegion;
	}

	public void setInputRegion(String inputRegion) {
		this.inputRegion = inputRegion;
	}
	
	String getInputArea() {
		return inputArea;
	}

	public void setInputArea(String inputArea) {
		this.inputArea = inputArea;
	}
	
	String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		
		if(inputDate == null) {
			
			this.inputDate = null;
			return;
		}
		
		inputDate = inputDate.trim();
		
		if(inputDate.length() != 10) {
			
			this.inputDate = null;
			return;
		}
		
		Date date = null;
		try {
		    
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		    
		    date = sdf.parse(inputDate);
		    
		    if (!inputDate.equals(sdf.format(date))) {
		        date = null;
		    }
		    
		} catch (ParseException ex) {
		    ex.printStackTrace();
		}
		
		if (date == null) {

			this.inputDate = null;
			
			return;
		}
		
		this.inputDate = inputDate;
	}

	String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		
		if(sourceName == null) {
			
			this.sourceName = null;
			return;
		}
		
		sourceName = sourceName.trim();
		
		//ECLI-IT-COST-1982-64.xml
		
		if(sourceName.startsWith("ECLI-")) {
			
			/* Rimuove il prefisso "ECLI-" e l'estensione del file */
			
			sourceName = sourceName.substring("ECLI-".length());
			
			int cut = sourceName.indexOf(".");
			
			if(cut > 3) sourceName = sourceName.substring(0, cut);

			if(sourceName.startsWith("IT-COST")) setInputAuthority("CORTE_COST");
			if(sourceName.startsWith("IT-CASS")) setInputAuthority("CORTE_CASS");
		}
		
		this.sourceName = sourceName;
	}
	
	private String failure = "";

	void setFailure(String failure) {
		this.failure = failure;
	}

	public String getFailure() {
		
		return failure;
	}

	boolean hasFailed() {
		
		if(failure == null || failure.trim().length() == 0) return false;
		
		return true;
	}
	
	private String inputText = "";
	
	String getInputText() {
		
		return inputText;
	}

	//Il testo alla fine della pipeline
	private String annotatedText = ""; 	
	
	private String htmlDebugText = null;
	
	String getAnnotatedText() {
		return annotatedText;
	}

	void setAnnotatedText(String annotatedText) {
		this.annotatedText = annotatedText;
	}

	public String getHtmlAnnotatedText() {
		
		//conf default
		Map<HtmlConf,Boolean> conf = new HashMap<HtmlConf,Boolean>();
		
		conf.put(HtmlConf.ADD_HEADER, true);
		conf.put(HtmlConf.ADD_PRE, false);
		conf.put(HtmlConf.ADD_ARTIFICIAL_SPACES, true);
		conf.put(HtmlConf.ADD_BR_TAGS, true);
		conf.put(HtmlConf.ADD_TITLES, true);
		conf.put(HtmlConf.ADD_ATTRIBUTES, true);
		conf.put(HtmlConf.TARGET_BLANK, true);
		conf.put(HtmlConf.EXPAND_LT_GT, false);
		
		return getHtmlAnnotatedText(conf);
	}
	
	public String getHtmlAnnotatedText(Map<HtmlConf,Boolean> conf) {
		
		if(conf != null) {
			
			if(conf.get(HtmlConf.ADD_HEADER) != null && !conf.get(HtmlConf.ADD_HEADER)) setHtmlAddHeader(false); else setHtmlAddHeader(true);
			if(conf.get(HtmlConf.ADD_PRE) != null && conf.get(HtmlConf.ADD_PRE)) setHtmlUsePre(true); else setHtmlUsePre(false);
			if(conf.get(HtmlConf.ADD_ARTIFICIAL_SPACES) != null && !conf.get(HtmlConf.ADD_ARTIFICIAL_SPACES)) setHtmlAddArtificialSpaces(false); else setHtmlAddArtificialSpaces(true);
			if(conf.get(HtmlConf.ADD_BR_TAGS) != null && !conf.get(HtmlConf.ADD_BR_TAGS)) setHtmlAddBrTags(false); else setHtmlAddBrTags(true);
			if(conf.get(HtmlConf.ADD_TITLES) != null && !conf.get(HtmlConf.ADD_TITLES)) setHtmlAddTitles(false); else setHtmlAddTitles(true);
			if(conf.get(HtmlConf.ADD_ATTRIBUTES) != null && conf.get(HtmlConf.ADD_ATTRIBUTES)) setHtmlAddAttributes(true); else setHtmlAddAttributes(false);
			if(conf.get(HtmlConf.TARGET_BLANK) != null && conf.get(HtmlConf.TARGET_BLANK)) setHtmlTargetBlank(true); else setHtmlTargetBlank(false);
		}
		
		return Pipeline.getHtmlAnnotatedText(this);
	}

	public String getHtmlDebug() {
		return htmlDebugText;
	}

	void setHtmlDebugText(String htmlDebugText) {
		this.htmlDebugText = htmlDebugText;
	}

	public String getJson() {
		
		String json = "[\n" + Pipeline.getJson(this) + "\n]\n"; 
		
		return json;
	}

	public String getCsv() {
	
		return getCsv(true);
	}

	public String getCsv(boolean addHeader) {
		
		String csv = "";
		
		if(addHeader) csv = "\"id\",\"source-name\",\"source-partition-id\",\"ref-type\",\"ref-scope\",\"text\",\"context\",\"alias\",\"partition\",\"doc-type\",\"authority\",\"region\",\"city\",\"detached-city\",\"section\",\"ministry\",\"other-authority\",\"eu-acronym\",\"number\",\"year\",\"full-number\",\"case-number\",\"area\",\"doc-date\",\"deposit-date\",\"notification-date\",\"publication-date\",\"applicant\",\"defendant\",\"ro-number\",\"rv-number\",\"gu-series\",\"url\",\"cited-doc-simple-id\",\"cited-art-simple-id\",\"cited-comma-simple-id\"\n";
		
		csv += Pipeline.getCsv(this);
		
		return csv;
	}

	public String getLinkolnAnnotatedText() {
		
		return Pipeline.getLinkolnAnnotatedText(this);
	}
	
	private long executionTime = 0;
	
	public long getExecutionTime() {
		return executionTime;
	}

	void setExecutionTime(long executionTime) {
		this.executionTime = executionTime;
	}

	void setText(String text) {
		
		inputText = text;
	}
	
	/*
	 * Constructor
	 */
	
	LinkolnDocument(InputType inputType) {
		
		if(inputType == null) inputType = InputType.PLAIN_TEXT;
		
		this.inputType = inputType;
		
		this.referenceClusters = new ReferenceClusters(this);
	}
	
	/*
	 * Text Annotations
	 */
	protected ArrayList<AnnotationEntity> entities = new ArrayList<AnnotationEntity>();
	
	protected Map<String,AnnotationEntity> id2entity = new HashMap<String,AnnotationEntity>();
	
	private int entityId = 0;
	
	private String getNextId() {
		
		entityId++;
		
		return String.valueOf(entityId);
	}

	void addAnnotationEntity(AnnotationEntity entity, boolean store) {
		
		if( !entity.getId().equals("") && id2entity.get(entity.getId()) != null) {
			
			//AnnotationEntity già presente nel linkoln document, non fare altro
			return;
		}

		//Nuova Annotation Entity
		
		String id = getNextId();
		
		entity.setId(id);
		
		if(store) {
			
			//Salva l'oggetto in memoria
			
			entities.add(entity);
			id2entity.put(entity.getId(), entity);
		}
	}

	void replaceAnnotationEntity(AnnotationEntity oldEntity, AnnotationEntity newEntity) {
		
		removeAnnotationEntity(oldEntity);
		
		entities.add(newEntity);
		id2entity.put(newEntity.getId(), newEntity);
	}

	void removeAnnotationEntity(AnnotationEntity oldEntity) {
		
		id2entity.remove(oldEntity.getId());
		entities.remove(oldEntity);
	}

	AnnotationEntity getAnnotationEntity(String id) {
		
		return id2entity.get(id);
	}
	
	Collection<AnnotationEntity> getAnnotationEntities() {
		
		return Collections.unmodifiableCollection(entities);
	}
	
	private Map<String,LinkolnReference> idReference2linkolnReference = new HashMap<String,LinkolnReference>();
	
	void addReference2LinkolnReference(Reference reference, LinkolnReference linkolnReference) {
		
		if(reference == null || linkolnReference == null) return;
		
		idReference2linkolnReference.put(reference.getId(), linkolnReference);
	}
	
	LinkolnReference getLinkolnReferenceFromId(String id) {
		
		if(id == null) return null;
	
		return idReference2linkolnReference.get(id);
	}
	
	private ReferenceClusters referenceClusters = null;

	ReferenceClusters getReferenceClusters() {
		return referenceClusters;
	}

	private Collection<LinkolnReference> linkolnReferences = new ArrayList<LinkolnReference>();
	
	boolean addLinkolnReference(LinkolnReference linkolnReference) {
		
		//Nota: per il momento l'id degli oggetti linkolnReference è generato semplicemente così
		linkolnReference.setId(linkolnReferences.size() + 1);
		
		//Salva nel linkolnReference anche l'eventuale sourceName
		if(sourceName != null && sourceName.trim().length() > 0) linkolnReference.setSourceName(sourceName);
		
		//TODO aggiornare in futuro
		linkolnReference.setSourcePartitionId(costCurrentId);
		
		if(partitionId2text.get(costCurrentId) == null) partitionId2text.put(costCurrentId, costCurrentText);
		
		return linkolnReferences.add(linkolnReference);
	}
	
	public Collection<LinkolnReference> getReferences() {
		
		return Collections.unmodifiableCollection(linkolnReferences);
	}
	
	/*
	 * XML-MARKER Documents - Context
	 */
	
	Reference contextReference = null;
	AnnotationEntity contextArticle = null;
	AnnotationEntity contextComma = null;
	
	boolean contextSkip = false;
	
	void resetContext() {
		
		contextReference = null;
		contextArticle = null;
		contextComma = null;
		contextSkip = false;
	}
	
	/*
	 * XML-MARKER-COST Documents
	 */
	
	String costCurrentId = "";
	String costCurrentText = "";
	
	/*
	 * Contiene riferimenti al codice civile o penale.
	 */
	boolean hasCodiceCivile = false;
	boolean hasCodicePenale = false;
	
}
