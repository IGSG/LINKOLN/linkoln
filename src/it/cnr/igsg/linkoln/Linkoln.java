/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

public class Linkoln {

	final public static String VERSION = "3.4.0";
	final private static String NAME = "IGSG Linkoln";
	
	final public static String getInfo() { return NAME + " " + VERSION; }

	private Linkoln() { }
	
	public static boolean run(LinkolnDocument linkolnDocument) {
		
		long startTime = System.currentTimeMillis();
		
		if(linkolnDocument.isDebug()) System.out.println("Running LINKOLN (" + VERSION + ")...\n");
		
		if(linkolnDocument.isDebug()) System.out.println("InputType: " + linkolnDocument.getInputType().toString() + "\n");
		
		if(linkolnDocument.isLoadMunicipalites()) {
		
			if(linkolnDocument.isDebug()) System.out.print("Loading municipalities... ");
			
			if(Util.initMunicipalities() && Util.token2code != null) {
				
				if(linkolnDocument.isDebug()) System.out.println("Done. (" + Util.token2code.size() + ")");
			
			} else {
				
				if(linkolnDocument.isDebug()) System.out.println("File not found!");
			}
		}
		
		if(linkolnDocument.isLoadCostRepository()) {
			
			if(linkolnDocument.isDebug()) System.out.print("Loading COST Repository... ");
			
			if(Repository.initCostRepository() && Repository.costRepository != null) {
				
				if(linkolnDocument.isDebug()) System.out.println("Done. (" + Repository.costRepository.size() + ")");
			
			} else {
				
				if(linkolnDocument.isDebug()) System.out.println("File not found!");
			}
		}
		
		if(linkolnDocument.isLoadCeduRepository()) {
			
			if(linkolnDocument.isDebug()) System.out.print("Loading CEDU Repository... ");
			
			if(Repository.initCeduRepository() && Repository.date_country2recordsCedu != null) {
				
				if(linkolnDocument.isDebug()) System.out.println("Done. (" + Repository.ecli2recordCedu.size() + ")");
			
			} else {
				
				if(linkolnDocument.isDebug()) System.out.println("File not found!");
			}
		}
		
		if(linkolnDocument.isLoadGuRepository()) {
			
			if(linkolnDocument.isDebug()) System.out.print("Loading GU Repository... ");
			
			if(Repository.initGuRepository() && Repository.guRepository != null) {
				
				if(linkolnDocument.isDebug()) System.out.println("Done. (" + Repository.guRepository.size() + ")");
			
			} else {
				
				if(linkolnDocument.isDebug()) System.out.println("File not found!");
			}
		}
		
		Pipeline.run(linkolnDocument);
		
		if(linkolnDocument.hasFailed()) return false;
		
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		
		linkolnDocument.setExecutionTime(elapsedTime);
		
		return true;
	}
}
