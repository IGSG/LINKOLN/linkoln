/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

class ReferenceCluster {

	//Cluster di references con features compatibili e condivise.

	//Nota: partition e rvNumber non devono essere condivisi.
	
	private Set<Reference> references = new HashSet<Reference>();
	
	private AnnotationEntity auth = null;
	private AnnotationEntity section = null;
	private AnnotationEntity detached = null;
	private AnnotationEntity otherAuth = null;
	private AnnotationEntity docType = null;
	private AnnotationEntity number = null;
	private AnnotationEntity fullNumber = null;
	private AnnotationEntity year = null;
	private AnnotationEntity euAcronym = null;
	private AnnotationEntity date = null;
	private AnnotationEntity depositDate = null;
	private AnnotationEntity notificationDate = null;
	private AnnotationEntity publicationDate = null;
	private AnnotationEntity caseNumber = null;
	private AnnotationEntity applicant = null;
	private AnnotationEntity defendant = null;
	private AnnotationEntity ministry = null;
	private AnnotationEntity region = null;
	private AnnotationEntity city = null;
	private AnnotationEntity subject = null;

	private ReferenceCluster() { }
	
	private LinkolnDocument linkolnDocument = null;
	
	ReferenceCluster(LinkolnDocument linkolnDocument) {
		
		this.linkolnDocument = linkolnDocument;
	}
	
	Collection<Reference> getReferences() {
		
		return references;
	}
	
	/*
	 * Metodi per l'aggiunta a posteriori dei valori derivanti dai NEAR SERVICES.
	 */
	void addGeo(AnnotationEntity ae) { 
	
		if(ae.getEntity().equals(Entity.REGION) && region == null) {
		
			region = ae;
		}
		
		if(ae.getEntity().equals(Entity.CITY) && city == null) {
			
			city = ae;
		}
	}

	void addAuthority(AnnotationEntity ae) {
		
		if(auth == null) {
			
			auth = ae;

			//Aggiungi anche l'eventuale sezione contenuta nell'authority  <-- NOTA: affinchè funzioni la section deve essere annotata dentro l'authority
			if(ae.getRelatedEntity(Entity.CL_SECTION) != null && section == null) {
				
				section = ae.getRelatedEntity(Entity.CL_SECTION); 
			}
			
			//Aggiungi anche l'eventuale indicazione geografica contenuta nell'authority
			if(ae.getRelatedEntity(Entity.CITY) != null && city == null) {
				
				city = ae.getRelatedEntity(Entity.CITY); 
			}
			
			if(ae.getRelatedEntity(Entity.REGION) != null && region == null) {
				
				region = ae.getRelatedEntity(Entity.REGION); 
			}
			
			//Non inserire nel cluster i valori artificiali THIS_COURT, THIS_TRIBUNAL e COURT
			if(auth != null && (auth.getValue().startsWith("THIS") || auth.getValue().equals("COURT")) ) {
				
				auth = null;
			}
			
			/*
			 * Se il clustering è disattivato è necessario aggiungere esplicitamente i valori dell'authority
			 * nei singoli reference contenuti nel Cluster.
			 */
			if(auth != null && !linkolnDocument.isUseClustering()) {
				
				for(Reference reference : references) reference.addRelatedEntity(auth);
				
				//TODO sezione e indicazioni geografiche?
			}
		}
	}

	//Aggiungi una nuova reference al cluster
	void add(Reference reference) {
		
		ReferenceClusters referenceClusters = linkolnDocument.getReferenceClusters();
		
		if(referenceClusters == null) return;
		
		references.add(reference);

		if(auth == null) {
			
			auth = referenceClusters.getAuthority(reference);
			
			//Non inserire nel cluster i valori artificiali HIS_COURT, THIS_TRIBUNAL e COURT
			if(auth != null && (auth.getValue().startsWith("THIS") || auth.getValue().equals("COURT")) ) {
				
				auth = null;
			}
		}
		
		if(otherAuth == null) {
			
			otherAuth = referenceClusters.getOtherAuthority(reference);
		}
		
		if(docType == null) {
			
			docType = referenceClusters.getDocumentType(reference);
		}
		
		if(section == null) {
				
			section = referenceClusters.getAuthSection(reference);
		}
		
		if(detached == null) {
				
			detached = referenceClusters.getDetCity(reference);
		}
		
		if(number == null) {
			
			number = referenceClusters.getNumber(reference);
		}
		
		if(fullNumber == null || 
			( referenceClusters.getFullNumber(reference) != null && referenceClusters.getFullNumber(reference).getValue().length() > fullNumber.getValue().length() ) ) {
			
			fullNumber = referenceClusters.getFullNumber(reference);
		}
		
		if(euAcronym == null || 
			( referenceClusters.getEuAcronym(reference) != null && referenceClusters.getEuAcronym(reference).getValue().length() > euAcronym.getValue().length() ) ) {
			
			euAcronym = referenceClusters.getEuAcronym(reference);
		}
		
		if(year == null) {
			
			year = referenceClusters.getYear(reference);
		}
		
		if(date == null) {
				
			date = referenceClusters.getDate(reference);
		}
		
		if(depositDate == null) {
			
			depositDate = referenceClusters.getDepositDate(reference);
		}
		
		if(notificationDate == null) {
			
			notificationDate = referenceClusters.getNotificationDate(reference);
		}

		if(publicationDate == null) {
			
			publicationDate = referenceClusters.getPublicationDate(reference);
		}

		if(caseNumber == null ||
			( referenceClusters.getCaseNumber(reference) != null && referenceClusters.getCaseNumber(reference).getValue().length() > caseNumber.getValue().length() ) ) {
			
			caseNumber = referenceClusters.getCaseNumber(reference);
		}
		
		if(applicant == null ||
			( referenceClusters.getApplicant(reference) != null && referenceClusters.getApplicant(reference).getValue().length() > applicant.getValue().length() ) ) {
			
			applicant = referenceClusters.getApplicant(reference);
		}
		
		if(defendant == null ||
			( referenceClusters.getDefendant(reference) != null && referenceClusters.getDefendant(reference).getValue().length() > defendant.getValue().length() ) ) {
			
			defendant = referenceClusters.getDefendant(reference);
		}

		if(ministry == null ||
			( referenceClusters.getMinistry(reference) != null && referenceClusters.getMinistry(reference).getValue().length() > ministry.getValue().length() ) ) {
			
			ministry = referenceClusters.getMinistry(reference);
		}
		
		if(region == null) {
			
			region = referenceClusters.getRegion(reference);
		}
		
		if(city == null) {
			
			city = referenceClusters.getCity(reference);
		}
		
		if(subject == null) {
			
			subject = referenceClusters.getSubject(reference);
		}
	}


	AnnotationEntity getAuth() {
		return auth;
	}

	AnnotationEntity getSection() {
		return section;
	}

	AnnotationEntity getDetached() {
		return detached;
	}

	AnnotationEntity getDocType() {
		return docType;
	}

	AnnotationEntity getNumber() {
		return number;
	}

	AnnotationEntity getYear() {
		return year;
	}

	AnnotationEntity getDate() {
		return date;
	}

	AnnotationEntity getDepositDate() {
		return depositDate;
	}

	AnnotationEntity getNotificationDate() {
		return notificationDate;
	}

	AnnotationEntity getPublicationDate() {
		return publicationDate;
	}

	AnnotationEntity getCaseNumber() {
		return caseNumber;
	}

	AnnotationEntity getApplicant() {
		return applicant;
	}

	AnnotationEntity getDefendant() {
		return defendant;
	}

	AnnotationEntity getRegion() {
		return region;
	}

	AnnotationEntity getCity() {
		return city;
	}

	AnnotationEntity getSubject() {
		return subject;
	}

	AnnotationEntity getOtherAuth() {
		return otherAuth;
	}

	AnnotationEntity getFullNumber() {
		return fullNumber;
	}

	AnnotationEntity getEuAcronym() {
		return euAcronym;
	}

	AnnotationEntity getMinistry() {
		return ministry;
	}
}
