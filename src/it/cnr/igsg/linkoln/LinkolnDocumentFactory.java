/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class LinkolnDocumentFactory {

	public static LinkolnDocument createLinkolnDocument(String text) {
		
		return createLinkolnDocument(text, null);
	}

	public static LinkolnDocument createLinkolnDocument(String text, InputType inputType) {

		return getLinkolnDocument(text, inputType);
	}
	
	public static LinkolnDocument createLinkolnDocumentFromFile(String fileName) {
		
		return createLinkolnDocumentFromFile(fileName, null, null);
	}
	
	public static LinkolnDocument createLinkolnDocumentFromFile(String fileName, InputType inputType) {
		
		return createLinkolnDocumentFromFile(fileName, inputType, null);
	}
	
	public static LinkolnDocument createLinkolnDocumentFromFile(String fileName, String inputCharSet) {
		
		return createLinkolnDocumentFromFile(fileName, null, inputCharSet);
	}
	
	public static LinkolnDocument createLinkolnDocumentFromFile(String fileName, InputType inputType, String inputCharSet) {
		
		String text = null;
		
		inputCharSet = Util.normalizeCharSet(inputCharSet);
		
		try {

			if(inputCharSet == null) {

				//Utilizza il charset di default dell'environment
				text = new String(Files.readAllBytes(Paths.get(fileName)));
			
			} else {
				
				//Java 8
				text = new String(Files.readAllBytes(Paths.get(fileName)), inputCharSet);
			}
			
		} catch (IOException e) {
			
			System.err.println("File error or not found: " + fileName);
			
			return null;
		}

		return getLinkolnDocument(text, inputType);
	}
	
	private static LinkolnDocument getLinkolnDocument(String text, InputType inputType) {
		
		//Se inputType non è dichiarato, take a guess
		if(inputType == null) inputType = Util.guessInputType(text);

		LinkolnDocument linkolnDocument = new LinkolnDocument(inputType);

		linkolnDocument.setText(text);
		
		//default configurations
		
		if(linkolnDocument.getInputType().equals(InputType.XML_IGSG_MARKER)) {
			
			linkolnDocument.skipWarnings(true);
		}

		if(linkolnDocument.getInputType().equals(InputType.PLAIN_TEXT) || linkolnDocument.getInputType().equals(InputType.HYBRID_TEXT)) {
			
			linkolnDocument.setHtmlAddHeader(true);
		}

		return linkolnDocument;
	}
}
