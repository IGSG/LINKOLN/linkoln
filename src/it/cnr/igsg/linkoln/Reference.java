/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

import java.util.HashMap;
import java.util.Map;

class Reference extends AnnotationEntity {
	
	Reference(Entity entity) {

		super(entity);

		if( !entity.equals(Entity.REF) && !entity.equals(Entity.LEG_REF) && !entity.equals(Entity.EU_LEG_REF) 
				&& !entity.equals(Entity.CL_REF) && !entity.equals(Entity.EU_CL_REF) && !entity.equals(Entity.LEG_REF_PART) && !entity.equals(Entity.EU_LEG_REF_PART)) {
			
			System.err.println("Invalid use of Reference(Entity entity) constructor - invalid entity: " + entity);
		}
	}

	/*
	 * UPGRADE utilizzato per:
	 * 
	 * - una partial reference che diventa una vera e propria reference;
	 * 
	 * - una reference che diventa reference+partition
	 * 
	 */
	void upgrade(Entity entity) {
		
		if(entity == null) return;
		
		if( !entity.equals(Entity.LEG_REF) && !entity.equals(Entity.EU_LEG_REF)	&& !entity.equals(Entity.CL_REF) && !entity.equals(Entity.EU_CL_REF)
				 && !entity.equals(Entity.LEG_REF_PART) && !entity.equals(Entity.EU_LEG_REF_PART) ) {
			
			return;
		}
		
		this.entity = entity;
	}

	//Indica soltanto che la reference è stata istanziata dal modulo PartialReferences (per potenziale uso interno)
	private boolean partialReference = false;
	
	boolean isPartialReference() {
		return partialReference;
	}

	void setPartialReference(boolean partialReference) {
		this.partialReference = partialReference;
	}

	private String context = "";
	
	final String getContext() {
		return context;
	}

	final void setContext(String context) {
		this.context = context;
	}
	
	final void addContext(String context) {
		this.context += context;
	}

	Map<Entity,AnnotationEntity> name2sharedEntity = new HashMap<Entity,AnnotationEntity>();
	
	//Indica se può essere considerata una reference al fine di istanziare un nuovo oggetto reference all'interno dei vari moduli di riconoscimento dei pattern delle Reference.
	boolean isReference() {
		
		boolean hasAuthority = false;
		boolean hasDocType = false;
		boolean hasNumber = false;
		boolean hasDate = false;
		
		if(getRelatedEntity(Entity.LEG_ALIAS) != null || getRelatedEntity(Entity.EU_LEG_ALIAS) != null || getRelatedEntity(Entity.CL_ALIAS) != null || getRelatedEntity(Entity.EU_CL_ALIAS) != null) {
			
			return true;
		}
		
		//Nota: CL_SECTION agisce da authority
		//TODO in realtà si deve controllare se all'interno di SECTION c'è un'authority related (es. AD_PLENARIA -> CDS, FERIALE -> CASS)
				
		if(getRelatedEntity(Entity.CL_AUTH) != null || getRelatedEntity(Entity.LEG_AUTH) != null || getRelatedEntity(Entity.EU_CL_AUTH) != null || getRelatedEntity(Entity.EU_LEG_AUTH) != null || getRelatedEntity(Entity.OTHER_AUTH) != null
				|| getRelatedEntity(Entity.CL_SECTION) != null) {
			
			hasAuthority = true;
		}
		
		if(name2sharedEntity.get(Entity.CL_AUTH) != null || name2sharedEntity.get(Entity.LEG_AUTH) != null || name2sharedEntity.get(Entity.EU_CL_AUTH) != null || name2sharedEntity.get(Entity.EU_LEG_AUTH) != null || name2sharedEntity.get(Entity.OTHER_AUTH) != null
				|| name2sharedEntity.get(Entity.CL_SECTION) != null) {
			
			hasAuthority = true;
		}
				
		if(getRelatedEntity(Entity.DOCTYPE) != null || getRelatedEntity(Entity.CL_DOCTYPE) != null || getRelatedEntity(Entity.LEG_DOCTYPE) != null || getRelatedEntity(Entity.EU_LEG_DOCTYPE) != null) {
			
			hasDocType = true;
		}
		
		if(name2sharedEntity.get(Entity.DOCTYPE) != null || name2sharedEntity.get(Entity.CL_DOCTYPE) != null || name2sharedEntity.get(Entity.LEG_DOCTYPE) != null || name2sharedEntity.get(Entity.EU_LEG_DOCTYPE) != null) {
			
			hasDocType = true;
		}
		
		if(getRelatedEntity(Entity.NUMBER) != null || getRelatedEntity(Entity.NUMBER_YEAR) != null || getRelatedEntity(Entity.FULL_NUMBER) != null || getRelatedEntity(Entity.FULL_EU_NUMBER) != null
				|| getRelatedEntity(Entity.CASENUMBER) != null || getRelatedEntity(Entity.ALTNUMBER) != null) {
			
			hasNumber = true;
		}
		
		if(getRelatedEntity(Entity.DOC_DATE) != null) {
			
			hasDate = true;
		}
		
		if( (hasAuthority || hasDocType) && (hasNumber || hasDate) ) {
			
			return true;
		}
		
		return false;
	}
	
}
