/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

class IdentifierGenerationUrl {

	static boolean run(LinkolnDocument linkolnDocument, LinkolnReference linkolnReference) {
		
		Identifier identifier = new Identifier();
		identifier.setType(Identifiers.URL);
	
		String url = "";
		
		//ALIAS
		String alias = linkolnReference.getAlias();
		
		if(alias != null) {
			
			if(alias.equals("CONV_EU_DIR_UOMO")) url = "https://presidenza.governo.it/CONTENZIOSO/contenzioso_europeo/documentazione/Convenzione.pdf";
			
			if(alias.equals("CONV_INFANZIA")) url = "https://web.archive.org/web/20161130180750/http://www.unicef.it/Allegati/Convenzione_diritti_infanzia_1.pdf";
			
			if(alias.equals("CONV_PAESAGGIO")) url = "https://ecomuseipiemonte.wordpress.com/wp-content/uploads/2014/06/convenzione_europea_paesaggio.pdf";
			
			if(alias.equals("CONV_DISABIL")) url = "https://www.lavoro.gov.it/temi-e-priorita/disabilita-e-non-autosufficienza/focus-on/Convenzione-ONU/Documents/Convenzione%20ONU.pdf";
			
			
			if(alias.equals("CARTA_DIR_FOND_UE")) url = "https://www.europarl.europa.eu/charter/pdf/text_it.pdf";
			
			if(alias.equals("DICHIARAZ_DIR_UOMO")) url = "https://www.ohchr.org/sites/default/files/UDHR/Documents/UDHR_Translations/itn.pdf";
			
			
			
			if(alias.equals("NORME_INT_CORTE_COST")) url = "https://www.gazzettaufficiale.it/eli/id/2021/11/03/21A06516/sg";
			
			//TODO Gazzetta Ufficiale 24 marzo 1956, n. 71, edizione speciale 
			//if(alias.equals("NORME_INT_CORTE_COST_1956")) url = "https://www.gazzettaufficiale.it/eli/id/2021/11/03/21A06516/sg";
			
			if(alias.equals("NORME_INT_CORTE_COST_2008")) url = "https://www.gazzettaufficiale.it/eli/id/2008/11/07/08A08080/sg";
			
			if( !url.equals("")) {
				
				identifier.setCode(url);
				identifier.setUrl(url);
				
				linkolnReference.addIdentifier(identifier);
				
				return true;
			}
		}
		
		//Riferimenti a fascicoli di Gazzetta Ufficiale
		
		String docType = linkolnReference.getDocType();
		
		if(docType != null && docType.equalsIgnoreCase("GU")) {
			
			String guAttr = linkolnReference.getGuAttr();
			
			String numero = linkolnReference.getNumber();
			
			String anno = linkolnReference.getYear();
			
			String data = linkolnReference.getDate();
			
			//http://www.gazzettaufficiale.it/gazzetta/serie_generale/caricaDettaglio?dataPubblicazioneGazzetta=2007-02-26&numeroGazzetta=47
			
			//ESISTE ANCHE QUESTA FORMA:
			//http://www.gazzettaufficiale.it/eli/gu/2018/03/10/58/sg/pdf
			//ovvero
			//http://www.gazzettaufficiale.it/eli/gu/2018/03/10/58/sg
			
			//SERIE GENERALE
			
			//return "http://www.gazzettaufficiale.it/gazzetta/serie_generale/caricaDettaglio?dataPubblicazioneGazzetta=" + guDate + "&numeroGazzetta=" + guNumber;
			
			if(numero != null && data != null) {
				
				if(guAttr == null || guAttr.equalsIgnoreCase("SG") || guAttr.equalsIgnoreCase("SUPPL_ORD")) {
					
					//SERIE GENERALE
					
					//http://www.gazzettaufficiale.it/gazzetta/serie_generale/caricaDettaglio?dataPubblicazioneGazzetta=2007-02-26&numeroGazzetta=47
					
					/*
					 * Supplementi ordinari: sono inseriti nel fascicolo ordinario, vengono aggiunti in fondo, quindi si può riferirsi al fascicolo ordinario.
					 */
	
					data = "dataPubblicazioneGazzetta=" + data;
					
					/*
					 * Utilizzare &amp; altrimenti SAXParseException
					 */
					//numero = "&numeroGazzetta=" + numero;
					numero = "&amp;numeroGazzetta=" + numero;
					
					url = "https://www.gazzettaufficiale.it/gazzetta/serie_generale/caricaDettaglio?" + data + numero;
					
					identifier.setCode(url);
					identifier.setUrl(url);
					
					linkolnReference.addIdentifier(identifier);
					
					return true;
				}
				
				if(guAttr.startsWith("EU_")) {
					
					//Ignora i riferimenti a Gazzetta Ufficiale dell'Unione Europea
					
					return false;
				}
				
				if(guAttr.equalsIgnoreCase("1_SS")) {
					
					//PRIMA SERIE SPECIALE (Corte Costituzionale)
					
					//https://www.gazzettaufficiale.it/gazzetta/corte_costituzionale/caricaDettaglio?dataPubblicazioneGazzetta=2022-11-23&numeroGazzetta=47
	
					data = "dataPubblicazioneGazzetta=" + data;
					
					numero = "&amp;numeroGazzetta=" + numero;
					
					url = "https://www.gazzettaufficiale.it/gazzetta/corte_costituzionale/caricaDettaglio?" + data + numero;
					
					identifier.setCode(url);
					identifier.setUrl(url);
					
					linkolnReference.addIdentifier(identifier);
					
					return true;
				}
			
			} else {
				
				if(numero != null && anno != null) {
					
					if(guAttr == null || guAttr.equalsIgnoreCase("SG") || guAttr.equalsIgnoreCase("SUPPL_ORD")) {
						
						//SERIE GENERALE

						//Usa il Repository GU
						
						String key = numero + "_" + anno;
						
						String uri = Repository.guRepository.get(key);
						
						if(uri != null) {
	
							identifier.setCode(uri);
							identifier.setUrl(uri);
							
							linkolnReference.addIdentifier(identifier);
							
							return true;
						}					
					}
				}
			}
		}
		
		return false;
	}
}
