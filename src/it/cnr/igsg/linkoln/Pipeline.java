/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.file.Paths;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

class Pipeline {
	
	/*
	 * Pipeline dei servizi di analisi del testo di Linkoln.
	 */

	static void run(LinkolnDocument linkolnDocument) {
		
		if(linkolnDocument.getInputType().equals(InputType.XML_IGSG_MARKER)) {
			
			LinkolnAnnotationService mr = new MarkerReader();
			
			mr.setLinkolnDocument(linkolnDocument);
			mr.setInput(linkolnDocument.getInputText());

			mr.run();
			
			linkolnDocument.setAnnotatedText(mr.getOutput());
			
		} else if(linkolnDocument.getInputType().equals(InputType.XML_IGSG_COST)) {
			
			if(linkolnDocument.getSourceName() != null && linkolnDocument.getSourceName().startsWith("IT-COST")) {
				
				String year = Util.readFirstNumber(linkolnDocument.getSourceName());
				String num = Util.readSecondNumber(linkolnDocument.getSourceName());
				
				linkolnDocument.setInputDate(Repository.getCostDate(year, num));
				
				if(linkolnDocument.isDebug() && linkolnDocument.getInputDate() != null && !linkolnDocument.getInputDate().equals("")) 
					System.out.println("inputDate: " + linkolnDocument.getInputDate());
			}
			
			LinkolnAnnotationService mcr = new MarkerCostReader();
			
			mcr.setLinkolnDocument(linkolnDocument);
			mcr.setInput(linkolnDocument.getInputText());

			mcr.run();
			
			linkolnDocument.setAnnotatedText(mcr.getOutput());
			
		} else {
			
			run(linkolnDocument, linkolnDocument.getInputText(), 0);
		}
	}
	
	static boolean run(LinkolnDocument linkolnDocument, String text, int position) {
		
		LinkolnAnnotationService service = new S002_PreProcessing();
		text = runService(linkolnDocument, service, text, position, false);
		
		if(linkolnDocument.getInputType().equals(InputType.HTML_DOCUMENT) || linkolnDocument.getInputType().equals(InputType.XML_DOCUMENT) || linkolnDocument.getInputType().equals(InputType.HYBRID_TEXT)
				|| linkolnDocument.getInputType().equals(InputType.XML_IGSG_MARKER) || linkolnDocument.getInputType().equals(InputType.XML_IGSG_COST)) {
			
			service = new S003_PreProcessingAnnotated();
			text = runService(linkolnDocument, service, text, position);
		}
		
		service = new S004_Dates();
		text = runService(linkolnDocument, service, text, position);

		service = new S006_MaskAcronymsIgnoreNumbers();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S008_NumberedItems();
		text = runService(linkolnDocument, service, text, position);

		service = new S009_Sentences();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S010_Points();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S011_Parts();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S012_Letters();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S014_Paragraphs();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S016_Commas();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S018_Articles();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S020_Annexes();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S024_Stopwords();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S026_UnmaskAcronyms();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S030_Geos();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S034_DetachedSections();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S038_CaseLawAuthorities();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S040_Sections();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S044_LegislationAuthorities();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S046_RegionalCaseLawAuthorities();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S048_RegionalLegislationAuthorities();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S054_UnnumberItems();
		text = runService(linkolnDocument, service, text, position);

		service = new S060_CaseNumbersCGUE();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S062_CaseNumbers();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S064_RvNumbers();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S066_FullEuropeanNumbers();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S068_FullNumbers();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S070_NumberYears();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S072_Years();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S074_Numbers();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S078_Journals();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S080_Aliases();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S100_Ministries();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S102_JointMinistries();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S104_NationalAuthorities();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S108_DocTypeAuth();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S110_DocTypes();
		text = runService(linkolnDocument, service, text, position);

		service = new S120_Subjects();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S130_Abbreviations();
		text = runService(linkolnDocument, service, text, position);
		
		if(Util.token2code != null && linkolnDocument.isLoadMunicipalites()) {
		
			service = new S140_Municipalities();
			text = runService(linkolnDocument, service, text, position);
		}
		
		service = new S142_ExtendDetachedAuthorities();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S144_ExtendAuthorities();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S148_ArticleNumbersWithoutPrefix();
		text = runService(linkolnDocument, service, text, position);

		service = new S150_Vs();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S152_NamedEntities();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S154_Parties();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S160_ReferencesEuropeanLegislation();
		text = runService(linkolnDocument, service, text, position);

		service = new S162_ReferencesLocalLegislation();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S164_ReferencesMinistries();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S166_ReferencesPrimaryLegislation();
		text = runService(linkolnDocument, service, text, position);

		service = new S170_ReferencesLocalCaseLaw();
		text = runService(linkolnDocument, service, text, position);
		
		if(linkolnDocument.getInputAuthority() != null && linkolnDocument.getInputAuthority().equalsIgnoreCase("CORTE_COST")) {
			
			service = new S171_ReferencesCorteCost();
			text = runService(linkolnDocument, service, text, position);
		}
		
		service = new S172_ReferencesPrimaryCaseLaw();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S174_ReferencesOtherAuthority();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S176_PartialReferences();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S182_AliasPartitions();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S184_ReferencesAlias();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S198_MergePartitions();
		text = runService(linkolnDocument, service, text, position);

		service = new S200_Partitions();
		text = runService(linkolnDocument, service, text, position);

		//Generazione dei Cluster 1-1 con gli oggetti Reference ed eventuale raggruppamento in base alle features solo se USE_CLUSTERING=true 
		linkolnDocument.getReferenceClusters().generateClusters();
				
		//Estensione della ricerca delle regioni/città nel resto del testo
		if(linkolnDocument.isUseNearRegions()) {
			
			NearGeoService nearGeoService = new NearGeoService();
			nearGeoService.run(linkolnDocument);
		}
		
		//Estensione della ricerca delle autorità emananti nel testo che precede una reference parziale o senza autorità
		if(linkolnDocument.isUseNearAuthorities()) {
			
			service = new S202_NearAuthorities();
			((S202_NearAuthorities) service).referenceClusters = linkolnDocument.getReferenceClusters();
			runService(linkolnDocument, service, text, position, false);
		}

		//Integrazione dell'autorità in situazioni specifiche se è stato impostato inputAuthority per il linkolnDocument
		InputAuthorityService inputAuthorityService = new InputAuthorityService();
		inputAuthorityService.run(linkolnDocument);
		
		//Nota: gli Warning devono essere annotati anche se gli warnings sono disabilitati con skipWarnings

		service = new S204_PartialWarnings();
		((S204_PartialWarnings) service).referenceClusters = linkolnDocument.getReferenceClusters();
		text = runService(linkolnDocument, service, text, position);

		service = new S206_AddOfPartitionsToReferences();
		text = runService(linkolnDocument, service, text, position);
		
		service = new S208_AddPartitionsToReferences();
		text = runService(linkolnDocument, service, text, position);
		
		if(linkolnDocument.getInputType().equals(InputType.XML_IGSG_MARKER)) {
			
			service = new MarkerReadPartition();
			text = runService(linkolnDocument, service, text, position);
		}

		service = new S212_Titles();
		text = runService(linkolnDocument, service, text, position);

		service = new S214_Warnings();
		text = runService(linkolnDocument, service, text, position);

		//Creazione degli oggetti LinkolnReference in corrispondenza delle Reference/Cluster che soddisfano i criteri oppure downgrade a warning
		service = new S216_CreateLinkolnReference();
		runService(linkolnDocument, service, text, position, false);

		if(linkolnDocument.isDebug()) {

			//Generazione dell'output versione HTML-DEBUG
			service = new S220_HtmlDebugRenderer();
			String html = runService(linkolnDocument, service, text, position, false);
			html = configureHtml(linkolnDocument, html);
			linkolnDocument.setHtmlDebugText(html);
		}
		
		//Pulizia del testo dalle altre annotazioni inserite da Linkoln
		service = new S240_CleanAnnotations();
		text = runService(linkolnDocument, service, text);
		
		linkolnDocument.setAnnotatedText(text);
		
		return true;
	}

	static String getHtmlAnnotatedText(LinkolnDocument linkolnDocument) {
		
		String text = linkolnDocument.getAnnotatedText();
		
		if(text == null) return null;
		
		String output = null;
		
		if(linkolnDocument.getInputType().equals(InputType.XML_IGSG_MARKER)) {
			
			//Trasforma direttamente da marker-xml a html
			
			text = getLinkolnAnnotatedText(linkolnDocument);
			output = buildHtml(text, false);
			
		} else if(linkolnDocument.getInputType().equals(InputType.XML_IGSG_COST)) {
			
			//Trasforma direttamente da marker-cost-xml a html
			
			text = getLinkolnAnnotatedText(linkolnDocument);
			output = buildHtml(text, true);
			
		} else {
			
			//Generazione dell'output versione HTML
			LinkolnAnnotationService service = new S218_HtmlRenderer();
			output = runService(linkolnDocument, service, text, false);
			output = configureHtml(linkolnDocument, output);
		}
		
		return output;	
	}

	static String getLinkolnAnnotatedText(LinkolnDocument linkolnDocument) {
		
		String text = linkolnDocument.getAnnotatedText();
		
		if(text == null) return null;
		
		//Annotazione con il tag <lkn:ref> e relativi attributi in corrispondenza delle linkoln references
		LinkolnAnnotationService service = new S230_LknRefAnnotations();

		return runService(linkolnDocument, service, text, false);
	}
	
	static String getJson(LinkolnDocument linkolnDocument) {
		
		String text = linkolnDocument.getAnnotatedText();
		
		if(text == null) return null;
		
		//Generazione dell'output versione JSON
		LinkolnAnnotationService service = new S224_JsonRenderer();
		String output = runService(linkolnDocument, service, text, false);
		
		return output;
	}
	
	static String getCsv(LinkolnDocument linkolnDocument) {
		
		String text = linkolnDocument.getAnnotatedText();
		
		if(text == null) return null;
		
		//Generazione dell'output versione CSV
		LinkolnAnnotationService service = new S226_CsvRenderer();
		String output = runService(linkolnDocument, service, text, false);
		
		return output;
	}

	private static String runService(LinkolnDocument linkolnDocument, LinkolnAnnotationService service, String text) {
		
		return runService(linkolnDocument, service, text, 0, true);
	}
	
	private static String runService(LinkolnDocument linkolnDocument, LinkolnAnnotationService service, String text, int position) {
		
		return runService(linkolnDocument, service, text, position, true);
	}
	
	private static String runService(LinkolnDocument linkolnDocument, LinkolnAnnotationService service, String text, boolean checkAnnotations) {
		
		return runService(linkolnDocument, service, text, 0, checkAnnotations);
	}
	
	private static String runService(LinkolnDocument linkolnDocument, LinkolnAnnotationService service, String text, int position, boolean checkAnnotations) {
		
		long start = System.currentTimeMillis();
		
		if(linkolnDocument.isDebug()) System.out.println("\nRunning " + service.getClass().getSimpleName() + "...");
		
		if(linkolnDocument.isDebug()) System.out.println("BEFORE: " + text);
		
		//Inserisci spazi artificiali
		//Add blanks at the beginning and the the end of the input for edge parsing 
		text = " " + text + " ";
		
		service.setLinkolnDocument(linkolnDocument);
		service.setInput(text);
		service.setPosition(position);
		service.run();
		
		String output = service.getOutput();
		
		if(checkAnnotations && !checkAnnotations(linkolnDocument, text, output, service.getClass().getName())) {
		
			return "";
		}

		//Remove the artificial blanks
		if(output.length() > 1) {
		
			if(output.substring(0, 1).equals(" ")) {
				
				output = output.substring(1);
			}

			if(output.substring(output.length()-1, output.length()).equals(" ")) {
				
				output = output.substring(0, output.length()-1);
			}
		}
		
		if(checkAnnotations && linkolnDocument.isDebug()) System.out.println(" AFTER: " + output);
		
		long end = System.currentTimeMillis();
		
		if(linkolnDocument.isDebug()) System.out.println(service.getClass().getSimpleName() + "\t" + (end-start));
		
		return output;
	}
	
	private static boolean checkAnnotations(LinkolnDocument linkolnDocument, String beforeXml, String afterXml, String serviceName) {
		
		String before = Util.removeAllAnnotations(beforeXml);
		String after = Util.removeAllAnnotations(afterXml);
		
		if( !after.equals(before)) {
			
			System.out.println("\n\nCheck text failed (" + serviceName + ").");
			
			linkolnDocument.setFailure(serviceName);
			
			if(linkolnDocument.isDebug()) {

				System.out.println("\n\nBEFORE:\n\n<<<\n" + beforeXml + "\n>>>");
				System.out.println("\n\n AFTER:\n\n<<<\n" + afterXml + "\n>>>");
				//System.out.println("\n\nbefore:\n\n<<<\n" + before + "\n>>>");
				//System.out.println("\n\n after:\n\n<<<\n" + after + "\n>>>");
				System.out.println("\n\nCheck text failed. Equal part:\n" + Util.getEqualPart(after, before));
			}
			
			return false;
		}
		
		return true;
	}
	
	private static String configureHtml(LinkolnDocument linkolnDocument, String text) {
		
		String output = text;
		
		if(linkolnDocument.getInputType().equals(InputType.PLAIN_TEXT) || linkolnDocument.getInputType().equals(InputType.HYBRID_TEXT)) {
			
			if(linkolnDocument.isHtmlAddBrTags()) {
			
				//Sostituisci i ritorni a capo con BR 
				output = output.replaceAll("\r\n", "<br/>").replaceAll("\n", "<br/>");
			}
			
			String preOpen = "";
			String preClose = "";
			
			if(linkolnDocument.isHtmlUsePre()) {
				
				//Usa gli spazi preformattati
				
				preOpen = "<pre>";
				preClose = "</pre>";

			} else {

				if(linkolnDocument.isHtmlAddArtificialSpaces()) {

					//Crea spazi artificiali per gli spazi multipli ed i tabs
				
					output = output.replaceAll("\\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
					output = output.replaceAll("\\s\\s", "&nbsp;&nbsp;");
				}
			}
			
			String header = "";
			String footer = "";
			
			if(linkolnDocument.isHtmlAddHeader()) {
				
				//Add an HTML header and footer for plain texts
				header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML//EN\" \"xhtml-strict.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<meta><title>" + Linkoln.getInfo() + "</title></meta><body>" + preOpen + "\n";
				footer = "\n" + preClose + "</body></html>";
			}
			
			output = header + output + footer;
		}
		
		return output;
	}

	private static String buildHtml(String xml, boolean cost) {
		
		if(xml == null || xml.trim().length() < 1) return null;
		
		String html = null;
		
		try {
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);

			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(new InputSource(new StringReader(xml)));
			
			html = transform(doc, cost);
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return html;
	}
	
	final static private String XSLT_FILENAME = "xsl/marker.xsl";
	
	final static private String XSLT_COST_FILENAME = "xsl/mrkc.xsl";
	
	private static String transform(Document doc, boolean cost) throws TransformerException {

		File xsltFile = Paths.get(XSLT_FILENAME).toFile();
		
		if(cost) xsltFile = Paths.get(XSLT_COST_FILENAME).toFile();
			
		String htmlMetaDirName = "../HTML-META/";

		StringWriter outWriter = new StringWriter();

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		
		Transformer transformer = null;
		
		if(xsltFile.exists()) {
			
	        transformer = transformerFactory.newTransformer(new StreamSource(xsltFile));
	        
	        if(cost) transformer.setParameter("meta_html_folder", htmlMetaDirName);
	
		} else {

			if(cost) {
				
				transformer = transformerFactory.newTransformer(new StreamSource(Pipeline.class.getResourceAsStream("/" + XSLT_COST_FILENAME)));
				
				transformer.setParameter("meta_html_folder", htmlMetaDirName);
			
			} else {
				
				transformer = transformerFactory.newTransformer(new StreamSource(Pipeline.class.getResourceAsStream("/" + XSLT_FILENAME)));				
			}
		}
		
        transformer.transform(new DOMSource(doc), new StreamResult(outWriter));
        
        return outWriter.getBuffer().toString();
    }
}
