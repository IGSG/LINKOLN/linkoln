/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

class IdentifierGenerationUrn {

	static boolean run(LinkolnDocument linkolnDocument, LinkolnReference linkolnReference) {
		
		Identifier identifier = new Identifier();
		identifier.setType(Identifiers.URN_NIR);
		
		String urnNir = "urn:nir:";
		String urlPrefix = "http://www.normattiva.it/uri-res/N2Ls?";
		
		
		String urnAuth = "";
		String urnType = "";
		String urnDate = "";
		String urnNumber = "";
		//String urnVersion = "";
		String urnPartition = "";
		String urnAllegato = "";
		
		
		/*
		 *
		 * EVENTUALE PARTIZIONE
		 * 
		 */

		String part = linkolnReference.getPartition();
		
		if(part != null) {
			
			urnPartition = part.toLowerCase();
			
			if(urnPartition.startsWith("all") || urnPartition.startsWith("tabella") 
					|| urnPartition.startsWith("tariffa") || urnPartition.startsWith("protocollo")) {
				
				urnAllegato = ":1"; //default nel caso non sia specificato un allegato specifico

				String annex = urnPartition;
				
				if(annex.indexOf("_") > -1) {
					
					annex = annex.substring(0, annex.indexOf("_"));
					
					//taglia la dichiarazione di allegato dal resto di partition value
					urnPartition = urnPartition.substring(urnPartition.indexOf("_") + 1);
				
				} else {
				
					//nella partition era specificato soltanto l'allegato
					urnPartition = "";
				}

				if(annex.indexOf("-") > -1) {
					annex = annex.substring(annex.indexOf("-") + 1);
					
					urnAllegato = ":" + annex;
				}
			}
			
			urnPartition = urnPartition.replaceAll("articolo", "art");
			urnPartition = urnPartition.replaceAll("lettera", "let");
			urnPartition = urnPartition.replaceAll("numero", "num");
			urnPartition = urnPartition.replaceAll("-", "");
			urnPartition = urnPartition.replaceAll("_", "-");
			
			if( !urnPartition.equals("")) {
				
				urnPartition = "~" + urnPartition;
			}
		}

		/*
		 * VIGENZA
		 * 
		 * Sintassi urn-nir: !vig=2022-11-22
		 * 
		 */
		
		String vigenza = "";
		if(linkolnDocument.getInputDate() != null) {
			
			 vigenza = "!vig=" + linkolnDocument.getInputDate();
		}

		String docType = linkolnReference.getDocType();

		/*
		 * ALIAS LEGISLATIVI e statuti regionali
		 */
		
		if(linkolnReference.getAlias() != null || (docType != null && docType.equals("STATUTO"))) {
			
			String docUrn = getAliasCode(linkolnReference);
			
			if(docUrn == null) {
				
				//URN dell'alias legislativo non trovato
				
				return false;
			
			} else {
			
				urnNir += docUrn + urnPartition + vigenza;
				
				identifier.setCode(urnNir);
				identifier.setUrl(urlPrefix + urnNir);
				
				linkolnReference.addIdentifier(identifier);
				
				return true;
			}
		}
		
		/*
		 * RIFERIMENTI LEGISLATIVI STANDARD 
		 */
		
		String auth = linkolnReference.getAuthority();
		String number = linkolnReference.getNumber();
		String date = linkolnReference.getDate();
		String year = linkolnReference.getYear();

		if(year == null && date != null) {
			
			year = date.substring(0, 4);
		}
		
		if(docType == null || number == null || year == null) {
			
			return false;
		}
		
		if(!docType.equals("L") && !docType.equals("LC") && !docType.equals("DL") && !docType.equals("DLGS") && !docType.equals("DECR") 
				&& !docType.equals("TU") && !docType.equals("REG")) {
			
			return false;
		}
		
		if(auth != null && !auth.equals("PRES_REP") && !auth.equals("PRES_CONS_MIN") && !auth.equals("CAPO_GOV") && !auth.equals("CAPO_PROVV_STATO") && !auth.equals("LUOGOTENENTE") && !auth.equals("RE") 
				&& !auth.equals("MINISTERO") && !auth.startsWith("REGIONE") && !auth.startsWith("PROVINCIA") ) {
			
			return false;
		}
		
		//Agenzie, etc.
		if(linkolnReference.getOtherAuthority() != null) {
			
			return false;
		}
		
		if(docType != null) {

			if(docType.equals("L")) { urnType = "legge"; urnAuth = "stato"; }
			if(docType.equals("DL")) { urnType = "decreto.legge"; urnAuth = "stato"; }
			if(docType.equals("DLGS")) { urnType = "decreto.legislativo"; urnAuth = "stato"; }
			if(docType.equals("LC")) { urnType = "legge.costituzionale"; urnAuth = "stato"; }
			if(docType.equals("DECR")) { urnType = "decreto"; urnAuth = "stato"; }
			if(docType.equals("TU")) { urnType = "testo.unico"; urnAuth = "stato"; }
			if(docType.equals("REG")) { urnType = "regolamento"; urnAuth = "stato"; }
		}
		
		if(auth != null) {
			
			if(auth.equals("PRES_REP")) urnAuth = "presidente.repubblica";
			if(auth.equals("PRES_CONS_MIN")) urnAuth = "presidente.consiglio";
			if(auth.equals("CAPO_GOV")) urnAuth = "capo.governo";
			if(auth.equals("CAPO_PROVV_STATO")) urnAuth = "capo.provvisorio.stato";
			if(auth.equals("LUOGOTENENTE")) urnAuth = "luogotenente";
			if(auth.equals("RE")) urnAuth = "re";
			
			if(auth.equals("MINISTERO")) {
				
				urnAuth = "ministero";
				
				String ministry = linkolnReference.getMinistry();

				if(ministry != null) {
					
					String[] ministries = ministry.split(";");
					
					for(int i = 0; i < ministries.length; i++) {
						
						urnAuth += "." + ministries[i].replaceAll("_", ".").toLowerCase();
					}
				}
			}
			
			if(auth.startsWith("REGIONE")) {
				
				//TODO Analizzare separatamente i casi speciali Trenito Alto Adige, Trento e Bolzano ?
				
				urnAuth = "regione";
				
				String region = linkolnReference.getRegion();

				if(region != null) {
					
					urnAuth += "." + Util.convertRegionToNir(region);
					
					/*
					if(region.equals("TOS")) {
						
						//Risolve sul portale Raccolta Normativa della Regione Toscana
						
						urlPrefix = "http://raccoltanormativa.consiglio.regione.toscana.it/articolo?urndoc=";
						
						//Il portale on supporta le partizioni
						urnPartition = "";
						vigenza = "";
					}
					*/

				} else {
					
					//Se la regione non è specificata, non comporre la URN
					
					return false;
				}
			}
			
			if(auth.startsWith("PROVINCIA")) {
				
				urnAuth = "provincia";
				
				String city = linkolnReference.getCity();

				if(city != null) {
					
					urnAuth += "." + Util.convertCityToNir(city);
										
				} else {
					
					//Se la città non è specificata, non comporre la URN
					
					return false;
				}
			}
		}
		
		urnNumber = number;
		urnDate = year;
		
		urnNir += urnAuth + ":" + urnType + ":" + urnDate + ";" + urnNumber + urnAllegato + urnPartition + vigenza; 
		
		identifier.setCode(urnNir);
		identifier.setUrl(urlPrefix + urnNir);

		linkolnReference.addIdentifier(identifier);
		
		return true;
	}

	private static String getAliasCode(LinkolnReference linkolnReference) {
		
		String alias = linkolnReference.getAlias();
		
		//String number = linkolnReference.getNumber();
		String date = linkolnReference.getDate();
		String year = linkolnReference.getYear();

		if(year == null && date != null) {
			
			year = date.substring(0, 4);
		}
		
		String docType = linkolnReference.getDocType();
		
		if(docType != null && docType.equals("STATUTO")) {
			
			String region = linkolnReference.getRegion();

			if(region != null) {

				//TODO Mancano alcuni statuti regionali

				//abruzzo
				if(region.equals("BAS")) return "stato:legge:1971-05-22;350:1";
				//calabria
				//campania
				if(region.equals("EMR")) return "stato:legge:1971;342:1";
				if(region.equals("FVG")) return "stato:legge.costituzionale:1963-01-31;1";
				if(region.equals("LAZ")) return "stato:legge:1971;346:1";
				if(region.equals("LIG")) return "stato:legge:1971-05-22;341:1";
				if(region.equals("LOM")) return "stato:legge:1971;339:1";
				//marche
				//molise
				if(region.equals("PIE")) return "stato:legge:1971-05-22;338:1";
				if(region.equals("PUG")) return "stato:legge:1971-05-22;349:1";
				//if(region.equals("SAR")) return "stato:legge:1971-05-22;343:1";
				if(region.equals("SAR")) return "stato:legge.costituzionale:1948-02-26;3";
				if(region.equals("SIC")) return "stato:regio.decreto.legislativo:1946-05-15;455:1";
				//if(region.equals("TOS")) return "stato:legge.costituzionale:1948-02-26;3";
				if(region.equals("TOS")) return "stato:legge:1971-05-22;343:1";
				if(region.equals("TAA")) return "stato:legge.costituzionale:1948-02-26;5";
				if(region.equals("UMB")) return "regione.umbria:legge:1971-05-22;344:1";
				if(region.equals("VDA")) return "stato:legge.costituzionale:1948-02-26;4";
				if(region.equals("VEN")) return "stato:legge:1971-05-22;340:1";				
			}
			
			return null;
		}
		
		/*
		 * ALIAS LEGISLATIVI
		 * 
		 * TODO verificare e terminare
		 * 
		 */
		
		if(alias.equals("COST")) return "stato:costituzione:1947-12-27";
		
		if(alias.equals("COD_CIV")) return "stato:regio.decreto:1942-03-16;262:2";
		if(alias.equals("COD_PEN")) return "stato:regio.decreto:1930-10-19;1398:1";
		if(alias.equals("COD_PROC_CIV")) return "stato:regio.decreto:1940-10-28;1443:1";
		if(alias.equals("COD_PROC_PEN")) return "stato:decreto.del.presidente.della.repubblica:1988-09-22;447";
		if(alias.equals("COD_PROC_PEN_1930")) return "stato:regio.decreto:1930-10-19;1399";
		if(alias.equals("DISP_ATT_COD_CIV")) return "stato:regio.decreto:1942-03-30;318:1";		
		if(alias.equals("DISP_ATT_COD_PROC_CIV")) return "stato:regio.decreto:1941-08-25;1368:1"; 
		
		/*
		
		http://www.normattiva.it/uri-res/N2Ls?urn:nir:ministero.grazia.e.giustizia:decreto:1989-09-30;334
		MINISTERO DI GRAZIA E GIUSTIZIA
		DECRETO 30 settembre 1989, n. 334
		Regolamento per l'esecuzione del codice di procedura penale. (GU n.233 del 5-10-1989 )
		note:
		Entrata in vigore del decreto: 24/10/1989
		 */
		
		if(alias.equals("DISP_ATT_COD_PROC_PEN")) return "stato:decreto.legislativo:1989-07-28;271:1";
		
		/*
		
		http://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:regio.decreto:1941-09-09;1023
		REGIO DECRETO 9 settembre 1941, n. 1023
		Disposizioni di coordinamento, transitorie e di attuazione dei Codici penali militari di pace e di guerra. (041U1023) (GU n.229 del 27-9-1941 - Suppl. Ordinario n. 229 )
		note:
		Entrata in vigore del provvedimento: 01/10/1941
		 */
		
		if(alias.equals("COD_MIL_GUERRA")) return "stato:relazione.e.regio.decreto:1941-02-20;303:1";
		if(alias.equals("COD_MIL_PACE")) return "stato:relazione.e.regio.decreto:1941-02-20;303:1";
		
		/*
		CODICE POSTALE E DELLE TELECOMUNICAZIONI
		https://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:decreto.del.presidente.della.repubblica:1973-03-29;156:1
		DECRETO DEL PRESIDENTE DELLA REPUBBLICA 29 marzo 1973, n. 156
		Approvazione del testo unico delle disposizioni legislative in materia postale, di bancoposta e di telecomunicazioni.
		
				
		http://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:decreto.del.presidente.della.repubblica:1992-12-16;495
		DECRETO DEL PRESIDENTE DELLA REPUBBLICA 16 dicembre 1992, n. 495
		Regolamento di esecuzione e di attuazione del nuovo codice della strada. (GU n.303 del 28-12-1992 - Suppl. Ordinario n. 134 )
		note:
		Entrata in vigore del decreto: 1-1-1993
		
		http://www.normattiva.it/uri-res/N2Ls?urn:nir:ministero.sviluppo.economico:decreto:2010-01-13;33
		MINISTERO DELLO SVILUPPO ECONOMICO
		DECRETO 13 gennaio 2010, n. 33
		Regolamento di attuazione del Codice della proprieta' industriale, adottato con decreto legislativo 10 febbraio 2005, n. 30. (10G0044) (GU n.56 del 9-3-2010 - Suppl. Ordinario n. 48 )
		note:
		Entrata in vigore del provvedimento: 10/3/2010
		
		http://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:decreto.legislativo:2006-04-03;152
		DECRETO LEGISLATIVO 3 aprile 2006, n. 152
		Norme in materia ambientale. (GU n.88 del 14-4-2006 - Suppl. Ordinario n. 96 )
		note:
		Entrata in vigore del provvedimento: 29/4/2006, ad eccezione delle disposizioni della Parte seconda che entrano in vigore il 12/8/2006.
		
		http://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:decreto.del.presidente.della.repubblica:2010-10-05;207
		DECRETO DEL PRESIDENTE DELLA REPUBBLICA 5 ottobre 2010, n. 207
		Regolamento di esecuzione ed attuazione del decreto legislativo 12 aprile 2006, n. 163, recante «Codice dei contratti pubblici relativi a lavori, servizi e forniture in attuazione delle direttive 2004/17/CE e 2004/18/CE». (10G0226) (GU n.288 del 10-12-2010 - Suppl. Ordinario n. 270 )
		note:
		Entrata in vigore del provvedimento 08/06/2011, ad esclusione degli articoli 73 e 74 che entrano in vigore il 25/12/2010.
		 */
		
		if(alias.equals("COD_NAVIG")) return "stato:regio.decreto:1942-03-30;327:1";
		if(alias.equals("REG_COD_NAVIG")) return "stato:decreto.del.presidente.della.repubblica:1952-02-15;328:1";
		if(alias.equals("COD_STRADA")) return "stato:decreto.legislativo:1992-04-30;285";
		if(alias.equals("COD_PROT_DATI")) return "stato:decreto.legislativo:2003-06-30;196";
		if(alias.equals("COD_COM_ELETTR")) return "stato:decreto.legislativo:2003-08-01;259";
		if(alias.equals("COD_BENI_CULT")) return "stato:decreto.legislativo:2004-01-22;42";
		if(alias.equals("COD_PROPR_INDUSTRIALE")) return "stato:decreto.legislativo:2005-02-10;30";
		if(alias.equals("COD_AMM_DIGIT")) return "stato:decreto.legislativo:2005-03-07;82";
		if(alias.equals("COD_NAUTICA_DIPORTO")) return "stato:decreto.legislativo:2005-07-18;171";
		if(alias.equals("COD_CONSUMO")) return "stato:decreto.legislativo:2005-09-06;206";
		if(alias.equals("COD_ASSICURAZIONI_PRIV")) return "stato:decreto.legislativo:2005-09-07;209";
		if(alias.equals("COD_CRISI_IMPRESA")) return "stato:decreto.legislativo:2019-01-12;14";
		
		
		// nuovo Codice dei Contratti pubblici in vigore dal: 19-4-2016
		if(alias.equals("COD_CONTR_PUBBL")) return "stato:decreto.legislativo:2016-04-18;50"; 	
		// vecchio Codice dei Contratti Pubblici in vigore fino al: 18-4-2016 
		//if(alias.equals("COD_CONTR_PUBBL")) return "stato:decreto.legislativo:2006-04-12;163"; 
		
		if(alias.equals("COD_PARI_OPPOR")) return "stato:decreto.legislativo:2006-04-11;198";
		if(alias.equals("COD_ORDINAM_MIL")) return "stato:decreto.legislativo:2010-03-15;66";
		if(alias.equals("COD_PROCESSO_AMM")) return "stato:decreto.legislativo:2010-07-02;104:2";
		if(alias.equals("COD_DEONT_FORENSE")) return "";
		
		/*
		CODICE DEL PROCESSO TRIBUTARIO
		http://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:decreto.legislativo:1992-12-31;546
		DECRETO LEGISLATIVO 31 dicembre 1992, n. 546
		Disposizioni sul processo tributario in attuazione della delega al Governo contenuta nell'art. 30 della legge 30 dicembre 1991, n. 413. (GU n.9 del 13-1-1993 - Suppl. Ordinario n. 8 )
		note:
		Entrata in vigore del decreto: 15-1-1993
		
		CODICE DEL TURISMO
		http://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:decreto.legislativo:2011-05-23;79:1
		DECRETO LEGISLATIVO 23 maggio 2011, n. 79
		Codice della normativa statale in tema di ordinamento e mercato del turismo, a norma dell'articolo 14 della legge 28 novembre 2005, n. 246, nonche' attuazione della direttiva 2008/122/CE, relativa ai contratti di multiproprieta', contratti relativi ai prodotti per le vacanze di lungo termine, contratti di rivendita e di scambio. (11G0123) (GU n.129 del 6-6-2011 - Suppl. Ordinario n. 139 )
		note:
		Entrata in vigore del provvedimento: 21/06/2011
		
		CODICE ANTIMAFIA
		http://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:decreto.legislativo:2011-09-06;159
		DECRETO LEGISLATIVO 6 settembre 2011, n. 159
		Codice delle leggi antimafia e delle misure di prevenzione, nonche' nuove disposizioni in materia di documentazione antimafia, a norma degli articoli 1 e 2 della legge 13 agosto 2010, n. 136. (11G0201) (GU n.226 del 28-9-2011 - Suppl. Ordinario n. 214 )
		note:
		Entrata in vigore del provvedimento: 13/10/2011.
		Le disposizioni del libro II, capi I, II, III e IV entrano in vigore secondo quanto disposto dall'art. 119.
		
		CODICE DI GIUSTIZIA CONTABILE
		http://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:decreto.legislativo:2016-08-26;174:1
		DECRETO LEGISLATIVO 26 agosto 2016, n. 174
		Codice di giustizia contabile, adottato ai sensi dell'articolo 20 della legge 7 agosto 2015, n. 124. (16G00187) (GU n.209 del 7-9-2016 - Suppl. Ordinario n. 41 )
		note:
		Entrata in vigore del provvedimento: 07/10/2016
		
		CODICE DEL TERZO SETTORE
		http://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:decreto.legislativo:2017-07-03;117
		DECRETO LEGISLATIVO 3 luglio 2017, n. 117
		Codice del Terzo settore, a norma dell'articolo 1, comma 2, lettera b), della legge 6 giugno 2016, n. 106. (17G00128) (GU n.179 del 2-8-2017 - Suppl. Ordinario n. 43 )
		note:
		Entrata in vigore del provvedimento: 03/08/2017
		
		CODICE DELLA PROTEZIONE CIVILE
		https://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:decreto.legislativo:2018-01-02;1
		DECRETO LEGISLATIVO 2 gennaio 2018, n. 1
		Codice della protezione civile. (18G00011)
		
		CODICE DELLA CRISI D'IMPRESA E DELL'INSOLVENZA
		https://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:decreto.legislativo:2019-01-12;14
		DECRETO LEGISLATIVO 12 gennaio 2019, n. 14
		Codice della crisi d'impresa e dell'insolvenza in attuazione della legge 19 ottobre 2017, n. 155. 
		
		 */

		if(alias.equals("TU_DOCUMENTAZIONE_AMM")) return "presidente.repubblica:decreto:2000-12-28;445";
		if(alias.equals("TU_ESPROPRIAZIONE_PUBBL")) return "presidente.repubblica:decreto:2001-06-08;327";
		if(alias.equals("TU_EDILIZIA")) return "presidente.repubblica:decreto:2001-06-06;380";
		if(alias.equals("TU_CIRCOLAZ_EU")) return "presidente.repubblica:decreto:2002-01-18;54";
		if(alias.equals("TU_SPESE_GIUST")) return "presidente.repubblica:decreto:2002-05-30;115";
		if(alias.equals("TU_CASELLARIO_GIUDIZ")) return "presidente.repubblica:decreto:2002-11-14;313";
		if(alias.equals("TU_DEBITO_PUBBL")) return "presidente.repubblica:decreto:2003-12-30;398";
		if(alias.equals("TU_ORDINAM_MIL")) return "presidente.repubblica:decreto:2010-03-15;90";
		if(alias.equals("TU_LEGGI_CONS_STATO")) return "stato:regio.decreto:1924-06-26;1054:1";
		if(alias.equals("TU_PUBBL_SICUREZZA")) return "stato:regio.decreto:1931-06-18;773:1";
		if(alias.equals("TU_PESCA")) return "stato:regio.decreto:1931-10-08;1604:1";
		if(alias.equals("TU_AVVOCATURA_STATO")) return "stato:regio.decreto:1933-10-30;1612:1";
		if(alias.equals("TU_ACQUE")) return "stato:regio.decreto:1933-12-11;1775:1";
		if(alias.equals("TU_CORTE_CONTI")) return "stato:regio.decreto:1934-07-12;1214:1";
		if(alias.equals("TU_LEGGI_SANITARIE")) return "stato:regio.decreto:1934-07-27;1265:1";
		if(alias.equals("TU_IMPOSTA_REGISTRO")) return "stato:regio.decreto:1986-04-26;131";
		if(alias.equals("TU_IMPOSTE_REDDITO")) return "presidente.repubblica:decreto:1986-12-22;917";
		if(alias.equals("TU_SEQUESTRO")) return "presidente.repubblica:decreto:1950-01-05;180:1";
		if(alias.equals("TU_IMPIEGATI")) return "presidente.repubblica:decreto:1957-03-30;3";
		if(alias.equals("TU_BANCARIO")) return "stato:decreto.legislativo:1993-09-01;385";
		if(alias.equals("TU_FINANZE")) return "stato:decreto.legislativo:1998;058";
		if(alias.equals("TU_STUPEFACENTI")) return "presidente.repubblica:decreto:1990-10-09;309";
		if(alias.equals("TU_ENTI_LOCALI")) return "stato:decreto.legislativo:2000-08-18;267";
		
		if(alias.equals("TU_ACCISE")) return "stato:decreto.legislativo:1995-10-26;504";
		if(alias.equals("TU_SUCCESSIONI")) return "stato:decreto.legislativo:1990-10-31;346:1";
		if(alias.equals("TU_SOCIETA_PART_PUBBL")) return "stato:decreto.legislativo:2016-08-19;175";
		if(alias.equals("TU_AMBIENTE")) return "stato:decreto.legislativo:2006-04-03;152";
		if(alias.equals("TU_IMMIGRAZIONE")) return "stato:decreto.legislativo:1998-07-25;286";
		if(alias.equals("TU_PUBBL_IMPIEGO")) return "stato:decreto.legislativo:2001-03-30;165";
		if(alias.equals("TU_ELEZIONE")) return "stato:decreto.del.presidente.della.repubblica:1960-05-16;570:1";
		
		
/*

{TuCamDep}(({S}?{Dash}?{S}?{TuCamDep})|({S}?[\(]{S}?{TuCamDep}{S}?[\)]))?
	{ saveAlias(yytext(), "presidente.repubblica", "decreto", "30031957", "361"); }

{TuAssInf}(({S}?{Dash}?{S}?{TuAssInf})|({S}?[\(]{S}?{TuAssInf}{S}?[\)]))?
	{ saveAlias(yytext(), "presidente.repubblica", "decreto", "30061965", "1124"); }

{TuIva}(({S}?{Dash}?{S}?{TuIva})|({S}?[\(]{S}?{TuIva}{S}?[\)]))?
	{ saveAlias(yytext(), "presidente.repubblica", "decreto", "26101972", "633"); }

{TuDoganale}(({S}?{Dash}?{S}?{TuDoganale})|({S}?[\(]{S}?{TuDoganale}{S}?[\)]))?
	{ saveAlias(yytext(), "presidente.repubblica", "decreto", "23011973", "43"); }

{TuPostale}(({S}?{Dash}?{S}?{TuPostale})|({S}?[\(]{S}?{TuPostale}{S}?[\)]))?
	{ saveAlias(yytext(), "presidente.repubblica", "decreto", "29031973", "156"); }

{TuMezzogiorno}(({S}?{Dash}?{S}?{TuMezzogiorno})|({S}?[\(]{S}?{TuMezzogiorno}{S}?[\)]))?
	{ saveAlias(yytext(), "presidente.repubblica", "decreto", "06031978", "218"); }

{TuPensioni}(({S}?{Dash}?{S}?{TuPensioni})|({S}?[\(]{S}?{TuPensioni}{S}?[\)]))?
	{ saveAlias(yytext(), "presidente.repubblica", "decreto", "23121978", "915"); }

{TuPromLeggi}(({S}?{Dash}?{S}?{TuPromLeggi})|({S}?[\(]{S}?{TuPromLeggi}{S}?[\)]))?
	{ saveAlias(yytext(), "presidente.repubblica", "decreto", "28121985", "1092"); }

{TuStupe}(({S}?{Dash}?{S}?{TuStupe})|({S}?[\(]{S}?{TuStupe}{S}?[\)]))?
	{ saveAlias(yytext(), "presidente.repubblica", "decreto", "09101990", "309"); }

{TuImpIpo}(({S}?{Dash}?{S}?{TuImpIpo})|({S}?[\(]{S}?{TuImpIpo}{S}?[\)]))?
	{ saveAlias(yytext(), "stato", "decreto.legislativo", "31101990", "347"); }

{TuSenato}(({S}?{Dash}?{S}?{TuSenato})|({S}?[\(]{S}?{TuSenato}{S}?[\)]))?
	{ saveAlias(yytext(), "stato", "decreto.legislativo", "20121993", "533"); }

{TuIstruzione}(({S}?{Dash}?{S}?{TuIstruzione})|({S}?[\(]{S}?{TuIstruzione}{S}?[\)]))?
	{ saveAlias(yytext(), "stato", "decreto.legislativo", "16041994", "297"); }

{TuFinanz}(({S}?{Dash}?{S}?{TuFinanz})|({S}?[\(]{S}?{TuFinanz}{S}?[\)]))?
	{ saveAlias(yytext(), "stato", "decreto.legislativo", "24021998", "58"); }

{TuEntiLoc}(({S}?{Dash}?{S}?{TuEntiLoc})|({S}?[\(]{S}?{TuEntiLoc}{S}?[\)]))?
	{ saveAlias(yytext(), "stato", "decreto.legislativo", "18082000", "267"); }

{TuMatern}(({S}?{Dash}?{S}?{TuMatern})|({S}?[\(]{S}?{TuMatern}{S}?[\)]))?
	{ saveAlias(yytext(), "stato", "decreto.legislativo", "26032001", "151"); }

{TuRadiotel}(({S}?{Dash}?{S}?{TuRadiotel})|({S}?[\(]{S}?{TuRadiotel}{S}?[\)]))?
	{ saveAlias(yytext(), "stato", "decreto.legislativo", "31072005", "177"); }

{TuSicLav}(({S}?{Dash}?{S}?{TuSicLav})|({S}?[\(]{S}?{TuSicLav}{S}?[\)]))?
	{ saveAlias(yytext(), "stato", "decreto.legislativo", "09042008", "81"); }

{TuCaccia}(({S}?{Dash}?{S}?{TuCaccia})|({S}?[\(]{S}?{TuCaccia}{S}?[\)]))?
	{ saveAlias(yytext(), "stato", "regio.decreto", "05061939", "1016"); } 

*/
		
		
		if(alias.equals("STATUTO_LAVORATORI")) return "stato:legge:1970-05-20;300";
		
		if(alias.equals("ORDIN_PENIT")) return "stato:legge:1975-07-26;354";
		
		if(alias.equals("LEGGE_FALL")) return "stato:regio.decreto:1942-03-16;267:1";

		if(alias.equals("LEGGE_FIN_2002")) return "stato:legge:2001-12-28;448";
		if(alias.equals("LEGGE_FIN_2003")) return "stato:legge:2002-12-27;289";
		
		
		/*
		 * Alias COMPLESSI
		 * 
		 * TODO 
		 * 
		 */
		
		if(alias.equals("LEGGE_FIN")) {
			
			if(year != null) {
				
				if(year.equals("2002")) return "stato:legge:2001-12-28;448";
				if(year.equals("2003")) return "stato:legge:2002-12-27;289";
			}
			
			return "";
		}
		
		/*
		 * C.C.N.L.
		 * 
		 * TODO
		 * 
		 */
		
		if(alias.equals("CCNL")) {
			
			return null;
		}
		
		return null;
	}
}
