/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

class Partition extends AnnotationEntity {

	Partition() {
		
		super(Entity.PARTITION);
	}
	
	int getRank() {
		
		int rank = 0;
		
		for(AnnotationEntity item : getRelatedEntities()) {
			
			if(item instanceof PartitionElement && ((PartitionElement) item).getRank() > rank) rank = ((PartitionElement) item).getRank(); 
		}
		
		return rank;
	}

	int getMinRank() {
		
		int rank = 10;
		
		for(AnnotationEntity item : getRelatedEntities()) {
			
			if(item instanceof PartitionElement && ((PartitionElement) item).getRank() < rank) rank = ((PartitionElement) item).getRank(); 
		}
		
		return rank;
	}

	int getMaxRank() {
		
		int rank = 0;
		
		for(AnnotationEntity item : getRelatedEntities()) {
			
			if(item instanceof PartitionElement && ((PartitionElement) item).getRank() > rank) rank = ((PartitionElement) item).getRank(); 
		}
		
		return rank;
	}

	void updateValue() {
		
		String normValue = "";
		
		if(this.getRelatedEntity(Entity.ANNEX) != null) {
			
			//Nota: è sempre presente un valore per gli allegati, che include il tipo di allegato (all, tabella, tariffa, protocollo)
			normValue = this.getRelatedEntity(Entity.ANNEX).getValue();
		}
		
		if(this.getRelatedEntity(Entity.ARTICLE) != null) {
			
			if(normValue.equals("")) {
				
				normValue = "articolo-" + this.getRelatedEntity(Entity.ARTICLE).getValue();
			
			} else {
				
				normValue += "_articolo-" + this.getRelatedEntity(Entity.ARTICLE).getValue();
			}
		}
		
		AnnotationEntity comma = this.getRelatedEntity(Entity.COMMA);
		
		if(comma != null) {

			if(normValue.equals("")) {
				
				normValue = "comma-" + comma.getValue();
			
			} else {
				
				normValue += "_comma-" + comma.getValue();
			}
		}
		
		if(this.getRelatedEntity(Entity.PARAGRAPH) != null) {
			
			if(normValue.equals("")) {
				
				normValue = "paragrafo-" + this.getRelatedEntity(Entity.PARAGRAPH).getValue();
			
			} else {
				
				normValue += "_paragrafo-" + this.getRelatedEntity(Entity.PARAGRAPH).getValue();
			}			
		}
		
		if(this.getRelatedEntity(Entity.LETTER) != null) {
			
			if(normValue.equals("")) {
				
				normValue = "lettera-" + this.getRelatedEntity(Entity.LETTER).getValue();
			
			} else {
				
				normValue += "_lettera-" + this.getRelatedEntity(Entity.LETTER).getValue();
			}
		}
		
		if(this.getRelatedEntity(Entity.ITEM) != null) {
			
			if(normValue.equals("")) {
				
				normValue = "numero-" + this.getRelatedEntity(Entity.ITEM).getValue();
			
			} else {
				
				normValue += "_numero-" + this.getRelatedEntity(Entity.ITEM).getValue();
			}			
		}
		
		if(this.getRelatedEntity(Entity.SENTENCE) != null) {
			
			if(normValue.equals("")) {
				
				normValue = "periodo-" + this.getRelatedEntity(Entity.SENTENCE).getValue();
			
			} else {
				
				normValue += "_periodo-" + this.getRelatedEntity(Entity.SENTENCE).getValue();
			}			
		}
		
		if(this.getRelatedEntity(Entity.POINT) != null) {
			
			if(normValue.equals("")) {
				
				normValue = "punto-" + this.getRelatedEntity(Entity.POINT).getValue();
			
			} else {
				
				normValue += "_punto-" + this.getRelatedEntity(Entity.POINT).getValue();
			}			
		}
		
		AnnotationEntity parte = this.getRelatedEntity(Entity.PARTE);
		
		if(parte == null && comma != null) parte = comma.getRelatedEntity(Entity.PARTE);
		
		if(parte != null) {
			
			if(normValue.equals("")) {
				
				normValue = "parte-" + parte.getValue();
			
			} else {
				
				normValue += "_parte-" + parte.getValue();
			}			
		}
		
		setValue(normValue);
	}
}
