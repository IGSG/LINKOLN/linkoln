/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.text.Normalizer;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Util {
	
	private static final Pattern digits = Pattern.compile("\\d+");

	private static final String munFileName = "data/municipalities.txt";
	
	//private static final String costRepositoryFileName = "data/costRepository.csv";
	
	static Map<String,String> token2code = null;
	
	//static Map<String,String> costRepository = null;

	static boolean initMunicipalities() {
		
		if(Util.token2code != null) {
			
			return false;
		}
		
		BufferedReader reader = null;
		InputStream in = null;
		
		try {
			
			File munFile = new File(munFileName);
			
			if( !munFile.exists()) {
				
				//Look for it as a resource in the jar file
				Util util = new Util();
				in = util.getClass().getResourceAsStream("/" + munFileName);
				
				if(in == null) {
					return false;
				}
				
				reader = new BufferedReader(new InputStreamReader(in));
				
			} else {
				
				reader = new BufferedReader(new InputStreamReader(new FileInputStream(munFile)));
			}
		
			Util.token2code = new HashMap<String,String>();
			
			String l = null;
	
		    while( ( l = reader.readLine() ) != null ) {
		    	
		    	if(l.startsWith("/*") || l.startsWith("*") || l.startsWith(" *") || l.startsWith("#")) continue;
	
				int split = l.indexOf(" ");
				String code = l.substring(0, split).trim();
				String namedEntity = l.substring(split).trim().toLowerCase();
			
				Util.token2code.put(namedEntity, code);
		    }
	    
		    reader.close();  
		    if(in != null) in.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return false;
		}
		
		return true;
	}
	
	/*
	 * 
	 * 1991,362,ORD,1991-07-11,1991-07-18
	 * 1991,363,SENT,1991-07-11,1991-07-23
	 * 
	 * Set di codici esistenti così composti:
	 * - numero_data
	 * - numero_anno
	 * - numero_anno_data
	 * - tipo_numero_anno
	 * - tipo_numero_data
	 * 
	 */
	/*
	static boolean initCostRepository() {
		
		File costRepositoryFile = new File(costRepositoryFileName);
		
		BufferedReader reader = null;
		InputStream in = null;
		
		try {
		
			if( !costRepositoryFile.exists()) {
				
				//Look for it as a resource in the jar file
				Util util = new Util();
				in = util.getClass().getResourceAsStream("/" + costRepositoryFileName);
				
				if(in == null) {
					return false;
				}
				
				reader = new BufferedReader(new InputStreamReader(in));
				
			} else {
				
				reader = new BufferedReader(new InputStreamReader(new FileInputStream(costRepositoryFile)));
			}
		
			Util.costRepository = new HashMap<String,String>();
			
			String l = null;
	
		    while( ( l = reader.readLine() ) != null ) {
	
		    	String[] items = l.split(",");
		    	
		    	String anno = items[0];
		    	String numero = items[1];
		    	String tipo = items[2];
		    	String data = items[3];
		    	
		    	String record = tipo + "_" + anno + "_" + numero + "_" + data;
		    	
		    	costRepository.put(numero + "_" + data, record);
		    	costRepository.put(numero + "_" + anno, record);
		    	costRepository.put(numero + "_" + anno + "_" + data, record);
		    	costRepository.put(tipo + "_" + numero + "_" + anno, record);
		    	costRepository.put(tipo + "_" + numero + "_" + data, record);
		    }
		    
		    reader.close();  
		    if(in != null) in.close();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	*/
	
	static final String readFirstNumber(String text) {
	
		if(text == null) return null;
		
		Matcher matcher = digits.matcher(text);
		String number = "";
		
		while(matcher.find()) {
			
			number = text.substring(matcher.start(), matcher.end());			
			break;
		}
		
		return number;
	}

	static final String readSecondNumber(String text) {
		
		if(text == null) return null;
		
		Matcher matcher = digits.matcher(text);
		String number = "";
		
		boolean first = false;
		while(matcher.find()) {
			
			number = text.substring(matcher.start(), matcher.end());
			if(first) break;
			first = true;
		}
		
		return number;
	}

	static final String readLastNumber(String text) {
		
		Matcher matcher = digits.matcher(text);
		String number = "";
		
		while( matcher.find() ) {
			number = text.substring(matcher.start(), matcher.end());
		}
		
		return number;
	}
	
	static final InputType guessInputType(String text) {
		
		if(text == null) return null;
		
		String trimmed = text.trim();
		if(trimmed.length() < 3) return null;
		
		int cut = 512;
		
		if(trimmed.length() < 512) cut = trimmed.length();

		String header = trimmed.substring(0,cut).toLowerCase();
		
		if(header.indexOf("<mrk:metadata") > -1 || header.indexOf("<mrk:documento") > -1) {
			
			//System.out.println("Guessed InputType: XML_IGSG_MARKER");
			
			return InputType.XML_IGSG_MARKER;
		}
		
		if(header.indexOf("<html") > -1 || header.indexOf("<!doctype html") > -1) {
			
			//System.out.println("Guessed InputType: HTML_DOCUMENT");
			
			return InputType.HTML_DOCUMENT;
		}
		
		if(header.indexOf("<root") > -1) {
			
			//System.out.println("Guessed InputType: XML_DOCUMENT");
			
			return InputType.XML_DOCUMENT;
		}
		
		//Verifica la generica presenza di un'annotazione html-like
		int lt = text.indexOf("<");
		int gt = text.indexOf(">");
		
		if(lt > -1 && gt > -1 && (gt-lt) < 512) {
			
			//System.out.println("Guessed InputType: HYBRID_TEXT");
			
			return InputType.HYBRID_TEXT;
		}
		
		//default
		return InputType.PLAIN_TEXT;
	}

	
	static final String removeAllAnnotations(String text) {
		
		Cleaner ac = new Cleaner();
		
		try {
			
			ac.yyreset(new StringReader(text));
			
			ac.yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			
			return "";
		}
		
		return ac.getOutput();
	}
	
	static final String tokenize(String text) {
		
		Tokenizer t = new Tokenizer();
		
		try {
			
			t.yyreset(new StringReader(text));
			
			t.yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			
			return "";
		}

		return t.getOutput().toLowerCase().replaceAll("\\s+", " ").trim();
	}
	
	static final boolean isFutureReference(int year, String metaYear) {
		
		if(metaYear.length() < 1) {
			return false;
		}
		
		int value = 0;
		
		try {
			
			value = Integer.valueOf(metaYear);
			
		} catch (NumberFormatException e) {

			return false;
		}
	
		if(metaYear.length() == 2) {
			
			if(value < 21) {
				
				metaYear = "20" + metaYear;
				
			} else {
				
				metaYear = "19" + metaYear;
			}
		}
		
		value = Integer.valueOf(metaYear);
		
		if(year > value) {
			
			return true;
		}
		
		return false;
	}
	
	static String getEqualPart(String a, String b) {
		
		for(int i = 0; i < a.length(); i++) {
			
			Character ai = a.charAt(i);
			
			if(i >= b.length()) {
				
				return a.substring(0, i);
			}
			
			Character bi = b.charAt(i);

			if( !ai.equals(bi)) {
				
				return a.substring(0, i);
			}
		}
		
		return a;
	}
	
	//Richiedi un ValueReader sul testo (text è l'intestazione di un partition element)
	static final ValueReader getValueReader(String text) {
		
		ValueReader vr = new ValueReader();
		
		try {
			
			vr.yyreset(new StringReader(text));
			
			vr.yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			
			return null;
		}
		
		return vr;
	}
	
	static String normalizeYear(String year) {
		
		if(year.length() == 2) {

			int value = Integer.valueOf(year);

			if(value < 29) {
				
				year = "20" + year;
				
			} else {
				
				year = "19" + year;
			}
		
		}
		
		return year;
	}
	
	static String normalizeDayMonth(String dayMonth) {
		
		if(dayMonth.length() == 1) dayMonth = "0" + dayMonth;
		
		return dayMonth;
	}
	
	static final String normalizeSpaces(String text) {
		
		if(text == null) return null;
		
		//Elimina ritorni a capo e spazi multipli
		
		return text.replaceAll("\r", " ").replaceAll("\n", " ").replaceAll("\\s+", " ").trim();
	}

	static final String normalizeText(String text) {
		
		//Normalizza il generico testo affinchè possa essere usato come value di un'annotation entity (es.: Applicant)
		
		TextNormalizer tn = new TextNormalizer();
		
		try {
			
			tn.yyreset(new StringReader(text));
			
			tn.yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			
			return "";
		}
		
		String output = tn.getOutput();
		
		//Rimuovi gli spazi multipli
		output = output.replaceAll("\\s+", " ").trim();
		
		return output;
	}
	
	static String stripAccents(String s) {
		
		s = Normalizer.normalize(s, Normalizer.Form.NFD);
		s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		return s;
	}
	
	static boolean isNullOrEmpty(String str) {
		
		if(str == null) return true;
		
		if(str.trim().length() < 1) return true;
		
		return false;
	}

	static String normalizeCharSet(String charSet) {
		
		if(charSet == null) return null;

		//charSet possibili: "UTF-8", "ISO-8859-1", "windows-1252"
		
		charSet = charSet.trim().toLowerCase();
		
		if(charSet.indexOf("utf") > -1) return "UTF-8";
		
		if(charSet.indexOf("win") > -1) return "windows-1252";
		
		if(charSet.indexOf("iso") > -1) return "iso-8859-1";
		
		return null;
	}
	
	static String convertRegionToNir(String region) {
		
		if(region.equals("VDA")) return "valle.aosta";
		
		region = convertRegionToName(region);
		
		if(region == null) return "";
		if(region.trim().length() < 1) return "";
		
		region = region.toLowerCase().replaceAll(" ", ".");
		
		return region;
	}

	static String convertCityToNir(String city) {
		
		if(city.equals("AQ")) return "aquila";
		if(city.equals("SP")) return "spezia";
		
		city = convertCityToName(city);
		
		if(city == null) return "";
		if(city.trim().length() < 1) return "";
		
		city = city.toLowerCase().replaceAll(" ", ".");
		
		return city;
	}
	
	static String convertRegionToName(String region) {
		
		if(region == null) return "";
		if(region.trim().length() < 1) return "";
		
		if(region.equals("ABR")) return "Abruzzo";
		if(region.equals("BAS")) return "Basilicata";
		if(region.equals("CAL")) return "Calabria";
		if(region.equals("CAM")) return "Campania";
		if(region.equals("EMR")) return "Emilia Romagna";
		if(region.equals("FVG")) return "Friuli Venezia Giulia";
		if(region.equals("LAZ")) return "Lazio";
		if(region.equals("LIG")) return "Liguria";
		if(region.equals("LOM")) return "Lombardia";
		if(region.equals("MAR")) return "Marche";
		if(region.equals("MOL")) return "Molise";
		if(region.equals("PIE")) return "Piemonte";
		if(region.equals("PUG")) return "Puglia";
		if(region.equals("SAR")) return "Sardegna";
		if(region.equals("SIC")) return "Sicilia";
		if(region.equals("TOS")) return "Toscana";
		if(region.equals("TAA")) return "Trentino Alto Adige";
		if(region.equals("UMB")) return "Umbria";
		if(region.equals("VDA")) return "Valle d'Aosta";
		if(region.equals("VEN")) return "Veneto";
		
		return "";
	}

	static String convertCityToName(String city) {
		
		if(city == null) return null;
		if(city.trim().length() < 1) return null;

		if(city.equals("AQ")) return "L'Aquila";
		if(city.equals("PZ")) return "Potenza";
		if(city.equals("CZ")) return "Catanzaro";
		if(city.equals("NA")) return "Napoli";
		if(city.equals("BO")) return "Bologna";
		if(city.equals("TS")) return "Trieste";
		if(city.equals("RM")) return "Roma";
		if(city.equals("GE")) return "Genova";
		if(city.equals("MI")) return "Milano";
		if(city.equals("AN")) return "Ancona";
		if(city.equals("CB")) return "Campobasso";
		if(city.equals("TO")) return "Torino";
		if(city.equals("BA")) return "Bari";
		if(city.equals("CA")) return "Cagliari";
		if(city.equals("PA")) return "Palermo";
		if(city.equals("FI")) return "Firenze";
		if(city.equals("TN")) return "Trento";
		if(city.equals("PG")) return "Perugia";
		if(city.equals("AO")) return "Aosta";
		if(city.equals("VE")) return "Venezia";

		if(city.equals("PE")) return "Pescara";
		if(city.equals("RC")) return "Reggio Calabria";
		if(city.equals("SA")) return "Salerno";
		if(city.equals("PR")) return "Parma";
		if(city.equals("LT")) return "Latina";
		if(city.equals("BS")) return "Brescia";
		if(city.equals("LE")) return "Lecce";
		if(city.equals("CT")) return "Catania";
		if(city.equals("BZ")) return "Bolzano";
		
		if(city.equals("AG")) return "Agrigento";
		if(city.equals("AL")) return "Alessandria";
		if(city.equals("AR")) return "Arezzo";
		if(city.equals("AP")) return "Ascoli Piceno";
		if(city.equals("AT")) return "Asti";
		if(city.equals("AV")) return "Avellino";
		if(city.equals("BL")) return "Belluno";
		if(city.equals("BN")) return "Benevento";
		if(city.equals("BG")) return "Bergamo";
		if(city.equals("BI")) return "Biella";
		if(city.equals("BR")) return "Brindisi";
		if(city.equals("CL")) return "Caltanissetta";
		if(city.equals("CE")) return "Caserta";
		if(city.equals("CH")) return "Chieti";
		if(city.equals("CO")) return "Como";
		if(city.equals("CS")) return "Cosenza";
		if(city.equals("CR")) return "Cremona";
		if(city.equals("KR")) return "Crotone";
		if(city.equals("CN")) return "Cuneo";
		if(city.equals("EN")) return "Enna";
		if(city.equals("FM")) return "Fermo";
		if(city.equals("FE")) return "Ferrara";
		if(city.equals("FG")) return "Foggia";
		if(city.equals("FR")) return "Frosinone";
		if(city.equals("GO")) return "Gorizia";
		if(city.equals("GR")) return "Grosseto";
		if(city.equals("IM")) return "Imperia";
		if(city.equals("IS")) return "Isernia";
		if(city.equals("SP")) return "La Spezia";
		if(city.equals("LC")) return "Lecco";
		if(city.equals("LI")) return "Livorno";
		if(city.equals("LO")) return "Lodi";
		if(city.equals("LU")) return "Lucca";
		if(city.equals("MC")) return "Macerata";
		if(city.equals("MN")) return "Mantova";
		if(city.equals("MT")) return "Matera";
		if(city.equals("ME")) return "Messina";
		if(city.equals("MO")) return "Modena";
		if(city.equals("NO")) return "Novara";
		if(city.equals("NU")) return "Nuoro";
		if(city.equals("OR")) return "Oristano";
		if(city.equals("PD")) return "Padova";
		if(city.equals("PV")) return "Pavia";
		if(city.equals("PC")) return "Piacenza";
		if(city.equals("PI")) return "Pisa";
		if(city.equals("PT")) return "Pistoia";
		if(city.equals("PN")) return "Pordenone";
		if(city.equals("PO")) return "Prato";
		if(city.equals("RG")) return "Ragusa";
		if(city.equals("RA")) return "Ravenna";
		if(city.equals("RE")) return "Reggio Emilia";
		if(city.equals("RI")) return "Rieti";
		if(city.equals("RN")) return "Rimini";
		if(city.equals("RO")) return "Rovigno";
		if(city.equals("SS")) return "Sassari";
		if(city.equals("SV")) return "Savona";
		if(city.equals("SI")) return "Siena";
		if(city.equals("SR")) return "Siracusa";
		if(city.equals("SO")) return "Sondrio";
		if(city.equals("TA")) return "Taranto";
		if(city.equals("TE")) return "Teramo";
		if(city.equals("TR")) return "Terni";
		if(city.equals("OG")) return "Ogliastra";
		if(city.equals("TP")) return "Trapani";
		if(city.equals("TV")) return "Treviso";
		if(city.equals("UD")) return "Udine";
		if(city.equals("VA")) return "Varese";
		if(city.equals("VC")) return "Vercelli";
		if(city.equals("VR")) return "Verona";
		if(city.equals("VV")) return "Vibo Valentia";
		if(city.equals("VI")) return "Vicenza";
		if(city.equals("VT")) return "Viterbo";
		
		if(city.equals("BT")) return "Barletta";
		if(city.equals("MB")) return "Monza";
		
		if(city.equals("NN")) return "Napoli Nord";
		
		if(city.equals("B745")) return "Carbonia";
		if(city.equals("E281")) return "Iglesias";
		if(city.equals("D704")) return "Forlì";
		if(city.equals("C573")) return "Cesena";
		if(city.equals("F023")) return "Massa";
		if(city.equals("B832")) return "Carrara";
		if(city.equals("G015")) return "Olbia";
		if(city.equals("L093")) return "Tempio Pausania";
		if(city.equals("G479")) return "Pesaro";
		if(city.equals("L500")) return "Urbino";
		if(city.equals("L746")) return "Verbania";
		
		if(city.equals("L328")) return "Trani";
		if(city.equals("F924")) return "Nola";
		if(city.equals("E372")) return "Vasto";
		if(city.equals("E379")) return "Ivrea";
		if(city.equals("L245")) return "Torre Annunziata";
		if(city.equals("I921")) return "Spoleto";
		if(city.equals("B300")) return "Busto Arsizio";
		if(city.equals("E435")) return "Lanciano";
		if(city.equals("G288")) return "Palmi";
		if(city.equals("D976")) return "Locri";
		if(city.equals("E974")) return "Marsala";
		if(city.equals("I804")) return "Sulmona";
		if(city.equals("C349")) return "Castrovillari";
		if(city.equals("F104")) return "Melfi";
		if(city.equals("A515")) return "Avezzano";
		if(city.equals("C034")) return "Cassino";
		if(city.equals("M208")) return "Lamezia Terme";
		if(city.equals("H612")) return "Rovereto";
		if(city.equals("I608")) return "Senigallia";
		if(city.equals("F284")) return "Molfetta";
		if(city.equals("E409")) return "Lagonegro";
		if(city.equals("L112")) return "Termini Imerese";
		if(city.equals("A124")) return "Alba";
		if(city.equals("A399")) return "Ariano Irpino";
		if(city.equals("G317")) return "Paola";
		if(city.equals("L628")) return "Vallo della Lucania";
		if(city.equals("E456")) return "Larino";
		if(city.equals("F912")) return "Nocera Inferiore";
		if(city.equals("G148")) return "Orvieto";
		if(city.equals("L182")) return "Tivoli";
		
		return null;
	}
	

	
	static String getTwoLettersCountryCode(String threeLettersCountryCode) {
		
		String country = threeLettersCountryCode;
		
		if(country.equals("AUT")) return "AT";
		if(country.equals("BEL")) return "BE";
		if(country.equals("BGR")) return "BG";
		if(country.equals("HRV")) return "HR";
		if(country.equals("CYP")) return "CY";
		if(country.equals("CZE")) return "CZ";
		if(country.equals("CSK")) return "CZ";
		if(country.equals("DNK")) return "DK";
		if(country.equals("EST")) return "EE";
		if(country.equals("FIN")) return "FI";
		if(country.equals("FRA")) return "FR";
		if(country.equals("DEU")) return "DE";
		if(country.equals("GRC")) return "GR";
		if(country.equals("ITA")) return "IT";
		if(country.equals("IRL")) return "IE";
		if(country.equals("ITA")) return "IT";
		if(country.equals("LVA")) return "LV";
		if(country.equals("LTU")) return "LT";
		if(country.equals("LUX")) return "LU";
		if(country.equals("MLT")) return "MT";
		if(country.equals("NLD")) return "NL";
		if(country.equals("POL")) return "PL";
		if(country.equals("PRT")) return "PT";
		if(country.equals("ROU")) return "RO";
		if(country.equals("SVK")) return "SK";
		if(country.equals("SVN")) return "SI";
		if(country.equals("ESP")) return "ES";
		if(country.equals("SWE")) return "SE";
		if(country.equals("GBR")) return "GB";
		if(country.equals("ALB")) return "AL";
		if(country.equals("AND")) return "AD";
		if(country.equals("ARM")) return "AM";
		if(country.equals("AZE")) return "AZ";
		if(country.equals("BIH")) return "BA";
		if(country.equals("GEO")) return "GE";
		if(country.equals("ISL")) return "IS";
		if(country.equals("LIE")) return "LI";
		if(country.equals("MKD")) return "MK";
		if(country.equals("MCO")) return "MC";
		if(country.equals("MNE")) return "ME";
		if(country.equals("NOR")) return "NO";
		if(country.equals("MDA")) return "MD";
		if(country.equals("RUS")) return "RU";
		if(country.equals("SMR")) return "SM";
		if(country.equals("SRB")) return "RS";
		if(country.equals("SCG")) return "RS";
		if(country.equals("CHE")) return "CH";
		if(country.equals("TUR")) return "TR";
		if(country.equals("UKR")) return "UA";
		if(country.equals("BLR")) return "BY";
		
		//CHL chile
		//HUN hungary
		//CSK cecoslovacchia
		//SCG Serbia Montenegro
		//Uno di questi: ALB;AND;ARM;AUT;AZE;BEL;BGR;BIH;HRV;CYP;CZE;DNK;ESP;EST;FIN;FRA;GEO;DEU;GRC;HUN;IRL;ISL;ITA;LIE;LTU;LUX;LVA;MCO;MDA;MKD;MLT;NLD;NOR;POL;PRT;ROU;RUS;SRB;SMR;SVK;SVN;SWE;SWZ;TUR;GBR;UKR
		if(country.equals("HUN")) return "HU";
		
		
		return "";
	}

	
	
	
	
	
	
	
	/*
	static String lookupAuthority(String authority) {
		
		if(authority == null) return null;
		if(authority.trim().length() < 1) return null;
		
		if(authority.equals("CE_ECHR")) return "Corte europea dei Diritti dell'Uomo";
		if(authority.equals("EU_CJEU")) return "Corte di Giustizia dell'Unione Europea";
		if(authority.equals("EU_CJEC")) return "Corte di Giustizia della Comunità Europea";
		if(authority.equals("IT_COST")) return "Corte Costituzionale";
		if(authority.equals("IT_CASS")) return "Corte Suprema di Cassazione";
		if(authority.equals("IT_CDS")) return "Consiglio di Stato";
		if(authority.startsWith("IT_CONT")) return "Corte dei Conti";
		if(authority.equals("IT_CSM")) return "Consiglio Superiore della Magistratura";
		if(authority.startsWith("IT_TAR")) return "Tribunale Amministrativo Regionale";
		if(authority.startsWith("IT_CPP")) return "Corte d'Appello";
		if(authority.startsWith("IT_CSS")) return "Corte d'Assise";
		if(authority.startsWith("IT_CSP")) return "Corte d'Assise d'Appello";
		if(authority.startsWith("IT_TRB")) return "Tribunale";
		if(authority.startsWith("IT_TMN")) return "Tribunale dei Minori";
		if(authority.startsWith("IT_TML")) return "Tribunale Militare";
		if(authority.startsWith("IT_USV")) return "Ufficio di Sorveglianza";
		if(authority.startsWith("IT_TSV")) return "Tribunale di Sorveglianza";
		if(authority.startsWith("IT_GPC")) return "Giudice di Pace";
		if(authority.startsWith("IT_PCR")) return "Procura della Repubblica";
		if(authority.startsWith("IT_PTR")) return "Pretura";
		if(authority.startsWith("IT_CGARS")) return "Consiglio di giustizia amministrativa per la regione siciliana";
		
		if(authority.startsWith("IT_CTC")) return "Commissione Tributaria Centrale";
		if(authority.startsWith("IT_CTR")) return "Commissione Tributaria Regionale";
		if(authority.startsWith("IT_CTP")) return "Commissione Tributaria Provinciale";
		
		if(authority.startsWith("EU_COUNCIL")) return "Consiglio dell'Unione Europea";
		if(authority.startsWith("EU_PARLIAMENT_COUNCIL")) return "Parlamento e Consiglio dell'Unione europea";
		if(authority.startsWith("EU_COMMISSION")) return "Commissione europea";
		if(authority.startsWith("EU_PARLIAMENT")) return "Parlamento europeo";
		
		return null;
	}
	
	static String lookupRegion(String region) {
		
		if(region == null) return null;
		if(region.trim().length() < 1) return null;
		
		if(region.equals("IT_ABR")) return "Abruzzo";
		if(region.equals("IT_BAS")) return "Basilicata";
		if(region.equals("IT_CAL")) return "Calabria";
		if(region.equals("IT_CAM")) return "Campania";
		if(region.equals("IT_EMR")) return "Emilia Romagna";
		if(region.equals("IT_FVG")) return "Friuli Venezia Giulia";
		if(region.equals("IT_LAZ")) return "Lazio";
		if(region.equals("IT_LIG")) return "Liguria";
		if(region.equals("IT_LOM")) return "Lombardia";
		if(region.equals("IT_MAR")) return "Marche";
		if(region.equals("IT_MOL")) return "Molise";
		if(region.equals("IT_PIE")) return "Piemonte";
		if(region.equals("IT_PUG")) return "Puglia";
		if(region.equals("IT_SAR")) return "Sardegna";
		if(region.equals("IT_SIC")) return "Sicilia";
		if(region.equals("IT_TOS")) return "Toscana";
		if(region.equals("IT_TAA")) return "Trentino Alto Adige";
		if(region.equals("IT_UMB")) return "Umbria";
		if(region.equals("IT_VDA")) return "Valle d'Aosta";
		if(region.equals("IT_VEN")) return "Veneto";
		
		return null;
	}
	
	static String lookupCity(String city) {
		
		if(city == null) return null;
		if(city.trim().length() < 1) return null;

		if(city.equals("IT_AQ")) return "L'Aquila";
		if(city.equals("IT_PZ")) return "Potenza";
		if(city.equals("IT_CZ")) return "Catanzaro";
		if(city.equals("IT_NA")) return "Napoli";
		if(city.equals("IT_BO")) return "Bologna";
		if(city.equals("IT_TS")) return "Trieste";
		if(city.equals("IT_RM")) return "Roma";
		if(city.equals("IT_GE")) return "Genova";
		if(city.equals("IT_MI")) return "Milano";
		if(city.equals("IT_AN")) return "Ancona";
		if(city.equals("IT_CB")) return "Campobasso";
		if(city.equals("IT_TO")) return "Torino";
		if(city.equals("IT_BA")) return "Bari";
		if(city.equals("IT_CA")) return "Cagliari";
		if(city.equals("IT_PA")) return "Palermo";
		if(city.equals("IT_FI")) return "Firenze";
		if(city.equals("IT_TN")) return "Trento";
		if(city.equals("IT_PG")) return "Perugia";
		if(city.equals("IT_AO")) return "Aosta";
		if(city.equals("IT_VE")) return "Venezia";

		if(city.equals("IT_PE")) return "Pescara";
		if(city.equals("IT_RC")) return "Reggio Calabria";
		if(city.equals("IT_SA")) return "Salerno";
		if(city.equals("IT_PR")) return "Parma";
		if(city.equals("IT_LT")) return "Latina";
		if(city.equals("IT_BS")) return "Brescia";
		if(city.equals("IT_LE")) return "Lecce";
		if(city.equals("IT_CT")) return "Catania";
		if(city.equals("IT_BZ")) return "Bolzano";
		
		if(city.equals("IT_AG")) return "Agrigento";
		if(city.equals("IT_AL")) return "Alessandria";
		if(city.equals("IT_AR")) return "Arezzo";
		if(city.equals("IT_AP")) return "Ascoli Piceno";
		if(city.equals("IT_AT")) return "Asti";
		if(city.equals("IT_AV")) return "Avellino";
		if(city.equals("IT_BL")) return "Belluno";
		if(city.equals("IT_BN")) return "Benevento";
		if(city.equals("IT_BG")) return "Bergamo";
		if(city.equals("IT_BI")) return "Biella";
		if(city.equals("IT_BR")) return "Brindisi";
		if(city.equals("IT_CL")) return "Caltanissetta";
		if(city.equals("IT_CE")) return "Caserta";
		if(city.equals("IT_CH")) return "Chieti";
		if(city.equals("IT_CO")) return "Como";
		if(city.equals("IT_CS")) return "Cosenza";
		if(city.equals("IT_CR")) return "Cremona";
		if(city.equals("IT_KR")) return "Crotone";
		if(city.equals("IT_CN")) return "Cuneo";
		if(city.equals("IT_EN")) return "Enna";
		if(city.equals("IT_FM")) return "Fermo";
		if(city.equals("IT_FE")) return "Ferrara";
		if(city.equals("IT_FG")) return "Foggia";
		if(city.equals("IT_FR")) return "Frosinone";
		if(city.equals("IT_GO")) return "Gorizia";
		if(city.equals("IT_GR")) return "Grosseto";
		if(city.equals("IT_IM")) return "Imperia";
		if(city.equals("IT_IS")) return "Isernia";
		if(city.equals("IT_SP")) return "La Spezia";
		if(city.equals("IT_LC")) return "Lecco";
		if(city.equals("IT_LI")) return "Livorno";
		if(city.equals("IT_LO")) return "Lodi";
		if(city.equals("IT_LU")) return "Lucca";
		if(city.equals("IT_MC")) return "Macerata";
		if(city.equals("IT_MN")) return "Mantova";
		if(city.equals("IT_MT")) return "Matera";
		if(city.equals("IT_ME")) return "Messina";
		if(city.equals("IT_MO")) return "Modena";
		if(city.equals("IT_NO")) return "Novara";
		if(city.equals("IT_NU")) return "Nuoro";
		if(city.equals("IT_OR")) return "Oristano";
		if(city.equals("IT_PD")) return "Padova";
		if(city.equals("IT_PV")) return "Pavia";
		if(city.equals("IT_PC")) return "Piacenza";
		if(city.equals("IT_PI")) return "Pisa";
		if(city.equals("IT_PT")) return "Pistoia";
		if(city.equals("IT_PN")) return "Pordenone";
		if(city.equals("IT_PO")) return "Prato";
		if(city.equals("IT_RG")) return "Ragusa";
		if(city.equals("IT_RA")) return "Ravenna";
		if(city.equals("IT_RE")) return "Reggio Emilia";
		if(city.equals("IT_RI")) return "Rieti";
		if(city.equals("IT_RN")) return "Rimini";
		if(city.equals("IT_RO")) return "Rovigno";
		if(city.equals("IT_SS")) return "Sassari";
		if(city.equals("IT_SV")) return "Savona";
		if(city.equals("IT_SI")) return "Siena";
		if(city.equals("IT_SR")) return "Siracusa";
		if(city.equals("IT_SO")) return "Sondrio";
		if(city.equals("IT_TA")) return "Taranto";
		if(city.equals("IT_TE")) return "Teramo";
		if(city.equals("IT_TR")) return "Terni";
		if(city.equals("IT_OG")) return "Ogliastra";
		if(city.equals("IT_TP")) return "Trapani";
		if(city.equals("IT_TV")) return "Treviso";
		if(city.equals("IT_UD")) return "Udine";
		if(city.equals("IT_VA")) return "Varese";
		if(city.equals("IT_VC")) return "Vercelli";
		if(city.equals("IT_VR")) return "Verona";
		if(city.equals("IT_VV")) return "Vibo Valentia";
		if(city.equals("IT_VI")) return "Vicenza";
		if(city.equals("IT_VT")) return "Viterbo";
		
		if(city.equals("IT_BT")) return "Barletta";
		if(city.equals("IT_MB")) return "Monza";
		
		if(city.equals("IT_NN")) return "Napoli Nord";
		
		if(city.equals("IT_B745")) return "Carbonia";
		if(city.equals("IT_E281")) return "Iglesias";
		if(city.equals("IT_D704")) return "Forlì";
		if(city.equals("IT_C573")) return "Cesena";
		if(city.equals("IT_F023")) return "Massa";
		if(city.equals("IT_B832")) return "Carrara";
		if(city.equals("IT_G015")) return "Olbia";
		if(city.equals("IT_L093")) return "Tempio Pausania";
		if(city.equals("IT_G479")) return "Pesaro";
		if(city.equals("IT_L500")) return "Urbino";
		if(city.equals("IT_L746")) return "Verbania";
		
		if(city.equals("IT_L328")) return "Trani";
		if(city.equals("IT_F924")) return "Nola";
		if(city.equals("IT_E372")) return "Vasto";
		if(city.equals("IT_E379")) return "Ivrea";
		if(city.equals("IT_L245")) return "Torre Annunziata";
		if(city.equals("IT_I921")) return "Spoleto";
		if(city.equals("IT_B300")) return "Busto Arsizio";
		if(city.equals("IT_E435")) return "Lanciano";
		if(city.equals("IT_G288")) return "Palmi";
		if(city.equals("IT_D976")) return "Locri";
		if(city.equals("IT_E974")) return "Marsala";
		if(city.equals("IT_I804")) return "Sulmona";
		if(city.equals("IT_C349")) return "Castrovillari";
		if(city.equals("IT_F104")) return "Melfi";
		if(city.equals("IT_A515")) return "Avezzano";
		if(city.equals("IT_C034")) return "Cassino";
		if(city.equals("IT_M208")) return "Lamezia Terme";
		if(city.equals("IT_H612")) return "Rovereto";
		if(city.equals("IT_I608")) return "Senigallia";
		if(city.equals("IT_F284")) return "Molfetta";
		if(city.equals("IT_E409")) return "Lagonegro";
		if(city.equals("IT_L112")) return "Termini Imerese";
		if(city.equals("IT_A124")) return "Alba";
		if(city.equals("IT_A399")) return "Ariano Irpino";
		if(city.equals("IT_G317")) return "Paola";
		if(city.equals("IT_L628")) return "Vallo della Lucania";
		if(city.equals("IT_E456")) return "Larino";
		if(city.equals("IT_F912")) return "Nocera Inferiore";
		if(city.equals("IT_G148")) return "Orvieto";
		if(city.equals("IT_L182")) return "Tivoli";
		
		return null;
	}
	*/
	

	
	/*
	public String getExtendedAuthorityName() {
		
		//Compone authority e geos
		String lookupAuth = lookupAuthority();
		if(lookupAuth == null) return null;

		String lookupRegion = lookupRegion();
		String lookupCity = lookupCity(authCity);
		String lookupDetachedCity = lookupCity(detachedAuthority);
		
		if(lookupRegion != null) lookupAuth += " - " + lookupRegion;
		if(lookupCity != null) lookupAuth += " - " + lookupCity;
		if(lookupDetachedCity != null) lookupAuth += " - " + lookupDetachedCity;
		
		return lookupAuth;
	}
	*/
	
	
}
