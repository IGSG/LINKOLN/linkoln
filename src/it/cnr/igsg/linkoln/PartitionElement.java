/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

class PartitionElement extends AnnotationEntity {

	PartitionElement(Entity entity) {
		
		super(entity);

		if( !entity.equals(Entity.ANNEX) && !entity.equals(Entity.ARTICLE) && !entity.equals(Entity.COMMA) && !entity.equals(Entity.PARAGRAPH)
				&& !entity.equals(Entity.LETTER) && !entity.equals(Entity.ITEM) 
				&& !entity.equals(Entity.SENTENCE) && !entity.equals(Entity.POINT) && !entity.equals(Entity.PARTE) ) {
			
			System.err.println("Invalid use of PartitionElement(Entity entity) constructor - invalid entity: " + entity);
		}
	}

	int getRank() {
		
		if(entity.equals(Entity.ANNEX)) return 9;
		
		if(entity.equals(Entity.ARTICLE)) return 8;  //Se viene cambiato aggiornare S200_Partitions.jflex per consistenza
		
		if(entity.equals(Entity.COMMA)) return 7;
		
		if(entity.equals(Entity.PARAGRAPH)) return 6;
		
		if(entity.equals(Entity.LETTER)) return 5;
		
		if(entity.equals(Entity.ITEM)) return 4;
		
		if(entity.equals(Entity.SENTENCE)) return 3;
		
		if(entity.equals(Entity.POINT)) return 2;
		
		if(entity.equals(Entity.PARTE)) return 1;
		
		return 0;
	}
	
}
