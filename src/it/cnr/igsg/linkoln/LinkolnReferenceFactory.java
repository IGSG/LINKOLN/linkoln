/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use linkolnReference file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

import java.util.ArrayList;

class LinkolnReferenceFactory {
	
	/*
	 * Creazione degli oggetti LinkolnReference.
	 */
	
	static boolean createLinkolnReference(LinkolnDocument linkolnDocument, Reference ref) {

		ReferenceClusters referenceClusters = linkolnDocument.getReferenceClusters();
		LinkolnReference linkolnReference = new LinkolnReference();
		
		/* Text e Context del riferimento */
		
		//Normalizza togliendo ritorni a capo e spazi multipli
		//String text = ref.getText().replaceAll("\r", " ").replaceAll("\n", " ").replaceAll("\\s+", " ").trim();
		//String context = ref.getContext().replaceAll("\r", " ").replaceAll("\n", " ").replaceAll("\\s+", " ").trim();
		String text = Util.normalizeSpaces(ref.getText());
		String context = Util.normalizeSpaces(ref.getContext());
		linkolnReference.setText(text);
		linkolnReference.setContext(context);
		
		/* Consulta i reference cluster per la generazione dei valori degli attributi */

		linkolnReference.setAlias(referenceClusters.getAlias(ref) != null ? referenceClusters.getAlias(ref).getValue():null);
		
		/* Partition, article, comma */
		
		String partition = referenceClusters.getPartition(ref);
		
		if(partition != null && partition.trim().length() > 0) {
	
			linkolnReference.setPartition(partition);
			
			int startArticle = partition.toLowerCase().indexOf("articolo-");
			
			if(startArticle > -1) {
				
				String article = partition.substring(startArticle);
				int endArticle = article.indexOf("_");
				if(endArticle > 0) article = article.substring(0, endArticle);
				
				linkolnReference.setArticle(article);
			}
			
			int startComma = partition.toLowerCase().indexOf("comma-");
			
			if(startComma > -1) {
				
				String comma = partition.substring(startComma);
				int endComma = comma.indexOf("_");
				if(endComma > 0) comma = comma.substring(0, endComma);
				
				linkolnReference.setComma(comma);
			}
		}
		
		/*
		 * NOTA
		 * 
		 * all'interno di un pattern che compone una reference il valore normalizzato di una specifica feature
		 * può essere trovato in annotazioni diverse (es.: il doctype in CL_DOCTYPE oppure LEG_DOCTYPE), può trovarsi
		 * in sotto-annotazioni esplicite (es.: il numero può essere all'interno di un FULL_EU_NUMBER)
		 * o implicite (un'autorità può essere stata inserita all'interno di un'annotazione SECTION).
		 * 
		 * Di conseguenza anche all'interno di un singolo pattern reference è possibile trovare il valore di una specifica
		 * feature in più posizioni: quella che viene effettivamente memorizzata nell'oggetto LinkolnReference è la prima
		 * che viene restituita dai vari metodi di ReferenceClusters (ad esempio, se c'è un'authority esplicita nel pattern
		 * viene letta prima quella di quella eventualmente annidata dentro la sezione).
		 */

		linkolnReference.setRoNumber(referenceClusters.getRoNumber(ref));
		linkolnReference.setRvNumber(referenceClusters.getRvNumber(ref));
		linkolnReference.setGuAttr(referenceClusters.getGuAttr(ref));
		
		linkolnReference.setAuthority((referenceClusters.getAuthority(ref) != null && 
				!referenceClusters.getAuthority(ref).getValue().equalsIgnoreCase("this") &&
				!referenceClusters.getAuthority(ref).getValue().equalsIgnoreCase("court")) ? 
				referenceClusters.getAuthority(ref).getValue() : null);
		
		linkolnReference.setAuthoritySection(referenceClusters.getAuthSection(ref) != null ? referenceClusters.getAuthSection(ref).getValue() : null);
		
		linkolnReference.setDetachedCity(referenceClusters.getDetCity(ref) != null ? referenceClusters.getDetCity(ref).getValue() : null);
		
		linkolnReference.setOtherAuthority(referenceClusters.getOtherAuthority(ref) != null ? referenceClusters.getOtherAuthority(ref).getValue() : null);
		
		linkolnReference.setCity(referenceClusters.getCity(ref) != null ? referenceClusters.getCity(ref).getValue() : null);
		linkolnReference.setRegion(referenceClusters.getRegion(ref) != null ? referenceClusters.getRegion(ref).getValue() : null);

		linkolnReference.setMinistry(referenceClusters.getMinistry(ref) != null ? referenceClusters.getMinistry(ref).getValue() : null);

		linkolnReference.setDocType(referenceClusters.getDocumentType(ref) != null ? referenceClusters.getDocumentType(ref).getValue() : null);
		
		linkolnReference.setNumber(referenceClusters.getNumber(ref) != null ? referenceClusters.getNumber(ref).getValue() : null);
		linkolnReference.setFullNumber(referenceClusters.getFullNumber(ref) != null ? referenceClusters.getFullNumber(ref).getValue() : null);
		linkolnReference.setEuAcronym(referenceClusters.getEuAcronym(ref) != null ? referenceClusters.getEuAcronym(ref).getValue() : null);
		linkolnReference.setYear(referenceClusters.getYear(ref) != null ? referenceClusters.getYear(ref).getValue() : null);
		
		linkolnReference.setDate(referenceClusters.getDate(ref) != null ? referenceClusters.getDate(ref).getValue() : null);
		linkolnReference.setDepositDate(referenceClusters.getDepositDate(ref) != null ? referenceClusters.getDepositDate(ref).getValue() : null);
		linkolnReference.setNotificationDate(referenceClusters.getNotificationDate(ref) != null ? referenceClusters.getNotificationDate(ref).getValue() : null);
		linkolnReference.setPublicationDate(referenceClusters.getPublicationDate(ref) != null ? referenceClusters.getPublicationDate(ref).getValue() : null);
		
		linkolnReference.setCaseNumber(referenceClusters.getCaseNumber(ref) != null ? referenceClusters.getCaseNumber(ref).getValue() : null);
		
		linkolnReference.setApplicant(referenceClusters.getApplicant(ref) != null ? Util.normalizeText(referenceClusters.getApplicant(ref).getText()) : null);
		
		if(referenceClusters.getDefendant(ref) != null) {
			
			if(referenceClusters.getDefendant(ref).getValue() != null && referenceClusters.getDefendant(ref).getValue().trim().length() > 0) {
				
				linkolnReference.setDefendant(referenceClusters.getDefendant(ref).getValue().trim());
				
			} else {
				
				Util.normalizeText(referenceClusters.getDefendant(ref).getText());
			}
		}
		
		linkolnReference.setArea(referenceClusters.getSubject(ref) != null ? referenceClusters.getSubject(ref).getValue() : null);

		/*
		 * Categorie del riferimento
		 */
		if(isCaseLawReference(linkolnReference) && !isLegislationReference(linkolnReference)) linkolnReference.setRefType("caselaw");
		if(isLegislationReference(linkolnReference) && !isCaseLawReference(linkolnReference)) linkolnReference.setRefType("legislation");
		
		//Assegna un defaul "national" per lo scope - fare un metodo specifico ?
		linkolnReference.setRefScope("nazionale");
		
		if(isInternational(linkolnReference)) linkolnReference.setRefScope("internazionale");
		if(isEuropean(linkolnReference)) linkolnReference.setRefScope("comunitario");
		if(isRegional(linkolnReference)) linkolnReference.setRefScope("regionale");
		if(isProvinciale(linkolnReference)) linkolnReference.setRefScope("provinciale");
		if(isComunale(linkolnReference)) linkolnReference.setRefScope("comunale");
		
		/*
		 * Generazione identificatore standard ed URL
		 */
		boolean identified = false;
		
		if(IdentifierGenerationUrl.run(linkolnDocument, linkolnReference)) identified = true;
		
		if(linkolnReference.getRefScope().equals("comunitario")) {
			
			if(IdentifierGenerationCelex.run(linkolnDocument, linkolnReference)) identified = true;
		}
		
		if( !identified && linkolnReference.getRefType().equals("caselaw")) {
			
			if(IdentifierGenerationEcli.run(linkolnDocument, linkolnReference)) identified = true;
		}

		if(IdentifierGenerationUrn.run(linkolnDocument, linkolnReference)) identified = true;

		/*
		 * Generazione del Simple ID
		 */
		if(linkolnReference.getAlias() != null) {
			
			linkolnReference.setCitedDocSimpleId(linkolnReference.getAlias());
			
		} else {
			
			for(Identifier identifier : linkolnReference.getIdentifiers()) {
				
				if(identifier.getType().equals(Identifiers.URN_NIR)) {
					
					String simpleId = identifier.getCode();
					
					if(simpleId.indexOf("urn:nir:") > -1) {
						
						simpleId = simpleId.substring(simpleId.indexOf("urn:nir:") + "urn:nir:".length());
						
						if(simpleId.startsWith("stato:")) {
							
							simpleId = simpleId.substring("stato:".length());
						}
							
						if(simpleId.indexOf("~") > -1) {
							
							simpleId = simpleId.substring(0, simpleId.indexOf("~"));
						}
					}
					
					linkolnReference.setCitedDocSimpleId(simpleId);
					break;
				}
				
				if(identifier.getType().equals(Identifiers.ECLI)) {
					
					String simpleId = identifier.getCode();
					
					if(simpleId.indexOf("ECLI:") > -1) {
						
						simpleId = simpleId.substring(simpleId.indexOf("ECLI:") + "ECLI:".length());
						
						if(simpleId.startsWith("IT:")) {
							
							simpleId = simpleId.substring("IT:".length());
						}
					}
					
					linkolnReference.setCitedDocSimpleId(simpleId);
					break;
				}
				
				if(identifier.getType().equals(Identifiers.CELEX)) {
					
					String simpleId = identifier.getCode();
					
					linkolnReference.setCitedDocSimpleId("CELEX:" + simpleId);
					break;
				}
			}
		}		
		
		//Aggiungi il linkolnReference al linkolnDocument
		linkolnDocument.addReference2LinkolnReference(ref, linkolnReference);
		linkolnDocument.addLinkolnReference(linkolnReference);
		
		if(linkolnDocument.getInputType() != null && linkolnDocument.getInputType().equals(InputType.XML_IGSG_MARKER)) {
			
			//Generazione del title per input XML-MARKER
			linkolnReference.setTitle(linkolnReference.toString());
		}

		return true;
	}

	static boolean isCaseLawReference(LinkolnReference linkolnReference) {
		
		String docType = linkolnReference.getDocType();
		String auth = linkolnReference.getAuthority();
		
		if(docType != null && ( docType.equals("SENT") || docType.equals("INTERPELLO") )) return true;
		
		ArrayList<String> auths = new ArrayList<String>();
		
		auths.add("CGUE");
		auths.add("CGCE");
		auths.add("CEDU");
		auths.add("CORTE_COST");
		auths.add("CORTE_CASS");
		auths.add("CONS_STATO");
		auths.add("CORTE_CONTI");
		auths.add("CONS_GIUST_AMM_SICILIA");
		auths.add("COMM_TRIBUT_CEN");
		auths.add("COMM_TRIBUT_REG");
		auths.add("COMM_TRIBUT_PROV");
		auths.add("TRIB_AMM_REG");
		auths.add("CORTE_APPELLO");
		auths.add("CORTE_ASSISE");
		auths.add("CORTE_ASSISE_APPELLO");
		auths.add("TRIB");										
		auths.add("TRIB_MINORI");
		auths.add("TRIB_MILITARE");												
		auths.add("UFF_SORVEGLIANZA");
		auths.add("TRIB_SORVEGLIANZA");																								
		auths.add("TRIB_ACQUE");
		auths.add("GIUDICE_PACE");
		auths.add("PROCURA");
		auths.add("PRETURA");
		
		if(auth != null && auths.contains(auth)) return true; 
		
		if(linkolnReference.getArea() != null) return true;
		if(linkolnReference.getAuthoritySection() != null) return true;
		if(linkolnReference.getDetachedCity() != null) return true;
		if(linkolnReference.getCaseNumber() != null) return true;
		if(linkolnReference.getRvNumber() != null) return true;
		if(linkolnReference.getApplicant() != null) return true;
		if(linkolnReference.getDepositDate() != null) return true;
		if(linkolnReference.getNotificationDate() != null) return true;
		
		return false;
	}
	
	static boolean isLegislationReference(LinkolnReference linkolnReference) {
		
		String docType = linkolnReference.getDocType();
		String auth = linkolnReference.getAuthority();
		
		ArrayList<String> types = new ArrayList<String>();
		
		types.add("L");
		types.add("LC");
		types.add("DDL");
		types.add("COD");
		types.add("TU");
		//types.add("DECR");
		types.add("DL");
		types.add("DLGS");
		types.add("REG");
		types.add("CIRC");
		//types.add("ORD");
		types.add("DIR");
		//types.add("DECIS");
		types.add("RACC");
		types.add("NOTA");
		types.add("RIS");
		types.add("PARERE");
		types.add("PROVV");
		types.add("DET");
		//types.add("DEL"); //Può essere anche emanata dalla Corte dei Conti
		types.add("IND");
		types.add("STATUTO");
		types.add("GU");
		
		if(docType != null && types.contains(docType)) return true;
		
		ArrayList<String> auths = new ArrayList<String>();
		
		auths.add("PRES_REP");
		auths.add("PRES_CONS_MIN");
		auths.add("CAPO_GOV");
		auths.add("CAPO_PROVV_STATO");
		auths.add("LUOGOTENENTE");
		auths.add("RE");
		auths.add("CONS_UE");
		auths.add("COMM_UE");
		auths.add("PARL_UE");
		auths.add("PARL_CONS_UE");
		auths.add("BCE");
		auths.add("CONS_MIN");
		auths.add("MINISTERO");
		auths.add("PRESIDENTE");
		auths.add("DIRETTORE");
		auths.add("DIRIGENTE");										
		auths.add("CAPO");
		auths.add("REGIONE");												
		auths.add("REGIONE_PRES");
		auths.add("REGIONE_GIUNTA");																								
		auths.add("REGIONE_CONS");
		auths.add("PROVINCIA");												
		auths.add("PROVINCIA_PRES");
		auths.add("PROVINCIA_GIUNTA");																								
		auths.add("PROVINCIA_CONS");
		auths.add("COMUNE");												
		auths.add("SINDACO");
		auths.add("COMUNE_GIUNTA");																								
		auths.add("COMUNE_CONS");
		
		if(auth != null && auths.contains(auth)) return true; 
		
		if(auth == null && docType != null && ( docType.equals("DECR") || docType.equals("ORD") || docType.equals("DECIS") ) ) return true;
		
		if(linkolnReference.getAlias() != null) return true;
		if(linkolnReference.getEuAcronym() != null) return true;
		if(linkolnReference.getMinistry() != null) return true;
		if(linkolnReference.getOtherAuthority() != null) return true;
		if(linkolnReference.getGuAttr() != null) return true;
		if(linkolnReference.getPartition() != null) return true;
		
		return false;
	}
	
	static boolean isInternational(LinkolnReference linkolnReference) {
		
		String alias = linkolnReference.getAlias();
		
		if(alias != null) {
			
			if(alias.equals("CONV_EU_DIR_UOMO")) return true;
			
			return false;
		}
		
		String auth = linkolnReference.getAuthority();
		
		ArrayList<String> auths = new ArrayList<String>();
		
		auths.add("CEDU");
		
		if(auth != null && auths.contains(auth)) return true; 
		
		return false;
	}
	
	static boolean isEuropean(LinkolnReference linkolnReference) {
		
		String alias = linkolnReference.getAlias();
		
		if(alias != null) {
			
			if(alias.startsWith("TRATTATO")) return true;
			
			if(alias.equals("CARTA_DIR_FOND_UE")) return true;
			
			return false;
		}
		
		if(linkolnReference.getEuAcronym() != null) return true;
		
		String docType = linkolnReference.getDocType();
		String auth = linkolnReference.getAuthority();
		
		/*
		 * Riferimento a Gazzetta Ufficiale dell'Unione Europea 
		 */
		String guAttr = linkolnReference.getGuAttr();
		
		if(docType != null && docType.equals("GU") && guAttr != null && guAttr.startsWith("EU_")) {
			
			return true;
		}
		
		ArrayList<String> types = new ArrayList<String>();
		
		types.add("DIR");
		types.add("IND");
		
		if(docType != null && types.contains(docType)) return true;
		
		ArrayList<String> auths = new ArrayList<String>();
		
		auths.add("CONS_UE");
		auths.add("COMM_UE");
		auths.add("PARL_UE");
		auths.add("PARL_CONS_UE");
		auths.add("BCE");
		
		auths.add("CGUE");
		auths.add("CGCE");
		
		if(auth != null && auths.contains(auth)) return true; 
		
		return false;
	}
	
	static boolean isRegional(LinkolnReference linkolnReference) {
		
		String auth = linkolnReference.getAuthority();
		
		ArrayList<String> auths = new ArrayList<String>();
		
		auths.add("REGIONE");												
		auths.add("REGIONE_PRES");
		auths.add("REGIONE_GIUNTA");																								
		auths.add("REGIONE_CONS");

		if(auth != null && auths.contains(auth)) return true;
		
		return false;
	}
	
	static boolean isProvinciale(LinkolnReference linkolnReference) {
		
		String auth = linkolnReference.getAuthority();
		
		ArrayList<String> auths = new ArrayList<String>();
		
		auths.add("PROVINCIA");												
		auths.add("PROVINCIA_PRES");
		auths.add("PROVINCIA_GIUNTA");																								
		auths.add("PROVINCIA_CONS");

		if(auth != null && auths.contains(auth)) return true;
		
		return false;
	}

	static boolean isComunale(LinkolnReference linkolnReference) {
		
		String auth = linkolnReference.getAuthority();
		
		ArrayList<String> auths = new ArrayList<String>();
		
		auths.add("COMUNE");												
		auths.add("SINDACO");
		auths.add("COMUNE_GIUNTA");																								
		auths.add("COMUNE_CONS");

		if(auth != null && auths.contains(auth)) return true;
		
		return false;
	}

}
