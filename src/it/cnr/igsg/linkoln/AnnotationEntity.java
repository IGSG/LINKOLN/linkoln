/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


class AnnotationEntity implements Comparable<AnnotationEntity> {

	protected Entity entity = null;
	
	protected String id = ""; //annotation numeric identifier
	
	protected int position = -1; //position relative to the plain text
	
	protected String text = ""; //full raw text included in the annotation
	
	private String value = ""; //normalized value
	
	public final String getValue() {
		return value;
	}

	private AnnotationEntity() {
		
	}
		
	AnnotationEntity(Entity entity) {
		
		if(entity == null) {
			
			System.err.println("Invalid use of AnnotationEntity(Entity entity) constructor - entity is null!");
		}
		
		this.entity = entity;
	}
	
	Entity getEntity() {
		
		return entity;
	}
	
	void setValue(String value) {
		
		if(value == null) {
			
			System.err.println("Error - value is NULL for entity: " + this.getEntity() + " text: " + getText() + " pos: " + getPosition());
			
			this.value = "";
			return;
		}

		value = value.toUpperCase().replaceAll("\n", "").replaceAll("\r", "").trim();

		//TODO aggiornare l'elenco dei caratteri non ammessi -- soltanto le virgolette doppie??
		
		this.value = value;
	}
	
	protected Map<Entity,AnnotationEntity> name2entity = new HashMap<Entity,AnnotationEntity>();
	
	final void addRelatedEntity(AnnotationEntity annotationEntity) {
		
		name2entity.put(annotationEntity.getEntity(), annotationEntity);
	}
	
	final AnnotationEntity getRelatedEntity(Entity entity) {
		
		return name2entity.get(entity);
	}
	
	final AnnotationEntity removeRelatedEntity(Entity entity) {
		
		return name2entity.put(entity, null);
	}
	
	final Collection<AnnotationEntity> getRelatedEntities() {
		
		return Collections.unmodifiableCollection(name2entity.values());
	}
	
	final String getText() {
		return text;
	}

	final void setText(String text) {
		this.text = text;
	}

	final void appendText(String text) {
		this.text += text;
	}

	String getDescription() {
		
		String output = "[" + this.getEntity() + ":" + this.getValue() + ":" + this.getId() + ":" + this.getPosition() + "] \"" + this.getText() + "\"";

		return output;
	}
	
	String getId() {
		return id;
	}

	final void setId(String id) {
		this.id = id;
	}

	final int getPosition() {
		return position;
	}

	final void setPosition(int position) {
		this.position = position;
	}
	
	boolean isAuthority() {
		
		if(entity.equals(Entity.LEG_AUTH)) return true;
		if(entity.equals(Entity.EU_LEG_AUTH)) return true;
		if(entity.equals(Entity.CL_AUTH)) return true;
		if(entity.equals(Entity.EU_CL_AUTH)) return true;
		
		//Comprendere anche other authority?
		if(entity.equals(Entity.OTHER_AUTH)) return true;
		
		return false;
	}

	@Override
	public int compareTo(AnnotationEntity o) {

		return this.getId().compareTo(o.getId());
	}

	@Override
	public String toString() {
		
		return entity.toString() + " [" + id + ", text=" + Util.normalizeSpaces(text) + ", value=" + value + "]";
	}
	
}
