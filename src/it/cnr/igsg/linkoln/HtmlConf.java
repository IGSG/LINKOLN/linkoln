/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

public enum HtmlConf {
	
	/**
	 * In caso di testo in input piatto, aggiunge una generica intestazione di documento HTML al testo restituito da getHtml() (default: true)
	 */
	ADD_HEADER,
	
	/**
	 * In caso di testo in input piatto, racchiude il testo restituito da getHtml() nei tag &lt;PRE&gt; e &lt;/PRE&gt; (default: false)
	 */
	ADD_PRE, 
	
	/**
	 * In caso di testo in input piatto, aggiunge &amp;nbsp; per mantenere la formattazione simile al testo non annotato in input (default: true)
	 */
	ADD_ARTIFICIAL_SPACES,
	
	/**
	 * In caso di testo in input piatto, sostituisce i ritorni a capo con &lt;br/&gt; per mantenere la formattazione simile al testo non annotato in input (default: true)
	 */
	ADD_BR_TAGS,
	
	/**
	 * Aggiunge title con i valori normalizzati delle features del riferimento ai tag &lt;a&gt; e &lt;font&gt; prodotti da getHtml() (default: true)
	 */
	ADD_TITLES,
		
	/**
	 * Stampa i valori normalizzati delle features del riferimento come attributi del tag &lt;a&gt; prodotto da getHtml() (default: false)
	 */
	ADD_ATTRIBUTES,
		
	/**
	 * Aggiunge l'attributo target con valore "_blank" ai tag &lt;a&gt; (default: true)
	 */
	TARGET_BLANK,
		
	/**
	 * Trasforma &lt; e &gt; in &amp;lt; e &amp;gt;, utile per mostrare il formato LKN in Linkoln Web.
	 */
	EXPAND_LT_GT;
	
}
