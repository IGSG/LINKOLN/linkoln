/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Run {

	public static void main(String[] args) {
		
		/* Basic command-line run */

		System.out.println(Linkoln.getInfo());

		if(args.length < 1) {
			
			help();
			
			return;
		}

		String fileName = args[0].trim();
		
		String inputCharSet = null;
		
		if(args.length > 1) inputCharSet = args[1].trim();
		
		LinkolnDocument linkolnDocument = LinkolnDocumentFactory.createLinkolnDocumentFromFile(fileName, inputCharSet);

		Linkoln.run(linkolnDocument);
		
		linkolnDocument.skipWarnings(true);
		linkolnDocument.skipRefsWithoutUrl(true);

		if( !linkolnDocument.hasFailed()) {
			
			String lkn = linkolnDocument.getLinkolnAnnotatedText();

			try {
			
				if(lkn != null) Files.write(Paths.get(fileName + ".lkn"), lkn.getBytes());
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			String html = linkolnDocument.getHtmlAnnotatedText();

			try {
			
				if(html != null) Files.write(Paths.get(fileName + ".html"), html.getBytes());
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			String json = linkolnDocument.getJson();
			
			try {
			
				if(json != null) Files.write(Paths.get(fileName + ".json"), json.getBytes());
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			String csv = linkolnDocument.getCsv();
			
			try {
			
				if(csv != null) Files.write(Paths.get(fileName + ".csv"), csv.getBytes());
				
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		
		System.out.println("\nDone.");
	}
	
	private static void help() {
		
		System.out.println("Usage: fileName [charSet]");
	}

}
