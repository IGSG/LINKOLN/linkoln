/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

public enum InputType {

	/**
	 * Indica che il testo in input è piatto (default).
	 */
	PLAIN_TEXT,
	
	/**
	 * Indica che il testo in input è un documento HTML.
	 */
	HTML_DOCUMENT,
	
	/**
	 * Indica che il testo in input è un documento XML.
	 */
	XML_DOCUMENT,

	/**
	 * Indica che il testo in input non è un vero e proprio documento html o xml,
	 * ma è un testo (o frammento di testo) piatto con l'aggiunta di annotazioni 
	 * html/xml-like che devono essere mantenute.
	 */
	HYBRID_TEXT,

	/**
	 * Indica che il testo in input è un XML prodotto da IGSG-MARKER.
	 */
	XML_IGSG_MARKER,
	
	/**
	 * Indica che il testo in input è un provvedimento della Corte Costituzionale in formato XML prodotto da IGSG-MARKER-COST.
	 */
	XML_IGSG_COST;

}
