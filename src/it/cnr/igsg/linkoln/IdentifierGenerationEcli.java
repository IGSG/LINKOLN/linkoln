/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

import java.util.Set;

class IdentifierGenerationEcli {

	static boolean run(LinkolnDocument linkolnDocument, LinkolnReference linkolnReference) {
		
		/*
		 * TODO
		 * 
		 * Completare (aumentare) le tipologie di documento di TAR/CDS
		 * 
		 * text = "TAR Piemonte ord. 13 gennaio 1981, n. 17";
		 *  ==> https://e-justice.europa.eu/ecli/it/ECLI:IT:TAR_PIE:1981:17SENT.html
		 * 
		 */
		
		Identifier identifier = new Identifier();
		identifier.setType(Identifiers.ECLI);
		
		String auth = linkolnReference.getAuthority();
		String date = linkolnReference.getDate();
		String country = linkolnReference.getDefendant();

		if(auth != null && auth.equals("CEDU") && country != null) {

			String applicant = linkolnReference.getApplicant();
			
			if(applicant != null) {
				
				//Process applicant
		    	applicant = Util.stripAccents(applicant).toLowerCase().trim();

		    	if(applicant.endsWith("e altri")) applicant = applicant.substring(0, applicant.length()-"e altri".length()).trim();
		    	if(applicant.endsWith("and others")) applicant = applicant.substring(0, applicant.length()-"and others".length()).trim();
		    	if(applicant.endsWith("et al.")) applicant = applicant.substring(0, applicant.length()-"et al.".length()).trim();
		    	if(applicant.endsWith("et al")) applicant = applicant.substring(0, applicant.length()-"et al".length()).trim();
			}
			
			RecordCedu closestMatch = null;
			
			if(date != null && Repository.date_country2recordsCedu != null) {
				
				String key = date + "_" + country;
				
				Set<RecordCedu> records = Repository.date_country2recordsCedu.get(key);
				
				if(records != null && records.size() > 0) {
					
					if(records.size() == 1) {
						
						for(RecordCedu recordCedu : records) {
							
							closestMatch = recordCedu;
							break;
						}
						
					} else {
						
						if(applicant != null) {
							
							String[] tokensA = applicant.split(" ");
							
							int closestMatches = 0;
		
							for(RecordCedu recordCedu : records) {
								
								String[] tokensB = recordCedu.applicant.split(" ");
								
								int matches = 0;
								
								for(int i = 0; i < tokensA.length; i++) {
									
									String tokenA = tokensA[i].trim();
									
									for(int k = 0; k < tokensB.length; k++) {
										
										String tokenB = tokensB[k].trim();
										
										if(tokenA.equals(tokenB)) matches++;
									}
								}
								
								if(matches > closestMatches) {
									closestMatches = matches;
									closestMatch = recordCedu;
								}
								
							}
						}	
					}
					
				} else {
					
					if(linkolnDocument.isDebug()) System.err.println("Cedu Repository lookup failed for key: " + key);
				}
				
			} else {
				
				//Se non è specificata la data cerca applicant_country e linka all'intero fascicolo

				if(applicant != null && Repository.date_country2recordsCedu != null) {
					
					String key = applicant + "_" + country;
					
					closestMatch = Repository.applicant_country2recordCedu.get(key);
				}
			}
			
			if(closestMatch != null) {
				
				identifier.setCode(closestMatch.ecli);
			
				//identifier.setUrl("http://hudoc.echr.coe.int/eng#{%22ecli%22:[%22" + hudocEcli + "%22]}");
				
				//TODO per le url si potrebbe usare direttamente l'item-id: https://hudoc.echr.coe.int/eng#{"itemid":["001-99612"]}
				
				//TODO oppure APPLICATION NUMBER, es.: https://hudoc.echr.coe.int/eng#{%22appno%22:[%222699/03%22]}   <=== FORSE è tutto il fascicolo
				
				identifier.setUrl("http://hudoc.echr.coe.int/eng#{%22itemid%22:[%22" + closestMatch.itemId + "%22]}");
				
				linkolnReference.addIdentifier(identifier);
			}
		}
		
		//Risolutore portale e-Justice
		//String urlPrefix = "https://e-justice.europa.eu/ecli/";  //Funziona meglio così: https://e-justice.europa.eu/ecli/it/ECLI:CODE.html 
		String urlPrefix = "https://e-justice.europa.eu/ecli/it/";
		
		String number = linkolnReference.getNumber();
		String subject = linkolnReference.getArea();
		String docType = linkolnReference.getDocType();
		String year = linkolnReference.getYear();
		String section = linkolnReference.getAuthoritySection();
		String region = linkolnReference.getRegion();
		
		if(year == null && linkolnReference.getDate() != null) {
			//System.out.println("REF: " + linkolnReference.getText());
			//System.out.println("DATE: " + linkolnReference.getDate());
			year = linkolnReference.getDate().substring(0, 4);
		}

		//if(auth == null || year == null || number == null) return false;
		
		if(auth == null) return false;
		
		if(auth.equals("CORTE_COST")) {
			
			/*
			 * Se è disponibile ANNO e NUMERO, si ignora il TIPO_DOC nella ricerca del Repository:
			 * - se non si trova il match si segnala il caso e non si crea l'identificatore
			 * - se si trova un match si crea l'identificatore e si fa una segnalazione nel caso il tipo doc non matchi.
			 * 
			 * 
			 */
				
			//Fai passare tutti i riferimenti post 2023
			boolean yearInRepository = true;
			
			if(year != null || date != null) {
				
				String y = year;
				
				if(y == null && date != null) y = date.substring(0, 4);
				
				try {
					Integer cutYear = null;
					if(y != null) cutYear = Integer.valueOf(y);

					if(cutYear != null && cutYear > 2023) yearInRepository = false;
					
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
			}

			if(yearInRepository && linkolnDocument.isLoadCostRepository()) {
				
				/*
				 * CON REPOSITORY
				 */
				
				
				//!Repository.checkRepositoryCost(linkolnReference)) return false;
				
				String ecli = null;
				
				if(number != null && year != null) {
					
					ecli = Repository.checkRepositoryCost(number, year, null, null);
					
				} else if(docType != null && date != null) {
					
					ecli = Repository.checkRepositoryCost(null, null, date, docType);
					
				} else {
					
				}
				
				if(ecli == null) {
					
					System.out.println("Warning -- Verifica citazione Cost -- docType:" + docType + " number:" + number + " year:" + year + " date:" + date + " text:" + linkolnReference.getText() + " context:" + linkolnReference.getContext());
					
				} else {
					
					urlPrefix = "http://www.cortecostituzionale.it/actionSchedaPronuncia.do?param_ecli=";

					identifier.setCode(ecli);
					identifier.setUrl(urlPrefix + identifier.getCode());
					linkolnReference.addIdentifier(identifier);
					
					return true;
				}
				
			} else {

				/*
				 * SENZA REPOSITORY
				 */
				
				if(year == null || number == null) return false;
				
				//Per la Corte Costituzionale è disponibile questo risolutore:
				urlPrefix = "http://www.cortecostituzionale.it/actionSchedaPronuncia.do?param_ecli=";

				identifier.setCode("ECLI:IT:COST:" + year + ":" + number);
				identifier.setUrl(urlPrefix + identifier.getCode());
				linkolnReference.addIdentifier(identifier);
				
				return true;
			}
			
			/*
			//Check Repository Corte Costituzionale
			if(linkolnDocument.isLoadCostRepository() && !Repository.checkRepositoryCost(linkolnReference)) return false;

			//Per la Corte Costituzionale è disponibile questo risolutore:
			urlPrefix = "http://www.cortecostituzionale.it/actionSchedaPronuncia.do?param_ecli=";

			identifier.setCode("ECLI:IT:COST:" + year + ":" + number);
			identifier.setUrl(urlPrefix + identifier.getCode());
			linkolnReference.addIdentifier(identifier);
			
			return true;
			*/
		}

		if(auth == null || year == null || number == null) return false;
		
		if(auth.equals("CORTE_CASS")) {
			
			if(subject != null) {

				if(subject.toLowerCase().startsWith("civ")) subject = "CIV";
				if(subject.toLowerCase().startsWith("pen")) subject = "PEN";
			} 
			
			//MAKE A GUESS
			
			if(subject == null && (linkolnReference.getApplicant() != null || linkolnReference.getDefendant() != null)) {
				
				//Solitamente se sono specificate le parti siamo nel penale
				subject = "PEN";
			}
			
			//look at the doc metadata
			if(subject == null && linkolnDocument.getInputArea() != null) {
				
				if(linkolnDocument.getInputArea().trim().toLowerCase().startsWith("civ")) subject = "CIV";
				if(linkolnDocument.getInputArea().trim().toLowerCase().startsWith("pen")) subject = "PEN";
			}

			if(subject == null && linkolnDocument.getInputAuthority() != null && linkolnDocument.getInputAuthority().equalsIgnoreCase("CORTE_COST")) {
				
				//Imposta CIVILE come default per CORTE_COST ??
				
				//Fai un guess un po' più preciso guardando eventuali riferimenti ai codici presenti nello stesso testo
				
				if( linkolnDocument.hasCodiceCivile && !linkolnDocument.hasCodicePenale) subject = "CIV";
				
				if( !linkolnDocument.hasCodiceCivile && linkolnDocument.hasCodicePenale) subject = "PEN";
				
			}
			
			if(subject == null) { 
				
				//TODO guess sector based on other references (eg.: many references to the civil or penal code)
				
				return false;
			}
			
			identifier.setCode("ECLI:IT:CASS:" + year + ":" + number + subject);
			identifier.setUrl(urlPrefix + identifier.getCode() + ".html");
			linkolnReference.addIdentifier(identifier);
			
			return true;
		}
		
		//Corte dei Conti
		if(auth.equals("CORTE_CONTI") && year != null && number != null && section != null) {
			
			String codiceSezione = "";
			
			//TODO Verificare i codici delle sezioni
			
			if(section.equals("UNITE")) codiceSezione = "SSRR";

			if(section.equals("AUTONOMIE")) codiceSezione = "";
			
			if(section.equals("GIURISD_REGIONALE") && region != null) {

				codiceSezione = "SG" + region;
			}

			if(section.equals("RIUNITE_CONTROLLO") && region != null) {

				codiceSezione = "SRC" + region;
			}

			if( !codiceSezione.equals("")) {
				
				identifier.setCode("ECLI:IT:CONT:" + year + ":" + number + codiceSezione);
				identifier.setUrl(urlPrefix + identifier.getCode() + ".html");
				linkolnReference.addIdentifier(identifier);

			}
			
			//TODO Usare il sistema di risoluzione di banchedati.corteconti.it ?
			
			
			/*
			//TODO Manca la sezione della Corte dei Conti
			String codiceSezione = ""; //SSRR, SGPIE, APP2, etc.
			
			if(auth.length() > 9) {
				//Es.: "IT_CONT_FVG"
				
				codiceSezione = "SRC" + auth.substring(8);
			}
			
			if( !codiceSezione.equals("")) {
				
				linkolnIdentifier.setCode("ECLI:IT:CONT:" + year + ":" + number + codiceSezione);

				//TODO tipo documento
				String codiceTipoDoc = "";
				
				//Controlla prima se il codiceTipoDoc è nel number stesso
				String fullNumber = "";
				if(entity.getRelatedEntity("NUMBER") != null) {
					
					fullNumber = entity.getRelatedEntity("NUMBER").getValue();
				}
				if(fullNumber.indexOf("-") > -1) codiceTipoDoc = fullNumber.substring(fullNumber.indexOf("-")+1);
				if(codiceTipoDoc.indexOf("-") > -1) {
					
					codiceTipoDoc = codiceTipoDoc.substring(codiceTipoDoc.indexOf("-")+1);
					
				} else {
				
					if(docType != null) {
						
						if(docType.equals("JUDGMENT")) codiceTipoDoc = "SENT";
						if(docType.equals("PARERE")) codiceTipoDoc = "PAR";
						//if(docType.equals("DELIBERA")) codiceTipoDoc = "DEL"; //???
						//TODO
					}
				}
				
				//Altro sistema di identificazione e URL: 
				//https://banchedati.corteconti.it/documentDetail/SRCLAZ/151/2013/PAR
				//https://banchedati.corteconti.it/documentDetail/SRCTOS/301/2009/REG
				//Altro stile:
				//https://banchedati.corteconti.it/documentDetail/MARCHE/SENTENZA/92/2017

				linkolnIdentifier.setUrl("https://banchedati.corteconti.it/documentDetail/" + codiceSezione + "/" + number + "/" + year + "/" + codiceTipoDoc);
				return linkolnIdentifier;
			}
			*/
		}
		
		//Consiglio di Stato
		if(auth.equals("CONS_STATO") && year != null && number != null) {
			
			//Se non è specificato docTupe assumi comunque Sentenza
			if(docType == null || docType.equals("SENT")) {
			
				identifier.setCode("ECLI:IT:CDS:" + year + ":" + number + "SENT");
				identifier.setUrl(urlPrefix + identifier.getCode() + ".html");
				linkolnReference.addIdentifier(identifier);
			}
		}
		
		//Consiglio di Giustizia Amministrativa per la Regione Sicilia
		if(auth.equals("CONS_GIUST_AMM_SICILIA") && year != null && number != null && docType != null) {
			
			if(docType.equals("SENT")) {
				
				identifier.setCode("ECLI:IT:CGARS:" + year + ":" + number + "SENT");
				identifier.setUrl(urlPrefix + identifier.getCode() + ".html");
				linkolnReference.addIdentifier(identifier);
			}
		}
		
		//Tribunali Amministrativi Regionali
		if(auth.equals("TRIB_AMM_REG") && year != null && number != null && region != null && docType != null) {

			if(docType.equals("SENT")) {
				
				identifier.setCode("ECLI:IT:TAR" + region + ":" + year + ":" + number + "SENT");
				identifier.setUrl(urlPrefix + identifier.getCode() + ".html");
				linkolnReference.addIdentifier(identifier);
			}
		}
		
		return false;
	}
}
