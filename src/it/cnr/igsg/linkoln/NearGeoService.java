/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

import java.util.ArrayList;

class NearGeoService {

	//NOTA: per il momento il servizio è usato solo per atti normativi regionali/provinciali

	//Nota: per il momento non impostare MAX_DIST
	//private static final int MAX_DIST = 512;

	NearGeoService() { }
	
	boolean run(LinkolnDocument linkolnDocument) {

		ReferenceClusters referenceClusters = linkolnDocument.getReferenceClusters();
		
		if(referenceClusters == null) return false;
		
		if(linkolnDocument.isDebug()) System.out.println("Running NearGeoService...");
		
		ArrayList<Reference> references = new ArrayList<Reference>();
		ArrayList<AnnotationEntity> regions = new ArrayList<AnnotationEntity>();
		ArrayList<AnnotationEntity> cities = new ArrayList<AnnotationEntity>();
		
		for(AnnotationEntity ae : linkolnDocument.getAnnotationEntities()) {
			
			if(ae instanceof Reference) {
				
				Reference ref = (Reference) ae;
				
				//System.out.println("AE in " + ae);
				
				if(referenceClusters.getAlias(ref) != null) continue;
				
				//Il docType deve essere specificato, per il momento considera leggi regionali/provinciali e statuti regionali
				if(referenceClusters.getDocumentType(ref) == null) continue;
				
				//Se doctype non è statuto e nemmeno legge (o regolamento o delibera), ignora 
				if( !referenceClusters.getDocumentType(ref).getValue().equals("STATUTO") && !referenceClusters.getDocumentType(ref).getValue().equals("L") 
						&& !referenceClusters.getDocumentType(ref).getValue().equals("REG") && !referenceClusters.getDocumentType(ref).getValue().equals("DEL")) continue;  

				//Per i casi in cui è legge (o regolamento o delibera), deve essere specificata l'autorità
				if( ( referenceClusters.getDocumentType(ref).getValue().equals("L") || referenceClusters.getDocumentType(ref).getValue().equals("REG") || referenceClusters.getDocumentType(ref).getValue().equals("DEL") ) 
						&& referenceClusters.getAuthority(ref) == null) continue;
				
				//Per i casi in cui è legge (o regolamento o delibera), deve essere specificato regionale, provinciale o comunale (o anche REGIONE_CONS)
				if(referenceClusters.getDocumentType(ref).getValue().equals("L") && 
					( !referenceClusters.getAuthority(ref).getValue().equals("REGIONE")	&& !referenceClusters.getAuthority(ref).getValue().equals("PROVINCIA") 
						&& !referenceClusters.getAuthority(ref).getValue().equals("COMUNE") && !referenceClusters.getAuthority(ref).getValue().equals("REGIONE_CONS") ) ) continue;

				//Deve mancare la specificazione della regione e della città
				if(referenceClusters.getRegion(ref) != null || referenceClusters.getCity(ref) != null) continue;
				
				references.add((Reference) ae);
				
				//System.out.println("AE out " + ae);
			}
			
			if(ae.getEntity().equals(Entity.REGION)) regions.add(ae);
			if(ae.getEntity().equals(Entity.CITY)) cities.add(ae);
		}
		
		for(Reference ref : references) {
			
			AnnotationEntity geo = null;
			
			if( ( referenceClusters.getAuthority(ref) != null && ( referenceClusters.getAuthority(ref).getValue().equals("REGIONE") || referenceClusters.getAuthority(ref).getValue().equals("REGIONE_CONS") ) ) ||
					( referenceClusters.getDocumentType(ref) != null && referenceClusters.getDocumentType(ref).getValue().equals("STATUTO") ) ) {
				
				geo = getClosestRegion(regions, ref);
			
			} else {

				geo = getClosestCity(cities, ref);
			}
		
			if(geo != null) {
				
				ref.addRelatedEntity(geo);
				
				referenceClusters.addGeo(ref, geo);
			}
		}
		
		return false;
	}
	
	private AnnotationEntity getClosestRegion(ArrayList<AnnotationEntity> regions, Reference ref) {
		
		AnnotationEntity region = null;
		
		Integer distance = null;
		
		for(AnnotationEntity a : regions) {
			
			int currentDistance = Math.abs(ref.getPosition() - a.getPosition());
			
			if(distance == null || (currentDistance > 0 && currentDistance < distance)) {
				
				region = a;
				distance = currentDistance;
			}
		}
		
		return region;
	}
	
	private AnnotationEntity getClosestCity(ArrayList<AnnotationEntity> cities, Reference ref) {
		
		AnnotationEntity city = null;
		
		Integer distance = null;
		
		for(AnnotationEntity a : cities) {
			
			int currentDistance = Math.abs(ref.getPosition() - a.getPosition());
			
			if(distance == null || (currentDistance > 0 && currentDistance < distance)) {
				
				city = a;
				distance = currentDistance;
			}
		}
		
		return city;
	}
	
}
