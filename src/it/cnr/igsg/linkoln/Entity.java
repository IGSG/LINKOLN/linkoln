/*******************************************************************************
 * Copyright (c) 2022-2029 Institute of Legal Information and Judicial Systems (IGSG-CNR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *  
 * Authors: Lorenzo Bacci (IGSG-CNR)
 ******************************************************************************/
package it.cnr.igsg.linkoln;

enum Entity {

	EU_LEG_ALIAS,
	EU_CL_ALIAS,
	LEG_ALIAS,
	CL_ALIAS,
	
	CL_AUTH,
	LEG_AUTH,
	EU_CL_AUTH,
	EU_LEG_AUTH,
	OTHER_AUTH,
	
	CL_SECTION,
	CL_DETACHED,

	MINISTRY,
	
	EU_ACRONYM,
	
	DOCTYPE,
	CL_DOCTYPE,
	LEG_DOCTYPE,
	EU_LEG_DOCTYPE,
	
	NUMBER,
	SIMPLE_NUMBER, //annotazione temporanea per numerazioni ambigue
	NUMBER_YEAR,
	FULL_NUMBER,
	FULL_EU_NUMBER,
	YEAR,
	DOC_DATE,
	DEPOSIT_DATE,
	NOTIFICATION_DATE,
	PUBLICATION_DATE,
	WRONG_DATE,

	ALTNUMBER,
	CASENUMBER,
	
	SUBJECT,

	PARTY, //TODO serve?
	APPLICANT,
	VERSUS,
	DEFENDANT,
	NAMED_ENTITY,
	
	COUNTRY,
	REGION,
	CITY,
	MUNICIPALITY,

	ECLI,
	
	RV_NUMBER, //numero massima
	RO_NUMBER, //registro ordinanze
	GU_ATTR,

	REF, //partial reference, can include partition
	CL_REF, //case law, can include partition
	EU_CL_REF, //european case law, can include partition
	LEG_REF, //legislation, can include partition
	EU_LEG_REF, //european legislation, can include partition
	
	LEG_REF_PART, //already includes partition
	EU_LEG_REF_PART, //already includes partition
	
	WARNING,
	
	PARTITION,
	ANNEX,
	ARTICLE,
	COMMA,
	PARAGRAPH,
	LETTER,
	ITEM,
	SENTENCE, //periodo
	POINT, //punto, elemento lista simbolica
	PARTE, //parte
	
	STPW, //stopwords
	
	MASK, //maschera temporaneamente testo ambiguo
	BREAK, //annotazione priva di testo che interrompe i pattern
	MISC, //testo annotato che non blocca i pattern (filler di testo)
	IGN; //testo annotato che blocca i pattern (ignore)
	
	static Entity findByName(String name) {
		
		for(Entity entity : values()) {
			
			if(name.equals(String.valueOf(entity))) return entity;
		}
		
		return null;
	}
}
