<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:mrk="https://www.igsg.cnr.it/marker/"
	xmlns:lkn="https://www.igsg.cnr.it/linkoln/"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/HTML/1998/html4"
	xmlns:h="http://www.w3.org/1999/xhtml"
	xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:date="http://exslt.org/dates-and-times">



	<!-- https://codice-amministrazione-digitale.readthedocs.io/it/latest/ -->
	<!-- https://iamkate.com/code/tree-views/ -->
	<!-- https://stackoverflow.com/questions/25807672/overflow-yscroll-is-not-working-for-chrome  -->
	

	<xsl:output method="html" indent="yes" />


	<xsl:template match="/">
		<html>
			<head>
				<title>IGSG Marker</title>
				<link href="./../xsl/marker-toc.css" rel="stylesheet" />
			</head>
			<body>
			
			 	<div class="meta">
		  			<xsl:apply-templates select = "//mrk:metadata" />
		  		</div>
		  		
		  		<div class="toc">
		  			<xsl:apply-templates select = "//mrk:metadata" mode="toc"/>
		  			<xsl:apply-templates select = "//mrk:documento" mode="toc"/>
		  			<xsl:apply-templates select = "//mrk:allegato" mode="toc"/>	
		  		</div>
		  	
		  		<div class="doc">
		  			<xsl:apply-templates select = "//mrk:documento" />
		  			<xsl:apply-templates select = "//mrk:allegato" />
		  		</div>
		  		
			</body>
		</html>
	</xsl:template>

	<xsl:variable name="source" select="//*[name()='root']/@source">
	</xsl:variable>

	<!-- Se è presente l'attributo URL crea un <a>, altrimenti <font color blue> -->
	<xsl:template match='lkn:ref[@url]'>
		<a href="{./@url}">
			<xsl:apply-templates />	
		</a>
	</xsl:template>

	<xsl:template match='lkn:ref'>
		<font color='blue'>
			<xsl:apply-templates />	
		</font>
	</xsl:template>

	<xsl:template match='lkn:warning'>
		<font color='red'>
			<xsl:apply-templates />	
		</font>
	</xsl:template>


	<!-- ======================================================== -->
	<!-- 														  -->
	<!-- 					 Template TOC 						  -->
	<!-- 														  -->
	<!-- ======================================================== -->
	
	<xsl:template match='mrk:metadata' mode ="toc">
		<xsl:variable name="doc-type">
		        <xsl:value-of select="//mrk:tipoDoc"/>
		</xsl:variable>
		<xsl:variable name="doc-date">
		        <xsl:value-of select="//mrk:dataDoc"/>
		</xsl:variable>
		<xsl:variable name="doc-num">
		        <xsl:value-of select="//mrk:numeroDoc"/>
		</xsl:variable>
		<p>
			<span class="titolo_toc"><xsl:value-of select="concat(concat(concat(concat($doc-type,'&#160;'),$doc-date),'&#160;'),$doc-num)"/></span>
		</p>
	</xsl:template>
	
	<xsl:template match='mrk:documento' mode="toc" >
		<!-- 
		<a class="anchor" href="#{local-name(.)}" title="vai a: #{local-name(.)}">
			<xsl:value-of select="local-name(.)"/> 
	    </a>
		<ul class="treeview">
			<xsl:apply-templates select = "mrk:testa" mode="toc"/>
			<xsl:apply-templates select = "mrk:corpo" mode="toc"/>
			<xsl:apply-templates select = "mrk:conclusione" mode="toc"/>
		</ul>
		 -->
		<ul class="treeview">
			<xsl:apply-templates mode ="toc"/>
		</ul>

	</xsl:template>
	
	<xsl:template match='mrk:testa' mode="toc" >
		<li>
			 <a class="anchor" href="#{local-name(.)}" title="vai a: #{local-name(.)}">
				  <!--<xsl:value-of select="local-name(.)"/>-->
				  Intestazione
			  </a>
		</li>
	</xsl:template>
	
	<xsl:template match='mrk:corpo' mode="toc" >
		<!--  titolo - capo - sezione - articolo - [comma] - [lettera] -->
		<li>
			<a class="anchor" href="#{local-name(.)}" title="vai a: #{local-name(.)}" >
				  <!--<xsl:value-of select="local-name(.)"/>-->
				  Articolato
			</a>		
		</li>
		<xsl:apply-templates mode="toc"/>
	</xsl:template>
	
	<xsl:template match='mrk:libro' mode='toc'>
		 <details open="true">
		 	<summary>
				<a class="anchor" href="#{@id}" title="vai a: #{@id}">
					  <xsl:value-of select="./mrk:intestazione"/>
				  </a>
				  <xsl:if test="./mrk:rubrica"> 
					   <span class="rubrica_toc">
					   		<xsl:value-of select="concat('&#160;-&#160;',./mrk:rubrica)"/>
					   	</span>
				  </xsl:if>
			 </summary>
			 <ul class="align_left">
			  	<xsl:apply-templates mode="toc"/>
			 </ul>
		 </details>
	</xsl:template>
	
	<xsl:template match='mrk:parte' mode='toc'>
		 <details open="true">
		 	<summary>
				<a class="anchor" href="#{@id}" title="vai a: #{@id}">
					  <xsl:value-of select="./mrk:intestazione"/>
				  </a>
				  <xsl:if test="./mrk:rubrica"> 
					   <span class="rubrica_toc">
					   		<xsl:value-of select="concat('&#160;-&#160;',./mrk:rubrica)"/>
					   	</span>
				  </xsl:if>
			 </summary>
			 <ul class="align_left">
			  	<xsl:apply-templates mode="toc"/>
			 </ul>
		 </details>
	</xsl:template>
	 	
	<xsl:template match='mrk:titolo' mode='toc'>
		 <details open="true">
		 	<summary>
				<a class="anchor" href="#{@id}" title="vai a: #{@id}">
					  <xsl:value-of select="./mrk:intestazione"/>
				  </a>
				  <xsl:if test="./mrk:rubrica"> 
					   <span class="rubrica_toc">
					   		<xsl:value-of select="concat('&#160;-&#160;',./mrk:rubrica)"/>
					   	</span>
				  </xsl:if>
			 </summary>
			 <ul class="align_left">
			  	<xsl:apply-templates mode="toc"/>
			 </ul>
		 </details>
	</xsl:template>
	
	<xsl:template match='mrk:capo' mode='toc'>
		 <details open="true">
		 	<summary>
				<a class="anchor" href="#{@id}" title="vai a: #{@id}">
					  <xsl:value-of select="./mrk:intestazione"/>
				  </a>
				  <xsl:if test="./mrk:rubrica"> 
					   <span class="rubrica_toc">
					   		<xsl:value-of select="concat('&#160;-&#160;',./mrk:rubrica)"/>
					   	</span>
				  </xsl:if>
			 </summary>
			<ul class="align_left">
			  	<xsl:apply-templates mode="toc"/>
			</ul>
		 </details>
	</xsl:template>
	
	<xsl:template match='mrk:sezione' mode='toc'>
		 <details open="true">
		 	<summary>
				<a class="anchor" href="#{@id}" title="vai a: #{@id}">
					  <xsl:value-of select="./mrk:intestazione"/>
				  </a>
				  <xsl:if test="./mrk:rubrica"> 
					   <span class="rubrica_toc">
					   		<xsl:value-of select="concat('&#160;-&#160;',./mrk:rubrica)"/>
					   	</span>
				  </xsl:if>
			 </summary>
			 <ul class="align_left">
			  	<xsl:apply-templates mode="toc"/>
			 </ul>
		 </details>
	</xsl:template>
	
	
	<xsl:template match='mrk:articolo' mode='toc'>
		 <details>
		 	<summary>
				<a class="anchor" href="#{@id}" title="vai a: #{@id}">
					  <xsl:value-of select="./mrk:intestazione"/>
				  </a>
				  <xsl:if test="./mrk:rubrica">  
					   <span class="rubrica_toc">
					   		<xsl:value-of select="concat('&#160;-&#160;',./mrk:rubrica)"/>
					   	</span>
				  </xsl:if>
			 </summary>
			 <ul class="align_left">
			  	<xsl:apply-templates mode="toc"/>
			 </ul>
		 </details>
	</xsl:template>
	
	
	<!-- leaf se non ha figli come COST -->
	<xsl:template match='mrk:comma' mode='toc'>
		<xsl:choose>
		<!--  FIXME: è sufficiente? test se ha discendenti lettera o numero -->
		  <xsl:when test="mrk:paragrafo/mrk:lettera or mrk:paragrafo/mrk:numero">  
		  	<li>  
			 <details>
			 	<summary>
					<a class="anchor" href="#{@id}" title="vai a: #{@id}">
						  <xsl:value-of select="./mrk:intestazione"/>
					  </a>
				 </summary>
				 <ul class="align_left">
				  	<xsl:apply-templates mode="toc"/>
				 </ul>
			 </details>
			  </li>
		  </xsl:when>
		  <xsl:otherwise>
		  	<li> 
			  <a class="anchor" href="#{@id}" title="vai a: #{@id}">
				  	<xsl:value-of select="./mrk:intestazione"/>
			  </a>
			  <ul class="align_left">
				  	<xsl:apply-templates mode="toc"/>
			  </ul>
			  </li>
		  </xsl:otherwise>
		</xsl:choose>	
	</xsl:template>
	
	
	<xsl:template match='mrk:paragrafo' mode='toc'>
		<xsl:apply-templates select='mrk:lettera'  mode="toc"/>
	</xsl:template>

	<!-- FIXME - leaf se non ha figli come COST -->
	<xsl:template match='mrk:lettera' mode='toc'>
		<xsl:choose>
		<!--  test se ha discendenti lettera o numero -->
		  <xsl:when test="mrk:numero">
		  	<li>   
			 <details>
			 	<summary>
					<a class="anchor" href="#{@id}" title="vai a: #{@id}">
						  <xsl:value-of select="./mrk:intestazione"/>
					  </a>
				 </summary>
				 <ul class="align_left">
				  	<xsl:apply-templates select='mrk:numero' mode="toc"/>
				 </ul>
			 </details>
			 </li>
		  </xsl:when>
		  <xsl:otherwise>
		  	<li>
			  <a class="anchor" href="#{@id}" title="vai a: #{@id}">
				  	<xsl:value-of select="./mrk:intestazione"/>
			  </a>
			  <ul class="align_left">
				  	<xsl:apply-templates  select='mrk:numero' mode="toc"/>
			  </ul>
			  </li>
		  </xsl:otherwise>
		</xsl:choose>	
	</xsl:template>
	
	
	<!-- FIXME - fai template come lettera con figli numero -->
	<xsl:template match='mrk:numero' mode='toc'>
		
		  	<li>
			  <a class="anchor" href="#{@id}" title="vai a: #{@id}">
				  	<xsl:value-of select="./mrk:intestazione"/>
			  </a>
			</li>
		 
	</xsl:template>
	
	
	<xsl:template match='mrk:allegato' mode="toc" >
		<ul class="treeview">
			<li>
			 	<a class="anchor" href="#{@id}" title="vai a: #{@id}">
					<!-- <xsl:value-of select="local-name(.)"/>  -->
					<xsl:value-of select="./mrk:intestazione"/>
			  	</a>
			</li>
		</ul>
			
	</xsl:template>
	
	<xsl:template match="*" mode='toc'/>
	 
	
	<xsl:template match='mrk:conclusione' mode="toc" >
		<li>
			 <a class="anchor" href="#{local-name(.)}" title="vai a: #{local-name(.)}">
				  <!--<xsl:value-of select="local-name(.)"/> -->
				  Conclusione
			  </a>
		</li>
	</xsl:template>

	<!-- ======================================================== -->
	<!-- -->
	<!-- Template articolato -->
	<!-- -->
	<!-- ======================================================== -->

	<xsl:template match='mrk:corpo'>
		<a name="{local-name(.)}"></a>
		<div>
			<xsl:apply-templates />
		</div>
	</xsl:template>
	
	
	<xsl:template match='h:p'>
		<p style= "{./@style}">
			<xsl:apply-templates />
		</p>
	</xsl:template>
	
	<xsl:template match='h:br'>	
		<br/>
	</xsl:template>
	
	<xsl:template match='h:table'>
		<a name="{@id}"></a>
		<table class="table">
			<xsl:apply-templates />
		</table>
	</xsl:template>
	
	<xsl:template match='h:th'>
		<th class = "th">
			<xsl:apply-templates />
		</th>
	</xsl:template>
	
	<xsl:template match='h:tr'>
		<tr class = "tr">
			<xsl:apply-templates />
		</tr>
	</xsl:template>
	
	<xsl:template match='h:td'>
		<td class = "td" style= "{./@style}" colspan= "{./@colspan}" rowspan= "{./@rowspan}">
			<xsl:apply-templates />
		</td>
	</xsl:template>
	
	<xsl:template match='h:pre'>
		<pre>
			<xsl:apply-templates />
		</pre>
	</xsl:template>

	<xsl:template match='mrk:documento'>
		<a name="{local-name(.)}"></a>	
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template match='mrk:testa'>
		<a name="{local-name(.)}"></a>	
		<div class="header">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<xsl:template match='mrk:intestazioneDoc'>
			<xsl:apply-templates />
	</xsl:template>

	<xsl:template match='mrk:emananteDoc'>
		<div class="emananteDoc">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<xsl:template match='mrk:tipoDoc'>
		<span class="documentType">
			<xsl:apply-templates />
		</span>
	</xsl:template>
	
	<xsl:template match='mrk:numeroDoc'>
		<span class="documentNumber">
			<xsl:apply-templates />
		</span>
	</xsl:template>
	
	<xsl:template match='mrk:codiceDoc'>
		<span class="documentCode">
			<xsl:apply-templates />
		</span>
	</xsl:template>
	
	<xsl:template match='mrk:dataDoc'>
		<span class="documentDate">
			<xsl:apply-templates />
		</span>
	</xsl:template>
	
	<xsl:template match='mrk:titoloDoc'>
		<div class="documentTitle">
			<xsl:apply-templates />
		</div>
	</xsl:template>
	
	<xsl:template match='mrk:preambolo'>
		<div class="preamble">
			<xsl:apply-templates />
		</div>
	</xsl:template>
	
	
	<!-- ========================== LIBRO ============================== -->
	<xsl:template match='mrk:libro'>
		<a name="{@id}"></a>
		<div class="libro">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<!-- ========================== PARTE ============================== -->
	<xsl:template match='mrk:parte'>
		<a name="{@id}"></a>
		<div class="parte">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<!-- ========================== TITOLO ============================== -->
	<xsl:template match='mrk:titolo'>
		<a name="{@id}"></a>
		<div class="titolo">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<!-- ========================== SEZIONE ============================== -->
	<xsl:template match='mrk:sezione'>
		<a name="{@id}"></a>
		<div class="sezione">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<!-- ========================== CAPO ============================== -->
	<xsl:template match='mrk:capo'>
		<a name="{@id}"></a>
		<div class="capo">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<!-- ========================== RUBRICA ============================== -->

	<xsl:template match='mrk:rubrica'>
		<a name="{@id}"></a>
		<p class="rubrica">
			<xsl:apply-templates />
		</p>
	</xsl:template>


	<!-- ======================== INTESTAZIONE =========================== -->


	<xsl:template match='mrk:intestazione'>
	  <xsl:element name="span">
		<xsl:attribute name="class">
			<xsl:value-of select ="concat('intestazione_',local-name(./..))"/>
		</xsl:attribute>
		<xsl:attribute name="title"><xsl:value-of select="./../@id" /></xsl:attribute>
			
		<xsl:apply-templates />	
	  </xsl:element>
	</xsl:template>
	
	



	<!-- ========================= ARTICOLO =============================== -->

	<xsl:template match='mrk:articolo'>
		<a name="{@id}"></a>
		<div class="articolo">
			<xsl:apply-templates />
		</div>
	</xsl:template>


	<!-- ========================= COMMA e sotto comma =============================== -->

	<xsl:template match='mrk:comma'>
		<a name="{@id}"></a>
		<div class="comma">
			<xsl:apply-templates />
		</div>
	</xsl:template>
	


	
	<xsl:template match='mrk:paragrafo'>
		<a name="{@id}"></a>
		<xsl:choose>
		    <!-- primo paragrafo di un comma numerato: non andare a capo -->
			<xsl:when test="name(preceding-sibling::*[1])='mrk:intestazione' and name(./..)='mrk:comma'" >
				<span>
					<xsl:apply-templates />
				</span>
			</xsl:when>
			<xsl:otherwise>
			   <xsl:choose>
			        <!-- @id ends-with "paragraph-i" -->
					<xsl:when test="'paragraph-i'=substring(@id, string-length(@id) - string-length('paragraph-i') +1)" >
						<p class="primo_paragrafo">
							<xsl:apply-templates />
						</p>
					</xsl:when>
			       <xsl:otherwise>
			            <p class="paragrafo">
							<xsl:apply-templates />
						</p>
			       </xsl:otherwise>
			    </xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	

	

	
	<!-- ========================= EL , EN , EP =============================== -->
	<xsl:template match='mrk:lettera'>
		<a name="{@id}"></a>
		<p class="lettera">
			<xsl:apply-templates />
		</p>
	</xsl:template>

	<xsl:template match='mrk:numero'>
		<a name="{@id}"></a>
		<p class="numero">
			<xsl:apply-templates />
		</p>
	</xsl:template>
	
	
	<xsl:template match='mrk:sottoNumero'>
		<a name="{@id}"></a>
		<p class="sottoNumero">
			<xsl:apply-templates />
		</p>
	</xsl:template>
	
	<xsl:template match='mrk:numeroLatino'>
		<a name="{@id}"></a>
		<p class="numeroLatino">
			<xsl:apply-templates />
		</p>
	</xsl:template>


	<xsl:template match='mrk:virgolette'>
		<xsl:choose>
			<xsl:when test="*" >
				<div class="virgolette_struttura">
					<xsl:apply-templates />
				</div>		
			</xsl:when>
			<xsl:otherwise>
				<span class="virgolette_parola">
					<xsl:apply-templates />
				</span>
			</xsl:otherwise>
		</xsl:choose>	
	</xsl:template>

	<xsl:template match='mrk:parole'>
	  <xsl:element name="span">
		<xsl:attribute name="class">parole</xsl:attribute>
		<xsl:attribute name="title">Parole tra virgolette</xsl:attribute>
		<xsl:apply-templates />	
	  </xsl:element>
	</xsl:template>

	<xsl:template match='mrk:parole_inserimento'>
	  <xsl:element name="span">
		<xsl:attribute name="class">inserimento</xsl:attribute>
		<xsl:attribute name="title">Modifica di parole - Parole da inserire</xsl:attribute>
		<xsl:apply-templates />	
	  </xsl:element>
	</xsl:template>

	<xsl:template match='mrk:parole_sostituzione'>
	  <xsl:element name="span">
		<xsl:attribute name="class">sostituzione</xsl:attribute>
		<xsl:attribute name="title">Modifica di parole - Parole da sostituire</xsl:attribute>
		<xsl:apply-templates />	
	  </xsl:element>
	</xsl:template>

	<xsl:template match='mrk:parole_soppressione'>
	  <xsl:element name="span">
		<xsl:attribute name="class">soppressione</xsl:attribute>
		<xsl:attribute name="title">Modifica di parole - Parole soppresse</xsl:attribute>
		<xsl:apply-templates />	
	  </xsl:element>
	</xsl:template>

	<xsl:template match='mrk:parole_dopo'>
	  <xsl:element name="span">
		<xsl:attribute name="class">dopo</xsl:attribute>
		<xsl:attribute name="title">Modifica di parole - Punto di battuta</xsl:attribute>
		<xsl:apply-templates />	
	  </xsl:element>
	</xsl:template>


	<xsl:template match='mrk:skip'>
			<div class="virgolette_parola">
				<xsl:apply-templates />
			</div>	
	</xsl:template>


	<xsl:template match='mrk:conclusione'>
			<a name="{local-name(.)}"></a>
			<div class="footer">
				<xsl:apply-templates />
			</div>	
	</xsl:template>


    <!-- mrk:allegato id="annex-2"><mrk:intestazione>ALLEGATO II</mrk:intestazione>   -->


	<xsl:template match='mrk:allegato'>
		<hr />
		<a name="{@id}"></a>
		<xsl:choose>
			<!-- 1. allegato con struttura  -->
			<xsl:when test=".//mrk:corpo" >
					<xsl:apply-templates />
			</xsl:when>
			<xsl:otherwise>
				<div class="allegato">
					<xsl:apply-templates />
				</div>		
			</xsl:otherwise>
		</xsl:choose>			
	</xsl:template>





	<!-- ========================= MARKER METADATA - LOG ========================= -->


	<xsl:template match='mrk:metadata'>
		<table class="metadata_table">
			<xsl:apply-templates />
			<xsl:call-template name="validationError"/>
			<xsl:call-template name="notWellFormattedError"/>
		</table>
	</xsl:template>


	<xsl:template match='mrk:info'>
		<tr>
			<td class="mrk_info">
				<xsl:apply-templates />
			</td>
		</tr>
	</xsl:template>
	
	<xsl:template match='mrk:eli'>
		<tr>
			<td class="mrk_eli">
				<a class="anchor" href="{.}" target="_blank"><xsl:value-of select="."/></a>
			</td>
		</tr>
	</xsl:template>
	
	<xsl:template match='mrk:vigenza'>
		<tr>
			<td class="mrk_vigenza">
				 <xsl:apply-templates />
			</td>
		</tr>
	</xsl:template>
	
	
	<xsl:template match='mrk:message'>
		<!-- 
			<tr>
				<td class="mrk_message">
					<xsl:apply-templates />
				</td>
			</tr>
		-->
	</xsl:template>
 	

	
	<xsl:template match='mrk:error'>
		<xsl:variable name="idref">
			<xsl:value-of select="@idref" />
		</xsl:variable>
		<tr>
			<td class="mrk_error">
				<xsl:if test="$idref!=''">
					<a class="anchor" href="#{@idref}" title="vai a: {@idref}">&#x2913;&#160;</a>
				</xsl:if>
				<xsl:apply-templates />
			</td>
		</tr>
	</xsl:template>
	
	
	<xsl:template match='mrk:warning'>
		<xsl:variable name="idref">
			<xsl:value-of select="@idref" />
		</xsl:variable>
		<tr>
			<td class="mrk_warning">
				<xsl:if test="$idref!=''">
					<a class="anchor" href="#{@idref}" title="vai a: {@idref}">&#x2913;&#160;</a>
				</xsl:if>
				<xsl:apply-templates />
			</td>
		</tr>
	</xsl:template>
	
	
	<xsl:template name= 'validationError'>
			<tr>
				<td class="mrk_error">
					<xsl:value-of select="//mrk:validationError"/>
				</td>
			</tr>
	</xsl:template>
	
	<xsl:template match='mrk:validationError'/>

	<xsl:template name= 'notWellFormattedError'>
			<tr>
				<td class="mrk_error">
					<xsl:value-of select="//mrk:notWellFormattedError"/>
				</td>
			</tr>
	</xsl:template>
	
	<xsl:template match='mrk:notWellFormattedError'/>


</xsl:stylesheet>