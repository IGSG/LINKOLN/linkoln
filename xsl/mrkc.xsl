<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:mrk="https://www.igsg.cnr.it/marker/"
	xmlns:mrkc="https://www.igsg.cnr.it/marker/cost/"
	xmlns:lkn="https://www.igsg.cnr.it/linkoln/"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/HTML/1998/html4"
	xmlns:h="http://www.w3.org/1999/xhtml"
	xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:date="http://exslt.org/dates-and-times">


	<xsl:output method="html" indent="yes" />
	<xsl:param name="meta_html_folder" />
	
	<xsl:template match="/">
		<html>
			<head>
				<title>IGSG Marker-COST</title>
				<!-- LINK CSS su REPO -->
				<link href="http://dati.igsg.cnr.it/repo/css/mrkc.css" rel="stylesheet" />
				<!--  <link href="./../xsl/mrkc.css" rel="stylesheet" /> -->
			</head>
			<body>
		  		<div class="toc">
		  			<xsl:apply-templates select = "//mrk:ecli" mode="toc"/>
		  			<p>
		  				<a class="anchor" href="#meta" title="vai a metadati">metadati</a>
		  			</p>
		  			<xsl:apply-templates select = "//mrk:documento" mode="toc"/>
		  			<xsl:apply-templates select = "//mrk:allegati" mode="toc"/>
		  			<!-- il pannello riferimenti non proviene da trasformazione xsl su xml  -->
		  			
		  		
		  			<p>
	  					<details open="true" >
	  						<summary><a class="anchor" href="#riferimenti" title="vai a riferimenti">riferimenti</a></summary>
	  							<ul class="treeview">
	  								<li><a class="anchor" href="#out" title="vai a precedenti">precedenti</a></li>
	  								<li><a class="anchor" href="#in" title="vai a citata da">citata da</a></li>
	  								<li><a class="anchor" href="#cost" title="vai a articoli cost.">costituzione</a></li>
	  							</ul>
	  					</details>
  					</p>
		  				<!-- </li>  -->
		  			<!--  </ul> -->
		  		
		  			<xsl:apply-templates select = "//mrkc:massime" mode="toc"/>
		  		</div>
		  		
		  		
				
	 	  	    <xsl:variable name="meta_html_panel">
					<xsl:value-of select="concat(concat($meta_html_folder,translate(//mrk:ecli,':','-')),'.meta.html')" />
				</xsl:variable>
	  			
				<!--  IMPORT div class ="meta" -->
		 		<xsl:copy-of select="document($meta_html_panel)/meta/div[@class='meta']"/>
		 
		  		
		  		<div class="doc">
		  			<xsl:apply-templates select = "//mrk:documento" />
		  			<xsl:apply-templates select = "//mrk:allegati" />
		  		</div>
		 
		
				<!--  IMPORT div class ="rif" -->
		  		<xsl:copy-of select="document($meta_html_panel)/meta/div[@class='rif']"/>
		  		
		  		
		  		<!-- al momento il div massime sta sotto al div rif (vedi grid-template-areas su css)-->
		  		<div class="massime">
		  			<xsl:apply-templates select = "//mrkc:massime" />
		  		</div>
		
			</body>
		</html>
	</xsl:template>


	
	<!-- XSL TEMPLATE dei RIF -->

	<!-- Se è presente l'attributo URL crea un <a>, altrimenti <font color blue> -->
	<xsl:template match='lkn:ref[@url]'>
		<xsl:choose>
		    <!-- TEST authority = CORTE_COST-->
		    
		  	<xsl:when test="@authority='CORTE_COST'">
			  	
			  	<xsl:variable name="year">
				   <xsl:choose>
				     <xsl:when test="@doc-year">
				        <xsl:value-of select="./@doc-year"/>
				     </xsl:when>
				     <xsl:otherwise>
				        <xsl:value-of select="substring(./@doc-date,1,4)"/>
				     </xsl:otherwise>
				   </xsl:choose>
				</xsl:variable>
		  	
		  	    <a class ="prec_cost" href="http://dati.igsg.cnr.it/repo/IT/CORTE_COST/{$year}/{./@doc-number}.html" target="_blank">
					<xsl:apply-templates />	
				</a>
				
				<!-- Fuori da dati.igsg usa le url -->
				<!-- <a class ="prec_cost" href="{./@url}" target="_blank"> -->	
		  	</xsl:when>
		  	<xsl:when test="@alias='COST'">
		  		<a class ="ref_cost" href="{./@url}" target="_blank">
					<xsl:apply-templates />	
				</a>
		  	</xsl:when>
		  <xsl:otherwise>
		  		<a href="{./@url}" target="_blank">
					<xsl:apply-templates />	
				</a>
		  </xsl:otherwise>
    	</xsl:choose>	
	</xsl:template>
	
	
	<xsl:template match='mrk:parole'>
		<span class="parole"><xsl:apply-templates /></span>
	</xsl:template>

	<xsl:template match='lkn:ref'>
		<xsl:choose>
		  	<xsl:when test="@authority='CORTE_COST'">
		  		<font class ="prec_cost" color='blue'>
					<xsl:apply-templates />	
				</font>
		  	</xsl:when>
		  <xsl:otherwise>
		  		<font color='blue'>
					<xsl:apply-templates />	
				</font>
		  </xsl:otherwise>
    	</xsl:choose>	
	</xsl:template>


	<xsl:template match='lkn:warning'>
		<font color='red'>
			<xsl:apply-templates />	
		</font>
	</xsl:template>
	
	<!-- Esito -->
	<xsl:template match='mrkc:esito'>
	  <xsl:element name="span">
		<xsl:attribute name="class">esito</xsl:attribute>
		<xsl:apply-templates />	
	  </xsl:element>
	</xsl:template>


	<!-- ======================================================== -->
	<!-- 														  -->
	<!-- 			Template TOC       							  -->
	<!-- 														  -->
	<!-- ======================================================== -->
	
	<xsl:template match='mrk:ecli' mode ="toc">
		<xsl:variable name="doc-year">
		        <xsl:value-of select="substring(.,14,4)"/>
		</xsl:variable>
		<xsl:variable name="doc-num">
		        <xsl:value-of select="substring(.,19,string-length(.))"/>
		</xsl:variable>
		<p>
			<span class="bold"><xsl:value-of select="concat(concat(concat('PRONUNCIA&#160;', $doc-num),'/'),$doc-year)"/></span>
		</p>
	</xsl:template>
	
	
	<xsl:template match='mrk:ecli_old' mode ="toc">
		<xsl:variable name="url">
			<xsl:value-of select="concat('https://www.cortecostituzionale.it/actionSchedaPronuncia.do?param_ecli=',.)" />
		</xsl:variable>
		<!-- PRENDE doc-num e doc-year dall'ECLI -->
		<xsl:variable name="doc-year">
		        <xsl:value-of select="substring(.,14,4)"/>
		</xsl:variable>
		<xsl:variable name="doc-num">
		        <xsl:value-of select="substring(.,19,string-length(.))"/>
		</xsl:variable>
		<xsl:variable name="meta_url">
			<xsl:value-of select="concat(concat(concat('https://dati.cortecostituzionale.it/ontology/Pronuncia/',$doc-num),'/'),$doc-year)" />
		</xsl:variable>
		<p>
			<a class="anchor_ecli" href="{$url}" title="vai al sito della Corte" target="_blank"><xsl:value-of select="."/></a>
			&#160;
		<!-- link a meta su https://dati.cortecostituzionale.it/ontology/Pronuncia/{doc-num}/{doc-year} -->
			<a class="anchor_ecli" href="{$meta_url}" title="vai ai metadati su dati.cortecostituzionale.it" target="_blank">[meta]</a>
		</p>
	</xsl:template>
	

	<!-- ======================================================== -->
	<!-- 														  -->
	<!-- 			FIXME Template TOC Massime      			  -->
	<!-- 														  -->
	<!-- ======================================================== -->


	<xsl:template match='mrk:allegati' mode="toc" >
		<p>
	  	  <details open="true" >
	  	  	<summary><a class="anchor" href="#{local-name(.)}" title="vai a:"><xsl:value-of select="local-name(.)"/></a></summary>
		  	<ul class="treeview">
		  		  <xsl:apply-templates mode="toc" />
		  	</ul>
		  </details>
  		</p>
	</xsl:template>
		
	<xsl:template match='mrk:allegato' mode="toc" >
			<li>
			 	<a class="anchor" href="#{local-name(.)}" title="vai a:">
					<xsl:value-of select="local-name(.)"/>
			  	</a>
			</li>
	</xsl:template>
	
	
	<xsl:template match='mrkc:massime' mode="toc" >
		<p>
	  	  <details open="true" >
	  	  	<summary><a class="anchor" href="#{local-name(.)}" title="vai a:"><xsl:value-of select="local-name(.)"/></a></summary>
		  	<ul class="treeview">
		  		  <xsl:apply-templates mode="toc" />
		  	</ul>
		  </details>
  		</p>
	</xsl:template>
	
	<xsl:template match='mrkc:massima' mode="toc" >
			<li>
			 	<a class="anchor" href="#{@id}" title="vai a:">
					<xsl:value-of select="./mrkc:numeroMassima"/>
			  	</a>
			</li>
	</xsl:template>
	
	<xsl:template match='mrk:documento' mode="toc" >
		<a class="anchor" href="#{local-name(.)}" title="vai a:">
				  <!-- pronuncia -->
				  <xsl:value-of select="local-name(.)"/> 
	    </a>
		<ul class="treeview">
			<xsl:apply-templates select = "mrk:testa" mode="toc"/>
			<xsl:apply-templates select = "mrk:corpo" mode="toc"/>
			<xsl:apply-templates select = "mrk:conclusione" mode="toc"/>
		</ul>
	</xsl:template>
	
	<xsl:template match='mrk:testa' mode="toc" >
		<li>
			 <a class="anchor" href="#{local-name(.)}" title="vai a:">
				  <xsl:value-of select="local-name(.)"/>
			  </a>
		</li>
	</xsl:template>
	
	<xsl:template match='mrk:corpo' mode="toc" >
		
		<li>
			<a class="anchor" href="#{local-name(.)}" title="vai a:">
				  <xsl:value-of select="local-name(.)"/>
			</a>		
			<ul class="tree">
				<xsl:apply-templates select = "mrkc:sezione" mode="toc"/>
			</ul>
		</li>
	</xsl:template>
	 

	<xsl:template match='mrkc:sezione' mode='toc'>
		<!-- TREE VIEW HERE: se non ha figli (paragrafi sotto la sezione) non dovrebbe mettere freccina expandable  -->
		<xsl:choose>
		  <xsl:when test="mrkc:paragrafo">
		  	<li> 
			 	<details open="true">
				 	  <summary>
						  <a class="anchor" href="#{@id}" title="vai a:">
							  <xsl:value-of select="./@id"/>
						  </a>
					  </summary>
					  <ul>
					  	<xsl:apply-templates select = "mrkc:paragrafo" mode="toc"/>
					  </ul>
				 </details>
			  </li>
		  </xsl:when>
		  <xsl:otherwise>
		  	<li> 
			  <a class="anchor" href="#{@id}" title="vai a:">
				  <xsl:value-of select="./@id"/>
			  </a>
			  <ul>
				  	<xsl:apply-templates select = "mrkc:paragrafo" mode="toc"/>
			  </ul>
			  </li>
		  </xsl:otherwise>
		</xsl:choose>	 
	</xsl:template>
	
	
	<xsl:template match='mrkc:paragrafo' mode="toc" >
		<li>
			 <a class="anchor" href="#{@id}" title="vai a:">
				  <xsl:variable name="step1">
       				 <xsl:call-template name="substring-after-last">
        				<xsl:with-param name="string" select="./@id"/>
        				<xsl:with-param name="delimiter" select="'_'"></xsl:with-param>
   				  	 </xsl:call-template>	
   				  </xsl:variable>
   				  <xsl:call-template name="substring-after-last">
        				<xsl:with-param name="string" select="$step1"/>
        				<xsl:with-param name="delimiter" select="'-'"></xsl:with-param>
   				  </xsl:call-template>.	  		  
			  </a>
		</li>
	</xsl:template>
	
	
	<xsl:template name="substring-after-last">
	  <xsl:param name="string" />
	  <xsl:param name="delimiter" />	  
	  <xsl:choose>
	    <xsl:when test="contains($string, $delimiter)">
	      <xsl:call-template name="substring-after-last">
	        <xsl:with-param name="string"
	          select="substring-after($string, $delimiter)" />
	        <xsl:with-param name="delimiter" select="$delimiter" />
	      </xsl:call-template>
	    </xsl:when>
	    <xsl:otherwise><xsl:value-of select="$string" /></xsl:otherwise>
	  </xsl:choose>
	</xsl:template>
	
	
	<xsl:template match='mrk:conclusione' mode="toc" >
		<li>
			 <a class="anchor" href="#{local-name(.)}" title="vai a:">
				  <xsl:value-of select="local-name(.)"/>
			  </a>
		</li>
	</xsl:template>


	<!-- ======================================================== -->
	<!-- -->
	<!-- Template corpo -->
	<!-- -->
	<!-- ======================================================== -->


	
	<xsl:template match='h:p'>
		<a name="{@id}"></a>
		<xsl:choose>
		    <!-- primo paragrafo di una lista numerata: non andare a capo -->
			<xsl:when test="name(preceding-sibling::*[1])='mrkc:intestazione'" >
				<span>
					<xsl:attribute name="title"><xsl:value-of select="./@id" /></xsl:attribute>
					<xsl:apply-templates />
				</span>
			</xsl:when>
			<xsl:otherwise> 
				<p>
					<xsl:attribute name="title"><xsl:value-of select="./@id" /></xsl:attribute>
					<xsl:apply-templates />
				</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<xsl:template match='mrk:documento'>
		<a name="{local-name(.)}"></a>	
		<div>
			<xsl:apply-templates />
		</div>
	</xsl:template>
	
	<xsl:template match='mrk:testa'>
		<a name="{local-name(.)}"></a>	
		<div>
			<xsl:apply-templates />
		</div>
	</xsl:template>
	
	<xsl:template match='mrk:corpo'>
		<a name="{local-name(.)}"></a>	
		<div>
			<xsl:apply-templates />
		</div>
	</xsl:template>
	
	
	<xsl:template match='mrk:conclusione'>
		<a name="{local-name(.)}"></a>	
		<div>
			<xsl:apply-templates />
		</div>
	</xsl:template>
	
	<xsl:template match='mrk:allegati'>
		<a name="{local-name(.)}"></a>	
		<div>
			<xsl:apply-templates />
		</div>
	</xsl:template>
	
	<xsl:template match='mrk:allegato'>
		<a name="{local-name(.)}"></a>
		<hr class="hr_massima"/>	
		<div>
			<xsl:apply-templates />
		</div>
	</xsl:template>
	
	
	<!-- ======================================================== -->
	<!-- 														  -->
	<!-- Template MASSIMA										  -->
	<!-- 														  -->
	<!-- ======================================================== -->


	<xsl:template match='mrkc:massime'>
		<a name="{local-name(.)}"></a>	
		<div>
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<xsl:template match='mrkc:massima'>
		<a name="{@id}"></a>	
		<div class="massima">
			<hr class="hr_massima"/>
			<xsl:apply-templates />	
		</div>
	</xsl:template>
	
	<xsl:template match='mrkc:numeroMassima'>
			<p>
				Massima n.&#160;
				<span class = "mrkc_numeroMassima">
					<xsl:apply-templates />
				</span>
			</p>
	</xsl:template>
	
	<xsl:template match='mrkc:testoMassima'>
			<div class = "mrkc_testoMassima">
				<xsl:apply-templates />
			</div>
	</xsl:template>
	
	<xsl:template match='mrkc:titoliMassima'>
			<p class = "mrkc_titoliMassima">
				<!--  Titolo:&#160; -->
				<xsl:apply-templates />
			</p>
	</xsl:template>
	
	
	<xsl:template match='mrkc:titoloMassima'>
			<span class = "mrkc_titoloMassima">
				<xsl:apply-templates />
			</span>
	</xsl:template>
	
	<!-- mettere un contenitore per parametroMassima e oggettoMassima ? oppure al primo tag metti un testo: Parametro/Oggetto 
	<mrkc:parametroMassima>
	          <lkn:ref alias="COST" id="1" partition="articolo-2" ref-scope="nazionale" ref-type="legislation" url="http://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:costituzione:1947-12-27~art2">Costituzione art. 2</lkn:ref>
	</mrkc:parametroMassima>
	<mrkc:parametroMassima>
	          <lkn:ref alias="COST" id="1" partition="articolo-3" ref-scope="nazionale" ref-type="legislation" url="http://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:costituzione:1947-12-27~art3">Costituzione art. 3</lkn:ref>
	</mrkc:parametroMassima>
	 -->
	<xsl:template match='mrkc:parametriMassima'>
		<p class ='bold'>Parametri costituzionali</p>
		<xsl:apply-templates />
	</xsl:template>
	 
	<xsl:template match='mrkc:parametroMassima'>
				<p class = "mrkc_parametroMassima">
					<xsl:apply-templates />
				</p>
	</xsl:template>
	
	<xsl:template match='mrkc:oggettiMassima'>
		<p class ='bold'>Atti oggetto del giudizio</p>
		<xsl:apply-templates />
	</xsl:template>
	
	<xsl:template match='mrkc:oggettoMassima'>
				<p class = "mrkc_oggettoMassima">
					<xsl:apply-templates />
				</p>
	</xsl:template>
	

	<!-- ======================================================== -->
	<!-- 														  -->
	<!-- END Template MASSIMA										  -->
	<!-- 														  -->
	<!-- ======================================================== -->
	
	<xsl:template match='mrkc:titoloSezione'>
		<xsl:choose>
			<xsl:when test="parent::*[1]/@id='fatto' or parent::*[1]/@id='diritto'" >
				<div class="titolo_sezione_italic">
					<xsl:apply-templates />
				</div>
			</xsl:when>
			<xsl:otherwise>  
				<div class="titolo_sezione">
					<xsl:apply-templates />
				</div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match='mrkc:intestazione'>
	  <xsl:element name="span">
		<xsl:attribute name="class">
			<xsl:value-of select ="concat('intestazione_',local-name(./..))"/>
		</xsl:attribute>
		<xsl:attribute name="title"><xsl:value-of select="./../@id" /></xsl:attribute>
		<xsl:apply-templates />	
	  </xsl:element>
	</xsl:template>
	
	
	<!-- ========================= SEZIONE =============================== -->

	<xsl:template match='mrkc:sezione'>
		<a name="{@id}"></a>
		<div class="{@id}">
			<xsl:apply-templates />
		</div>
	</xsl:template>
		
	
	<xsl:template match='mrkc:paragrafo'>
		<a name="{@id}"></a>
		<p class="paragrafo_cost">
			<xsl:apply-templates />
		</p>	      
	</xsl:template>
	
	<xsl:template match='mrkc:item'>
		<a name="{@id}"></a>
		<p class="item">
			<xsl:apply-templates />
		</p>
	</xsl:template>
	
	<xsl:template match='mrkc:subitem'>
		<a name="{@id}"></a>
		<p class="subitem"> 
			<xsl:apply-templates />
		</p>
	</xsl:template>
	
	
	
	<xsl:template match='h:br'>	
		<br/>
	</xsl:template>
	
	<xsl:template match='h:ul'>
		<ul class="ul_cost">
			<xsl:apply-templates />	
		</ul>
	</xsl:template>
	
	<xsl:template match='h:li'>	
		<li class="li_cost">
			<xsl:apply-templates />
		</li>
	</xsl:template>
	
	
	<xsl:template match='h:pre'>
		<pre>
			<xsl:apply-templates />
		</pre>
	</xsl:template>
	


	<!-- ========================= MARKER METADATA - LOG ========================= -->


	<xsl:template match='mrk:metadata'>
		
		<!-- <
		table class="metadata_table">
			<xsl:apply-templates />
		</table>
	     -->
	</xsl:template>


	<xsl:template match='mrk:info'>
		
		<tr>
			<td class="mrk_info">
				<xsl:apply-templates />
			</td>
		</tr>
	</xsl:template>
	
	

	
	
	
	<xsl:template match='mrk:vigenza'>
		<tr>
			<td class="mrk_vigenza">
				 <xsl:apply-templates />
			</td>
		</tr>
	</xsl:template>
	
	<xsl:template match='mrk:message'>
		<tr>
			<td class="mrk_message">
				<xsl:apply-templates />
			</td>
		</tr>
	</xsl:template>


	
	<xsl:template match='mrk:error'>
		<xsl:variable name="idref">
			<xsl:value-of select="@idref" />
		</xsl:variable>
		<tr>
			<td class="mrk_error">
				<xsl:if test="$idref!=''">
					<a class="anchor" href="#{@idref}" title="vai a: {@idref}">&#x2913;&#160;</a>
				</xsl:if>
				<xsl:apply-templates />
			</td>
		</tr>
	</xsl:template>
	
	
	<!-- &#x2304; &#x2913; &#x23EC;  &#x261F;   -->
	<xsl:template match='mrk:warning'>
		<xsl:variable name="idref">
			<xsl:value-of select="@idref" />
		</xsl:variable>
		<tr>
			<td class="mrk_warning">
				<xsl:if test="$idref!=''">
					<a class="anchor" href="#{@idref}" title="vai a: {@idref}">&#x2913;&#160;</a>
				</xsl:if>
				<xsl:apply-templates />
			</td>
		</tr>
	</xsl:template>
	
	
	<xsl:template name= 'validationError'>
			<tr>
				<td class="mrk_error">
					<xsl:value-of select="//mrk:validationError"/>
				</td>
			</tr>
	</xsl:template>
	
	<xsl:template match='mrk:validationError'/>

	<xsl:template name= 'notWellFormattedError'>
			<tr>
				<td class="mrk_error">
					<xsl:value-of select="//mrk:notWellFormattedError"/>
				</td>
			</tr>
	</xsl:template>
	
	<xsl:template match='mrk:notWellFormattedError'/>


</xsl:stylesheet>