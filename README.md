<img src="linkoln-logo.png" alt="Linkoln logo" width="400"/>

<hr>

**Linkoln** is a software developed at the [Institute of Legal Informatics and Judicial Systems](https://www.igsg.cnr.it) (IGSG) of the [National Research Council of Italy](https://www.cnr.it/) (CNR) for the automatic detection and linking of legal references contained in legal texts written in Italian.

<h2>More info available at https://linkoln.gitlab.io</h2>

<p>&nbsp;</p>
<p>&nbsp;</p>

<img src="marker-legge-html-modifiche-linkoln.png" alt="IGSG Marker and Linkoln screenshot"/>


